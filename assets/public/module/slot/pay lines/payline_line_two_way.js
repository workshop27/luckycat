var payline_base = require('./payline');
cc.Class({
    extends: payline_base,

    properties: {
        LinesParent : {
            type : cc.Node,
            default : null,
            displayName: "中獎線 Parent Node",
            tooltip: "指定的物件，裡面必須存放顯示線的物件，並且線的名稱是編號名稱",
        },

        ReverseLinesParent : {
            type: cc.Node,
            default : null,
            displayName: "逆向中獎線 Parent Node",
            tooltip: "指定的物件，裡面必須存放顯示線的物件，並且線的名稱是編號名稱",
        },

        ShowAllLineStepTime : {
            default: 0,
            displayName: '逐一播放延遲秒數',
        },

        '掃光效果' : undefined,

        ShowLineEffectNode : {
            type: cc.Node,
            default: null,
            displayName: "掃光特效 Animation Node",
        },

    },

    smart_check() {
        if ( this.LinesParent == null ) return GW.notify_fail("查無資料(payline) 中獎線ParentNode(LineParent) 請檢察相關設定");
        if ( this.Mask == null ) return GW.notify_fail("(payline) 查無遮罩(mask)設定");
        if ( this.ShowAllLineScoreLabel == null ) return GW.notify_fail("全線得分顯示");
        if ( this.ShowPaylineNode == null ) return GW.notify_fail("查無資料(payline) 得分層(ShowPaylineNode) 請檢察相關設定");
        if ( this.ShowOneLineScoreLabel == null ) return GW.notify_fail("查無資料(payline) 顯示單線分數(ShowOneLineScoreLabel) 請檢察相關設定");
        if ( this.ShowLineEffectNode != null ) {
            this.ShowLineEffect = this.ShowLineEffectNode.getComponent(cc.Animation);
        }
    },

    /**
     * 載入秀線物件
     */
    load_line() {
        var lines = this.LinesParent.getChildren();
        this.lines = lines;
        
        if ( this.ReverseLinesParent == null ) {
            this.reverse_lines = lines;
        } else {
            var reverse_lines = this.ReverseLinesParent.getChildren();
            this.reverse_lines = reverse_lines;
        }

        this.close_all_lines();
    },

    start () {
        this.smart_check();
        this.load_line();
        
        this.Mask.active = false;
        this.ShowAllLineScoreLabel.node.active = true;
        this.show_all_line_score(0);

        this.ShowOneLineScoreLabel.node.active = true;
        this.ShowOneLineScoreLabel.string = "";

        this.status = {
            round_data : null,  // 目前播放得分資料
            round_idx : 0,      // 播放得分序號
            allline_done: false, // 是否播過全線分數
            call_stop: false,   // 是否按下停止播放分數
        };
    },

    is_payline() { return ( machine.status.state == machine.mState.Payline ) ? true : false; },

    /**
     * 播放掃線效果
     */
    show_line_effect() {
        if ( this.ShowLineEffect == null ) return;
        this.ShowLineEffect.stop();
        this.ShowLineEffect.play();
    },

     /**
      * 全部轉輪停止
      */
    /* all_reel_done(round_data) {
        // console.log(round_data);
        // 沒有得分, 回到遊戲
        if ( round_data['total_score'] == 0 ) return machine.show_end_payline_done();
        machine.controller_bar.add_win_score_label(round_data['total_score']);
        return this.show_all_payline(round_data);
    }, */

    /**
     * 顯示全線中獎
     * @public 入口function
     */
    show_all_payline(round_data=null) {
        if ( round_data == null ) {
            round_data = this.status.round_data;
            if ( this.status.call_stop == true ) return this.show_payline_done();
            this.show_end_payline();    // 關閉上一個

        } else {
            // 中斷全線
            if ( machine.show_all_payline_break(round_data) == true ) return;

            this.status.round_data = round_data;
            this.status.allline_done = false;
            this.status.call_stop = false;
        }

        this.status.round_idx = 0;
        var payline = round_data['payline'];

        //if ( this.ShowAllLineSound != null ) sound.PlaySound(this.ShowAllLineSound);

        var keys = Object.keys(payline);
        var repeat = keys.length;
        
        this.status.step_idx = 0;
        this.status.step_score = 0;

        this.schedule(function(){
            var payline = this.status.round_data['payline'];
            var key = keys[this.status.step_idx];
            var score = payline[key]['score'];
            var total_score = this.status.step_score;
            total_score = total_score.add_float(score);

            this.status.step_score = total_score;
            this.show_one_payline(key, payline[key], false, true);
            this.show_all_line_score(this.status.step_score); // 總得分顯示
            if ( this.ShowOneLineSound != null ) sound.PlaySound(this.ShowOneLineSound);
            this.status.step_idx ++;
        },this.ShowAllLineStepTime, repeat-1, this.ShowAllLineStepTime);

        var wait_time = (keys.length+1) * this.ShowAllLineStepTime + this.ShowAllPaylineTime;

        /*
        for(var key in payline) { // 顯示線
            total_score += payline[key]['score'];
            console.log('wait:'+wait_time);
            this.scheduleOnce(function() {
                this.show_one_payline(key, payline[key], false, true);
                var score = total_score;
                this.show_all_line_score(score); // 總得分顯示
            }, wait_time);
            wait_time += this.ShowAllLineStepTime;
        }
        wait_time += this.ShowAllPaylineTime;
        */
        this.show_line_effect();                // 掃光特效
        this.show_all_line_node_active(true);   // 其他指定(Node)效果 
        
        var have_bigwin = false;

        // 總得分顯示
        if ( this.check_big_win() == true && machine.check_bonus_game(false) == false ) { // 沒有 BigWin才需要顯示總分
            have_bigwin = true;
        } else { // 有BigWin就打一個旗標
            this.show_all_line_score(round_data['total_score']);
        }

        this.scheduleOnce(function() { 
            if ( this.is_payline() == false ) return;
            this.status.allline_done = true;
            if (machine.check_bonus_game(false) == true) {
                this.show_end_payline(true);
                machine.check_bonus_game(true);
                return;
            }
            if ( have_bigwin ) {
                machine.run_big_win(); 
                this.status.bigwin_done = true;
                return;
            }
            this.loop_show_one_payline(); 
            this.show_all_line_node_active(false);
        }, wait_time);

        return machine.event_show_all_payline(round_data);
    },

    /**
     * 開/關 全線播放執行Node
     * @param {*} bool 
     */
    show_all_line_node_active(bool) {
        if ( this.ShowAllLineNode == null ) return;
        this.ShowAllLineNode.active = bool;
        
    },

    show_one_line_node_active(bool) {
        if ( this.ShowOneLineNode == null ) return;
        this.ShowOneLineNode.active = bool;
    },

    /**
     * 關閉所有中獎效果
     * @from this.loop_show_one_payline
     * @private
     */
    show_end_payline(close_mask=false) {
        this.show_all_line_node_active(false);
        this.show_one_line_node_active(false);
        machine.reel_frame.show_end_payline(); // 關閉 symbol 特效
        this.close_all_lines();

        // for(var idx in this.status.round_data['payline']) this.lines[idx].active = false;   // 關掉線
        this.show_all_line_score(0);                    // 關分數
        if ( close_mask ) this.active_mask(false);      // 關遮罩
    },

    /**
     * 關閉所有線
     */
    close_all_lines() {
        if(this.lines == null) return;
        this.lines.forEach(function(one_line) { one_line.active = false; });
        this.reverse_lines.forEach(function(one_line){ one_line.active = false; });
    },

    /**
     * 單線輪播
     * @param {*} index 
     */
    loop_show_one_payline(index=0) {
        if ( this.status.call_stop == true ) return this.show_payline_done();
        if ( machine.is_auto_spin() == true ) return this.show_payline_done();

        this.show_end_payline();    // 關閉上一個

        var round_data = this.status.round_data;
        var keys = Object.keys(round_data['payline']);
        var line = keys[index];
        var payline = round_data['payline'];
        
        if ( this.ShowOneLineSound != null ) sound.PlaySound(this.ShowOneLineSound);

        this.show_one_line_node_active(true);
        this.show_one_payline(line, payline[line], true, false); // 播放下一個
        this.show_line_effect();
        
        var next = index + 1;
        this.scheduleOnce(function() {
            if ( this.is_payline() == false ) return;

            if ( next >= keys.length ) {
                this.show_all_payline();
            } else {
                this.loop_show_one_payline(next); 
            }
        }, this.ShowOnePaylineTime);

        var oneline = payline[line];
        oneline['number'] = line;
        return machine.event_show_one_payline(oneline);
    },

    /**
     * 顯示單線得獎
     * @param {number} line 線號
     * @param {json} payline 得獎資料 
     * @param {boolean} show_score 是否顯示單線分數
     * @param {boolean} is_show_all_line 是否為全線播放
     */
    show_one_payline(line, payline, show_score, is_show_all_line) {
        
        if ( machine.show_one_payline_break(line, payline, show_score) ) return;
        this.active_mask(true);
        
        // 把線打開
        this.one_line_active(line, true);
        return machine.reel_frame.show_payline(payline, show_score, is_show_all_line); // 顯示 symbol 得獎效果
    },

    one_line_active(line_number, bool) {
        //console.log(line_number);
        if ( line_number < 0 ) this.reverse_lines[-line_number].active = bool;
        else this.lines[line_number].active = bool; 
    },

    /**
     * 顯示全線得分
     * @param {*} score 
     */
    show_all_line_score( score ) {
        if ( score == 0 ) {
            this.ShowAllLineScoreLabel.stretch_string = "";
        } else {
            this.ShowAllLineScoreLabel.stretch_string = score;
        }
    },

    /**
     * 按下結束輪播
     */
    call_stop() {
        if ( this.status.call_stop == true ) return;
        this.status.call_stop = true;

        if ( this.status.allline_done == true ) {
            this.unschedule(this.loop_show_one_payline);
            return this.show_payline_done();
        }
    },

    /**
     * 結束播放，回到machine控制
     */
    show_payline_done() {
        if ( this.is_payline() == false ) return;
        this.show_end_payline(true);
        sound.playGetMoney();
        machine.show_end_payline_done();
        this.unscheduleAllCallbacks();
        this.status.allline_done = false;
    },

    /**
     * 遮罩開關
     * @param {boolean} active 
     */
    active_mask(active) { return this.Mask.active = active },

});
