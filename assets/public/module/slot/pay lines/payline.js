/*
 * 有任何需要中斷執行的內容，可使用繼承複寫方式處置
 */
cc.Class({
  extends: cc.Component,

  properties: {
    基礎設定: undefined,

    Mask: {
      type: cc.Node,
      default: null,
      displayName: "滾輪區遮罩"
    },

    ShowPaylineNode: {
      type: cc.Node,
      default: null,
      displayName: "得分層",
      tooltip: "Symbol 播放中獎效果, 顯示層級需要高於 mask"
    },

    全線播放設定: undefined,

    ShowAllLineScoreLabel: {
      type: require("../../common/stretch_background_label_m"),
      default: null,
      displayName: "全線分數 Label"
    },

    ShowAllPaylineTime: {
      default: 1,
      displayName: "全線分數顯示時間"
    },

    ShowAllLineSound: {
      type: cc.AudioClip,
      default: null,
      displayName: "全線得獎音效"
    },

    ShowAllLineNode: {
      type: cc.Node,
      default: null,
      displayName: "全線播放執行Node"
    },

    單線輪播設定: undefined,

    ShowOneLineScoreLabel: {
      type: cc.Label,
      default: null,
      displayName: "顯示單線分數"
    },

    ShowOnePaylineTime: {
      default: 3,
      displayName: "單線分數顯示時間"
    },

    ShowOneLineSound: {
      type: cc.AudioClip,
      default: null,
      displayName: "單線播放音效"
    },

    ShowOneLineNode: {
      type: cc.Node,
      default: null,
      displayName: "單線輪播執行Node"
    }
  },

  notify_fail(message) {
    alert(message);
    console.log(message);
  },

  smart_check() {
    if (this.Mask == null)
      return this.notify_fail("(payline) 查無遮罩(mask)設定");
    if (this.ShowAllLineScoreLabel == null)
      return this.notify_fail("全線得分顯示");
    if (this.ShowOneLineScoreLabel == null)
      return this.notify_fail(
        "查無資料(payline) 顯示單線分數(ShowOneLineScoreLabel) 請檢察相關設定"
      );
  },

  preload() {
    this.smart_check();

    this.Mask.active = false;
    this.ShowAllLineScoreLabel.node.active = true;
    this.ShowAllLineScoreLabel.stretch_string = "";
    this.show_all_line_score(0);

    this.ShowOneLineScoreLabel.node.active = true;
    this.ShowOneLineScoreLabel.string = "";

    this.status = {
      round_data: null, // 目前播放得分資料
      round_idx: 0, // 播放得分序號
      allline_done: false, // 是否播過全線分數
      call_stop: false, // 是否按下停止播放分數
      bigwin_done: false // 是否播放 bigwin
    };
  },

  is_payline() {
    if (cc.machine.status.bonus_gaming == true) return false;
    return cc.machine.status.state == cc.machine.mState.Payline ? true : false;
  },

  /**
   * 全部轉輪停止
   */
  all_reel_done(round_data) {
    // console.log(round_data);
    // 沒有得分, 回到遊戲
    if (cc.machine.status.bonus_gaming == true) return;
    if (
      round_data["total_score"] == 0 &&
      cc.machine.check_bonus_game(false) == false
    )
      return cc.machine.show_end_payline_done();
    cc.machine.slot_control_bar.set_win_score_label(round_data["total_score"]);
    return this.show_all_payline(round_data);
  },

  /**
   * 判斷是否要播放bigwin
   */
  check_big_win() {
    if (this.status.bigwin_done == true) return false; // 播過了就不要再播了
    if (cc.machine.check_big_win() == false) return false;
    return true;
  },

  /**
   * 顯示全線中獎
   * @public 入口function
   */
  show_all_payline(round_data = null) {
    if (round_data == null) {
      round_data = this.status.round_data;
      if (this.status.call_stop == true) return this.show_payline_done();
      this.show_end_payline(); // 關閉上一個
    } else {
      if (
        round_data["total_score"] == 0 &&
        cc.machine.check_bonus_game(true) == true
      )
        return;
      // 中斷全線
      if (cc.machine.show_all_payline_break(round_data) == true) return;

      this.status.round_data = round_data;
      this.status.allline_done = false;
      this.status.call_stop = false;
    }

    this.status.round_idx = 0;
    var payline = round_data["payline"];

    if (this.ShowAllLineSound != null)
      cc.Sound.PlaySound(this.ShowAllLineSound, false);

    for (var key in payline) {
      // 顯示線
      this.show_one_payline(key, payline[key], false, true);
    }

    if (this.show_line_effect != null) this.show_line_effect();
    this.show_all_line_node_active(true);
    var have_bigwin = false;

    // 總得分顯示
    if (
      this.check_big_win() == true &&
      cc.machine.check_bonus_game(false) == false
    ) {
      // 沒有 BigWin才需要顯示總分
      have_bigwin = true;
    } else {
      // 有BigWin就打一個旗標
      this.show_all_line_score(round_data["total_score"]);
    }

    this.scheduleOnce(() => {
      if (this.is_payline() == false) return;
      this.status.allline_done = true;
      if (cc.machine.check_bonus_game(false) == true) {
        this.show_end_payline(true);
        cc.machine.check_bonus_game(true);
        return;
      }
      if (have_bigwin) {
        cc.machine.run_big_win();
        this.status.bigwin_done = true;
        return;
      }
      this.loop_show_one_payline();
    }, this.ShowAllPaylineTime);

    return cc.machine.event_show_all_payline(round_data);
  },

  /**
   * 開/關 全線播放執行Node
   * @param {*} bool
   */
  show_all_line_node_active(bool) {
    if (this.ShowAllLineNode == null) return;
    this.ShowAllLineNode.active = bool;
  },

  show_one_line_node_active(bool) {
    if (this.ShowOneLineNode == null) return;
    this.ShowOneLineNode.active = bool;
  },

  /**
   * 關閉所有中獎效果
   * @from this.loop_show_one_payline
   * @private
   */
  show_end_payline(close_mask = false) {
    this.show_all_line_node_active(false);
    this.show_one_line_node_active(false);
    cc.machine.reel_frame.show_end_payline(); // 關閉 symbol 特效
    if (
      this.lines != null &&
      this.status.round_data != null &&
      this.status.round_data["payline"] != null
    ) {
      for (var idx in this.status.round_data["payline"]) {
        if (this.lines[idx] == null) continue;
        this.lines[idx].active = false; // 關掉線
      }
    }
    this.show_all_line_score(0); // 關分數
    if (close_mask) this.active_mask(false); // 關遮罩
  },

  /**
   * 單線輪播
   * @param {*} index
   */
  loop_show_one_payline(index = 0) {
    if (this.status.call_stop == true) return this.show_payline_done();
    if (cc.machine.is_auto_spin() == true) return this.show_payline_done();

    this.show_end_payline(); // 關閉上一個

    var round_data = this.status.round_data;
    var keys = Object.keys(round_data["payline"]);
    var line = keys[index];
    var payline = round_data["payline"];

    if (this.ShowOneLineSound != null)
      cc.Sound.PlaySound(this.ShowOneLineSound);

    this.show_one_line_node_active(true);
    this.show_one_payline(line, payline[line], true, false); // 播放下一個
    if (this.show_line_effect != null) this.show_line_effect();

    var next = index + 1;
    this.scheduleOnce(function () {
      if (this.is_payline() == false) return;

      if (this.check_full_reward(next) == true) {
        this.show_full_reward();
      } else if (next >= keys.length) {
        this.show_all_payline();
      } else {
        this.loop_show_one_payline(next);
      }
    }, this.ShowOnePaylineTime);

    var oneline = payline[line];
    if (oneline == null) {
      console.log("show pay line got null", [line, payline, round_data]);
      return;
    }
    oneline["number"] = line;
    return cc.machine.event_show_one_payline(oneline);
  },

  /**
   * 檢查是否有全盤
   */
  check_full_reward(next) {
    return false;
  },

  /**
   * 顯示單線得獎
   * @param {number} line 線號
   * @param {json} payline 得獎資料
   * @param {boolean} show_score 是否顯示單線分數
   * @param {boolean} is_show_all_line 是否為全線播放
   */
  show_one_payline(line, payline, show_score, is_show_all_line) {
    if (cc.machine.show_one_payline_break(line, payline, show_score)) return;
    this.active_mask(true);

    if (this.lines != null && this.lines[line] != null)
      this.lines[line].active = true; // 把線打開
    return cc.machine.reel_frame.show_payline(
      payline,
      show_score,
      is_show_all_line
    ); // 顯示 symbol 得獎效果
  },

  /**
   * 顯示全線得分
   * @param {*} score
   */
  show_all_line_score(score) {
    if (cc.machine != null) {
      var tmp = cc.machine.call_controller("show_all_line_score", score);
      if (tmp != false) score = tmp;
    }

    if (score == 0) {
      this.ShowAllLineScoreLabel.stretch_string = "";
    } else {
      this.ShowAllLineScoreLabel.stretch_string = score.format_money();
    }
  },

  /**
   * 按下結束輪播
   */
  call_stop() {
    if (this.status.call_stop == true) return;
    this.status.call_stop = true;

    if (this.status.allline_done == true) {
      this.unschedule(this.loop_show_one_payline);
      return this.show_payline_done();
    }
  },

  /**
   * 結束播放，回到machine控制
   */
  show_payline_done() {
    this.status.bigwin_done = false;
    if (this.is_payline() == false) return;
    this.unscheduleAllCallbacks();
    this.show_end_payline(true);
    // sound.playGetMoney();
    cc.machine.show_end_payline_done();
  },

  /**
   * 遮罩開關
   * @param {boolean} active
   */
  active_mask(active) {
    return (this.Mask.active = active);
  },

  /**
   * 載入 symbol
   * @param { Component } symbol symbol component
   * @from machine.load_symbol
   */
  load_symbol(symbol) {
    return;
  },

  /**
   * Spin後獲得答案，決定是否要中斷執行
   * @param { json } data 獲得資訊
   * @return true : 中斷處理, 不停輪
   * @return false : machine 繼續執行停輪
   */
  spin_response_break(data) {
    return false;
  },

  /**
   * 單一滾輪停止前，決定是否要中斷停輪
   * @param { int } reel_id Reel 編號
   * @return true : 中斷、不停輪
   * @return false: 停輪
   */
  one_reel_done_break(reel_id) {
    return false;
  },

  /**
   * 全部滾輪停止後，決定是否要中斷執行
   */
  all_reel_done_break() {
    return false;
  }
});
