var payline_base = require("./payline");
cc.Class({
  extends: payline_base,

  properties: {
    LinesParent: {
      type: cc.Node,
      default: null,
      displayName: "中獎線 Parent Node",
      tooltip: "指定的物件，裡面必須存放顯示線的物件，並且線的名稱是編號名稱"
    },

    掃光效果: undefined,

    ShowLineEffectNode: {
      type: cc.Node,
      default: null,
      displayName: "掃光特效 Animation Node"
    },

    FgMask: {
      type: cc.Node,
      default: null,
      displayName: "Fg滾輪區遮罩"
    }
  },

  notify_fail(message) {
    alert(message);
    console.log(message);
  },

  smart_check() {
    if (this.LinesParent == null)
      return this.notify_fail(
        "查無資料(payline) 中獎線ParentNode(LineParent) 請檢察相關設定"
      );
    if (this.Mask == null)
      return this.notify_fail("(payline) 查無遮罩(mask)設定");
    if (this.ShowAllLineScoreLabel == null)
      return this.notify_fail("全線得分顯示Label");
    // if ( this.ShowPaylineNode == null ) return this.notify_fail("查無資料(payline) 得分層(ShowPaylineNode) 請檢察相關設定");
    if (this.ShowOneLineScoreLabel == null)
      return this.notify_fail(
        "查無資料(payline) 顯示單線分數(ShowOneLineScoreLabel) 請檢察相關設定"
      );
    if (this.ShowLineEffectNode != null) {
      this.ShowLineEffect = this.ShowLineEffectNode.getComponent(cc.Animation);
    }
  },

  /**
   * 載入秀線物件
   */
  load_line() {
    var lines = this.LinesParent.getChildren();
    this.lines = lines;
    lines.forEach(function (line) {
      line.active = false;
    });
  },

  start() {
    this.smart_check();
    this.load_line();

    this.Mask.active = false;
    this.ShowAllLineScoreLabel.node.active = true;
    this.show_all_line_score(0);

    this.ShowOneLineScoreLabel.node.active = true;
    this.ShowOneLineScoreLabel.string = "";

    this.status = {
      round_data: null, // 目前播放得分資料
      round_idx: 0, // 播放得分序號
      allline_done: false, // 是否播過全線分數
      call_stop: false // 是否按下停止播放分數
    };
  },

  /**
   * 播放掃線效果
   */
  show_line_effect() {
    if (this.ShowLineEffect == null) return;
    this.ShowLineEffect.stop();
    this.ShowLineEffect.play();
  },

  check_full_reward(next) {
    return this.lines.length == next;
  },

  show_full_reward() {
    if (this.status.call_stop == true) return this.show_payline_done();
    if (cc.machine.is_auto_spin() == true) return this.show_payline_done();

    this.show_end_payline(); // 關閉上一個

    var round_data = this.status.round_data;
    var keys = Object.keys(round_data["payline"]);
    var payline = round_data["payline"];
    var score = 0;

    if (this.ShowOneLineSound != null) sound.PlaySound(this.ShowOneLineSound);

    this.show_one_line_node_active(true);

    for (let index in keys) {
      let line = keys[index];
      this.show_one_payline(line, payline[line], true, false); // 播放下一個
      score += payline[line]["score"] * 1000;
    }

    var score_label = this.ShowOneLineScoreLabel;
    var symbol = cc.machine.reel_frame.reel[0].query_symbol()[0];

    cc.machine.move_node(score_label.node, symbol);

    score_label.node.x = 0;
    score_label.node.y = 0;
    score_label.string = (round_data["total_score"] * 1000 - score) / 1000;

    this.scheduleOnce(function () {
      if (this.is_payline() == false) return;
      this.show_all_payline();
    }, this.ShowOnePaylineTime);
  },

  /**
   * 遮罩開關
   * @param {boolean} active
   */
  active_FgMask(active) {
    return (this.FgMask.active = active);
  }
});
