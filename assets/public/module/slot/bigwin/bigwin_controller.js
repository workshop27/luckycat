var machine_state = {
  Lock: -1,
  Normal: 0,
  Spin: 1,
  SpinResponse: 2,
  CallStop: 3,
  Payline: 4
};
var mState = cc.Enum(machine_state);

cc.Class({
  extends: cc.Component,

  properties: {
    "節點 設定": undefined,

    mask_node: {
      default: null,
      type: cc.Node,
      displayName: "遮罩"
    },

    active_node_show: {
      default: [],
      type: [cc.Node],
      displayName: "需開關的節點"
    },

    collect: {
      default: null,
      type: cc.Node,
      displayName: "Collect快播按鈕"
    },

    score: {
      default: null,
      type: cc.Node,
      displayName: "分數"
    },

    "特效 設定": undefined,

    effect_parent_node: {
      default: null,
      type: cc.Node,
      displayName: "特效群"
    },

    "數值 設定": undefined,

    pause_duration: {
      type: cc.Float,
      default: 2,
      displayName: "暫停時間"
    },

    show_mutiple_threshold: {
      type: cc.Float,
      default: 20,
      displayName: "贏分幾倍呈現"
    },

    bigwin_node: {
      type: cc.Node,
      default: null,
      displayName: "BigWinNode"
    },

    bigwin_sound: {
      type: cc.AudioClip,
      default: null,
      displayName: "BigWinSound"
    },

    megawin_mutiple: {
      type: cc.Float,
      default: 30,
      displayName: "MEGA幾倍呈現"
    },

    megawin_node: {
      type: cc.Node,
      default: null,
      displayName: "MegaWinNode"
    },

    megawin_sound: {
      type: cc.AudioClip,
      default: null,
      displayName: "MegaWinSound"
    },

    super_win_mutiple: {
      type: cc.Float,
      default: 30,
      displayName: "Super幾倍呈現"
    },

    superwin_node: {
      type: cc.Node,
      default: null,
      displayName: "SuperWinNode"
    },

    superwin_sound: {
      type: cc.AudioClip,
      default: null,
      displayName: "SuperWinSound"
    },

    close_delay: 3
  },

  start() {
    this.node.setPosition(0, 0);
    this.mask_node.active = false;
    this.score_scroller = this.score.getComponent("scroll_number_m");
    this.mask_origin_opacity = this.mask_node.opacity;
    this.bigwin_node.active = false;
    this.megawin_node.active = false;
    this.superwin_node.active = false;
    this.node.active = false;
    cc.big_win_controller = this;

    this.bet = 0;
    this.end_value = 0;
    // this.scheduleOnce(function(){ this.run_big_win(4, 1000); },1);
  },

  check_have_big_win(bet, score) {
    if (cc.machine.status.bonus_gaming == true) return false;
    return this.check_big_win_score(bet, score);
  },

  check_big_win_score(bet, score) {
    var check = bet * this.show_mutiple_threshold;
    if (score < check) return false;

    return true;
  },

  check_have_mega_win(score) {
    if (this.status >= 2) return false;
    if (score < this.bet * this.megawin_mutiple) return false;
    this.status = 2;
    this.bigwin_node.active = false;
    this.megawin_node.active = true;
    this.superwin_node.active = false;
    if (this.megawin_sound != null)
      cc.Sound.PlaySound(this.megawin_sound, false);
    return true;
  },

  check_have_super_win(score) {
    if (this.status >= 1) return false;
    if (score < this.bet * this.super_win_mutiple) return false;
    this.status = 1;
    this.bigwin_node.active = false;
    this.megawin_node.active = false;
    this.superwin_node.active = true;
    if (this.superwin_sound != null)
      cc.Sound.PlaySound(this.superwin_sound, false);
    return true;
  },

  /**
   * 檢查是否達到條件
   */
  run_big_win(bet, score) {
    // if ( this.check_have_big_win(bet, score) == false ) return;
    if (this.bigwin_sound != null) cc.Sound.PlaySound(this.bigwin_sound, false);
    this.bet = bet;
    this.node.active = true;
    this.bigwin_node.active = true;
    this.megawin_node.active = false;
    this.superwin_node.active = false;
    this.status = 0;
    this.end_value = score;
    this.show(0, score);
    return true;
  },

  tick_call(score) {
    this.now_score = score;

    cc.big_win_controller.check_have_super_win(score);
    cc.big_win_controller.check_have_mega_win(score);
    if (this.end_value <= score) cc.big_win_controller.close();
  },

  //顯示 Big Win 視窗
  show(start_value, end_value) {
    this.toggle_active_node(true);

    this.score_scroller.start_scroll(
      start_value,
      end_value,
      this.score_scroll_finish_callback,
      this,
      this.tick_call
    );
  },

  //關閉 Big Win 視窗
  close() {
    if (this.status > 2) return;
    this.status = 3;
    this.scheduleOnce(function () {
      this.node.active = false;
      cc.machine.payline.loop_show_one_payline();
    }, 3);
  },

  //滾分完後的事件
  score_scroll_finish_callback() {},
  //按鈕事件，加速
  speedup_click() {
    this.score_scroller.call_fast();
    /*
        this.schedule(function(){
            console.log("check close",[this.end_value , this.score_scroller.current_value ]);
            if ( this.end_value <= this.score_scroller.current_value ) this.close();
        },1,3,5); */
  },

  toggle_active_node(active) {
    this.node.active = true;
    this.mask_node.active = active;
  }
});
