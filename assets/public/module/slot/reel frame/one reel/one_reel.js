var minSpeedRate = 10; // 速度最小單位

/**
 * 滾輪狀態
 * up : 往上推
 * spining : 滾動中
 * focus : 聽牌中
 * down : 停輪往下壓在彈回來
 * stop : 完全靜止
 */
var state_type = { up: 0, spining: 1, focus: 2, down: 3, stop: -1 };
var eState = cc.Enum(state_type);
/**
 * one_reel
 * 單一滾輪區
 */
cc.Class({
  extends: cc.Component,

  properties: {
    ID: {
      default: 0,
      displayName: "輪盤編號"
    },

    Length: {
      default: 3,
      displayName: "直排個數"
    },

    HideCount: {
      default: 1,
      displayName: "隱藏幾個Symbol",
      tooltip: "設定本值數量1個，則上下端各再多藏3個",
      notify: function () {
        if (this.HideCount < 1) this.HideCount = 3;
      }
    },

    Speed: {
      default: 2000,
      displayName: "Spin速度"
    },

    Time: {
      default: 1000,
      displayName: "Spin時間(毫秒)"
    },

    "分隔線-進階設定": undefined,

    StartNode: {
      default: null,
      type: cc.Node,
      displayName: "Spin時啟動"
    },

    KeepRollingSound: {
      type: cc.AudioClip,
      default: null,
      displayName: "滾輪持續音效"
    },

    StopNode: {
      default: null,
      type: cc.Node,
      displayName: "停輪時啟動"
    },

    StopSound: {
      type: cc.AudioClip,
      default: null,
      displayName: "停輪音效"
    },

    SymbolReelStopDuration: {
      default: 0.5,
      displayName: "停輪動態持續時間"
    },

    "--- 聽牌設定 ---": undefined,

    notFocus: {
      default: false,
      type: cc.Boolean,
      displayName: "取消聽牌"
    },

    FocusNode: {
      default: null,
      type: cc.Node,
      displayName: "聽牌"
    },

    FocusSound: {
      type: cc.AudioClip,
      default: null,
      //type: cc.AudioClip,
      displayName: "聽牌音效"
    },

    FocusTime: {
      default: 3000,
      displayName: "聽牌時間"
    },

    FocusSpeed: {
      default: 0.8,
      displayName: "聽牌遞減速度"
    },

    FocusMinSpeed: {
      type: cc.Float,
      default: 90,
      displayName: "聽牌最小速度",
      tooltip: "建議不要小於 90，僅用於聽牌之最低速度"
    },

    "分隔線-力回饋設定": undefined,

    TweenUpValue: {
      default: 20,
      displayName: "上揚量"
    },

    TweenUpSpeed: {
      default: 1000,
      displayName: "上揚速度"
    },

    TweenUpDecrease: {
      default: 0.8,
      displayName: "上揚遞減"
    },

    TweenDownValue: {
      default: 10,
      displayName: "停輪下彈量"
    },

    TweenDownDecrease: {
      default: 0.7,
      displayName: "停輪下彈遞減"
    },

    "--- 排列帶效果 ---": undefined,
    Table: {
      default: [],
      type: [cc.Integer],
      displayName: "顯示排列帶",
      tooltip: "填入Symbol ID"
    },

    TableSize: {
      default: 0,
      displayName: "期望排列帶個數"
    },

    "--- 指定 Symbol 大小 ---": undefined,
    SymbolSize: {
      default: new cc.Size(),
      displayName: "指定本滾輪的 Symbol 大小"
    },

    ReelButton: {
      type: cc.Button,
      default: null,
      displayName: "停輪按鈕"
    },

    "--- 得分額外設定 ---": undefined,
    ShowPaylineDelay: {
      default: 0,
      displayName: "得分延遲顯示"
    }
  },

  /**
   * 初始轉輪設定
   * @todo
   * 當 reel_frame 載入後會呼叫，執行下列功能
   * 計算symbol起始與終點高度
   */
  preload() {
    // 預設狀態
    this.init_status();

    // 計算高度
    this.set_symbol_end_height();

    // 設置空的 symbol 資料
    this.allocate_symbol();

    // 隨機塞入symbol
    this.scheduleOnce(function () {
      this.random_symbol_fill();
    }, 0);
  },

  /*********************************************
   * 資料計算
   ********************************************/

  init_status() {
    this.state_func = {
      0: "tween_up",
      1: "spining",
      2: "tween_focus",
      3: "tween_down"
    };

    this.status = {
      state: eState.stop, // 目前滾輪狀態
      focus: false, // 是否是聽牌輪
      first_idx: -1 - this.HideCount,

      // 最下端的 Symbol
      last_idx: this.Length + this.HideCount - 1,
      max_symbol: 0,
      tween_up_value: 0, // 上抬量
      tween_up_speed: 0, // 上台速度
      call_stop: false, // 外部呼叫停輪
      tween_down_up: false, // 往下彈狀態，是否是回彈中
      tween_down_value: 0, // 力回饋量
      tween_down_speed: 0, // 速度
      reel_data: null, // 本次滾輪結果,
      reel_idx: 0 // 停輪前放入答案還有幾個要處理
    };

    this.update_dt = 0;
    // 滾輪最大 symbol 數量
    this.status.max_symbol = this.status.last_idx - this.status.first_idx + 1;
  },

  /**
   * 單輪停輪按鈕開關
   * @param {*} bool 開關
   */
  stop_button_active(bool) {
    if (this.ReelButton == null) return;
    return (this.ReelButton.interactable = bool);
  },

  /**
   * 從畫面按停止
   */
  push_stop_button() {
    return cc.machine.reel_frame.call_stop_reel(this.ID);
  },

  /**
   * 取得Symbol大小
   */
  symbol_size() {
    if (this.SymbolSize != null) return this.SymbolSize;
    return this.reel_frame.SymbolSize;
  },

  /**
   * 計算 symbol 滾下來後，最低的位置(超出可視範圍)
   * @from this.preload
   */
  set_symbol_end_height() {
    this.end_y = this.get_symbol_index_y(this.status.last_idx + 1);
    return this.end_y;
  },

  /**
   * 取得 Symbol 最低位置(超出可視範圍)
   */
  end_height() {
    return this.end_y;
  },

  /**
   * 查出 symbol 相對 indx 的 y 軸位置
   * @param {int} idx -1: 為可視範圍外(最上頭), 0: 開始是可視範圍內
   */
  get_symbol_index_y(idx) {
    var sym_h = this.symbol_size().height;
    var max = this.status.max_symbol;

    if (this.HideCount > 0) idx++;

    if (max < 4) {
      if (max % 2 == 0) {
        return -idx * sym_h;
      } else {
        // 單數
        return -idx * sym_h + sym_h / 2;
      }
    } else {
      if (max % 2 == 0) {
        return -(idx - 2) * sym_h;
      } else {
        // 單數
        return -(idx - 2) * sym_h + sym_h / 2;
      }
    }
  },

  /***************************
   * 功能區
   ****************************/

  set_symbol(index, sym_id) {
    this.remove_symbol(index);
    var sym = cc.lair.take_node(sym_id);

    this.symbol[index] = sym;

    sym.parent = this.node;
    sym.y = this.array_y[index];
    sym.x = 0;
    sym.active = true;
    sym.index = index;
    sym.link = null;

    return sym;
  },

  /**
   * 移除位置上的symbol
   * @param { Integer } index 位置代號
   */
  remove_symbol(index) {
    index = parseInt(index);
    if (this.symbol[index] == null) return;

    this.symbol[index].sym.remove();
    this.symbol[index].sym.active = false;
    this.symbol[index] = null;
  },

  /**
   * 放置指定的 symbol 在位置裡
   * @param {*} sym_id symbol 編號
   * @param {*} index 位置
   */
  put_symbol(sym_id, index) {
    index = parseInt(index);
    var id = cc.machine.call_controller("put_symbol_id", [this.ID, sym_id]);
    if (id != null && id != false) sym_id = id;
    var sym = cc.lair.take_node(sym_id);
    cc.machine.call_controller("put_symbol", [this.ID, sym]);
    if (sym == null) {
      var msg = "lair take node got null: id:" + sym_id;
      cc.notify_fail(msg);
      throw error(msg);
      return null;
    }
    sym.parent = this.node;
    sym.y = this.array_y[index];
    sym.x = 0;

    sym.active = true;
    this.symbol[index] = sym;
    sym.index = index;
    sym.link = null;

    return sym;
  },

  /**
   * 預設 symbol 放置區
   */
  allocate_symbol() {
    var empty = {};
    var allocate_y = {};

    // 正常數量
    for (var i = 0; i < this.Length; i++) {
      empty[i] = null;
      allocate_y[i] = this.get_symbol_index_y(i);
    }

    // 上面再多一個
    empty[-1] = null;
    allocate_y[-1] = this.get_symbol_index_y(-1);

    // 額外多得
    if (this.HideCount > 0) {
      for (var i = 0; i < this.HideCount; i++) {
        var idx1 = this.Length + i;
        var idx2 = -2 - i;
        empty[idx1] = null;
        empty[idx2] = null;

        allocate_y[idx1] = this.get_symbol_index_y(idx1);
        allocate_y[idx2] = this.get_symbol_index_y(idx2);
      }
    }

    this.symbol = empty;
    this.array_y = allocate_y;
  },

  /**
   * 把滾輪填滿隨機 symbol
   * @from this.preload
   */
  random_symbol_fill() {
    var _keys = Object.keys(this.symbol);
    for (var i = 0; i < _keys.length; i++) {
      var idx = _keys[i];
      // var _ranID = cc.machine.get_random_symbol();
      var _ranID = this.get_reel_symbol();
      this.put_symbol(_ranID, idx);
    }

    this.expansion_check(); // 大尺寸symbol修正
  },

  /****************************
   * 控制區
   *****************************/

  begin_focus() {
    // if ( this.notFocus == true ) return;

    // 如果有跑 call_stop 都縮回來
    this.status.reel_idx = this.status.last_idx;
    this.status.call_stop = false;
    // console.log(this.ID+'. begin focus');
  },

  /**
   * 開始滾動
   */
  do_spin() {
    this.status.state = eState.up;
    this.status.tween_up_value = 0;
    this.status.tween_up_speed = this.TweenUpSpeed;

    this.status.call_stop = false;

    this.status.tween_down_up = false;
    this.status.tween_down_value = 0; // 力回饋量
    this.status.tween_down_speed = this.Speed; // 速度
    this.status.reel_data = null;
    this.status.tween_focus_speed = this.Speed; // 聽牌遞減速度
    this.status.focus = false;

    this.node.big = 1;
    this.status.big_count = null;
    this.stop_button_active(true);
  },

  update_dt: 0,
  update(dt) {
    if (this.status == null) return;
    if (this.status.state == eState.stop) return;
    /*
        this.update_dt += dt;
        if ( this.update_dt < 0.02 ) return;
        this.update_dt -= 0.02;
        */
    var type = this.status.state;
    var func = this.state_func[type];
    this[func](dt);
  },

  /**
   * 滾輪動作
   * @param {*} dt update frame 每禎秒差
   */
  spining(dt) {
    if (this.status.state != eState.spining) return;

    var _speed = 0.0165 * this.Speed;

    for (var idx in this.symbol) {
      idx = parseInt(idx);

      if (this.symbol[idx] == null) continue;
      this.symbol[idx].y -= _speed;
    }

    var last_idx = this.status.last_idx;
    var last_symbol = this.symbol[last_idx];

    var step = last_symbol.y - this.end_height();
    if (step <= 0) return this.shift_symbol();
  },

  /**
   * 開始滾輪會往上抬一下
   * @param {*} dt
   */
  tween_up(dt) {
    if (this.status.state != eState.up) return;
    var dec = this.status.tween_up_speed * this.TweenUpDecrease; // 遞減量
    this.status.tween_up_speed = dec;
    if (this.status.tween_up_speed < minSpeedRate)
      this.status.tween_up_speed = minSpeedRate;
    var _speed = this.status.tween_up_speed * dt;

    this.status.tween_up_value += _speed;
    for (var idx in this.symbol) this.symbol[idx].y += _speed;

    if (this.status.tween_up_value >= this.TweenUpValue) {
      this.begin_spining();
    }
  },

  begin_spining() {
    this.status.state = eState.spining;
    if (this.KeepRollingSound != null && this.keepSound == null) {
      this.keepSound = cc.Sound.PlaySound(this.KeepRollingSound, true);
    }
  },

  // 停輪前的力回饋感
  tween_down(dt) {
    if (this.status.state != eState.down) return;

    // 回彈中
    if (this.status.tween_down_up == true) {
      this.status.tween_down_speed *= 1 + this.TweenDownDecrease;
      if (this.status.tween_down_speed < minSpeedRate)
        this.status.tween_down_speed = minSpeedRate * 4;
      if (this.status.tween_down_speed > this.Speed)
        this.status.tween_down_speed = this.Speed;

      var _speed = this.status.tween_down_speed * dt;
      if (this.status.tween_down_value < _speed)
        _speed = this.status.tween_down_value;

      for (var idx in this.symbol) this.symbol[idx].y += _speed;
      this.status.tween_down_value -= _speed;

      // 完全停止
      if (this.status.tween_down_value <= 0) {
        this.status.state = eState.stop;
        this.spin_end();
      }
    } else {
      // 下垂中
      this.status.tween_down_speed *= this.TweenDownDecrease;
      if (this.status.tween_down_speed < minSpeedRate)
        this.status.tween_down_speed = minSpeedRate * 4;
      var _speed = this.status.tween_down_speed * dt;
      for (var idx in this.symbol) this.symbol[idx].y -= _speed;
      this.status.tween_down_value += _speed;

      // 到底了，設為回彈
      if (this.status.tween_down_value > this.TweenDownValue) {
        this.begin_tween_up();
      }
    }
  },

  stop_keep_rolling_sound() {
    if (this.keepSound == null) return;
    cc.Sound.stop_sound_effect(this.keepSound);
    this.keepSound = null;
  },

  begin_tween_up() {
    this.status.tween_down_up = true;
    this.stop_keep_rolling_sound();
  },

  /**
   * 聽牌滾輪
   * @param {number} dt
   */
  tween_focus(dt) {
    if (this.status.state != eState.focus) return;
    this.stop_keep_rolling_sound();

    var _speed = dt * this.status.tween_focus_speed;
    this.status.tween_focus_speed *= this.FocusSpeed;
    var minSpeed = this.FocusMinSpeed;
    if (this.status.tween_focus_speed < minSpeed)
      this.status.tween_focus_speed = minSpeed;

    for (var idx in this.symbol) {
      idx = parseInt(idx);

      if (this.symbol[idx] == null) continue;
      this.symbol[idx].y -= _speed;
    }

    var last_idx = this.status.last_idx;
    var last_symbol = this.symbol[last_idx];

    var step = last_symbol.y - this.end_height();
    if (step <= 0) return this.shift_symbol();
  },

  count_big_symbol_idx() {
    if (this.node.big == null) return;
    if (this.node.big <= 1) return;

    if (this.status.big_count == null) this.status.big_count = this.node.big;
    this.status.big_count--;
    if (this.status.big_count < 0) this.status.big_count = this.node.big;
    // console.log("big_count", this.status.big_count);
  },

  /**
   * 滾動中的 symbol 滾進一個index
   * @from this.spining
   * @private
   */
  shift_symbol() {
    // 移除最後一個
    this.remove_symbol(this.status.last_idx);

    // 把其他 symbol index 推進一格
    var new_array = {};
    for (var idx in this.symbol) {
      var sym = this.symbol[idx];
      if (sym == null) continue;

      var next_index = parseInt(idx) + 1;
      new_array[next_index] = sym;
      sym.index = next_index;
    }
    this.symbol = new_array;

    // 增加第一格
    this.put_first();

    // 大號 symbol 修正
    this.expansion_check();

    // 大號 symbol 數量
    this.count_big_symbol_idx();

    // 差異修正
    var first_sym = this.symbol[this.status.first_idx];
    first_sym.y -= this.array_y[0] - this.symbol[0].y;

    if (first_sym.sym.RollingNode != null) {
      first_sym.sym.RollingNode.active = this.status.focus == false;
    }

    // 檢查是否能停輪了
    if (this.check_reel_begin_stop() == true) {
      // 檢查是否能停輪了
      // this.reel_frame.merge_stop_symbol_count(this.ID);
      if (this.status.focus == true) {
        this.status.state = eState.stop;
        this.spin_end();
      } else {
        this.status.state = eState.down; // 可以停了
        var symbols = this.query_symbol();
        var playSound = true; //為 false 則不播放停輪音效
        for (var idx in symbols) {
          if (symbols[idx].sym.StopSound != null) {
            //檢查此輪中否有擁有出場音效的 symbol 存在
            playSound = false;
            break;
          }
        }
        if (playSound == true && this.StopSound != null)
          cc.Sound.PlaySound(this.StopSound);
      }
    }
  },

  /**
   * 檢查是否可以開始停輪了
   */
  check_reel_begin_stop() {
    // if ( this.reel_frame.status.focus !== null ) return false; // 外面在聽牌
    if (this.status.call_stop == false) return false; // 還沒被叫到停，繼續轉
    // if ( this.status.reel_shift_idx > 0 ) return false; // 緩衝的沒塞完，繼續轉
    if (this.status.reel_data == null) return false; // 沒有設立答案，繼續轉
    // if ( this.status.reel_idx > this.status.first_idx ) return false;      // 答案還沒塞完，繼續轉
    if (this.status.reel_idx >= this.status.first_idx) return false; // 答案還沒塞完，繼續轉
    // if ( this.status.reel_shift_last > 0 ) return false;// (下)緩衝還沒塞完，繼續轉

    return true; // 可以停了
  },

  /**
   * 若Symbol有設置 expansion 大型 Symbol 則要隱藏被蓋到的symbol
   * @from shift_symbol
   * @from random_symbol_fill
   * @private
   */
  expansion_check() {
    var last = this.status.last_idx;
    var first = this.status.first_idx;
    var array = this.symbol;

    for (var i = last; i >= first; i--) {
      if (array[i] == null) continue;
      if (array[i].active == false) continue;
      var size = array[i].sym.Expansion;
      if (size.height == 1) continue;
      this.fix_expansion_symbol(array[i]);
    }
  },

  fix_expansion_symbol(symbol_node) {
    this.hide_expansion_symbol(symbol_node);
  },

  /**
   * 隱藏被expansion蓋到的symbol
   * @param {*} symbol_node 大symbol node
   * @from this.expansion_check()
   */
  hide_expansion_symbol(symbol_node) {
    var now_idx = symbol_node.index;
    var height = symbol_node.sym.Expansion.height;
    var from = now_idx - 1;
    var to = from - height + 2;
    var array = this.symbol;
    var linked = [];
    for (var i = from; i >= to; i--) {
      // console.log("hide idx:"+i);
      if (array[i] == null) continue;
      this.symbol_hide_link(array[i], symbol_node);
      linked.push(symbol_node);
    }

    symbol_node.linked = linked;
  },

  symbol_hide_link(target, from) {
    target.active = false;
    target.link = from;

    if (target.linked != null) {
      for (var idx in target.linked) {
        this.release_link(target.linked[idx]);
      }
    }
  },

  release_link(symbol) {
    symbol.link = null;
    symbol.active = true;
  },

  get_reel_symbol() {
    var sym = cc.machine.call_controller("get_reel_symbol", this.ID);
    if (sym != false && sym != null) return sym;

    if (this.Table != null && this.Table.length > 1) {
      var _ran = cc.machine.get_random(this.Table.length);
      return this.Table[_ran];
    }

    sym = cc.machine.get_random_symbol();
    return sym;
  },

  /**
   * 放入第一格
   * @private
   * @from this.shift_symbol();
   */
  put_first() {
    var first_idx = this.status.first_idx;
    this.symbol[first_idx] = null;

    if (this.status.call_stop == false) {
      // 還沒叫停, 繼續亂數轉
      return this.put_symbol(this.get_reel_symbol(), first_idx);
    }

    if (this.status.reel_data == null) {
      // 還沒拿到答案
      return this.put_symbol(this.get_reel_symbol(), first_idx);
    }

    var idx = this.status.reel_idx;
    var reel_data = this.status.reel_data;
    var sym_id = parseInt(reel_data[idx]);

    if (this.node.name === "0. One Reel") {
      cc.log("sym_id:", sym_id);
      cc.log("first_idx:", first_idx);
    }

    // if (this.ID == 0) console.log(this.ID+', rell_idx:'+this.status.reel_idx, sym_id);
    this.status.reel_idx--;
    return this.put_symbol(sym_id, first_idx);
  },

  put_reel_symbol(one_reel_sym) {
    // console.log(one_reel_sym);
    for (var idx in one_reel_sym) {
      var sym = one_reel_sym[idx];
      var node = this.set_symbol(idx, sym);
      //cc.machine.controller.put_symbol([this.ID, node]);
    }

    for (var idx in this.symbol) {
      var sym_node = this.symbol[idx];
      sym_node.sym.show_normal();
      cc.machine.controller.put_symbol([this.ID, sym_node]);
    }
  },

  /**
   * 呼叫所有symbol的出場音效
   */
  call_stop_sound() {
    var symbols = this.query_symbol();
    for (var idx in symbols) {
      if (symbols[idx].sym.StopSound == null) continue;
      cc.Sound.PlaySound(symbols[idx].sym.StopSound);
    }
  },

  /**
   * 呼叫停輪，外部控制呼叫本輪停輪
   * @todo 不會馬上停，會跑完把答案放完
   */
  call_stop() {
    this.stop_button_active(false);
    this.status.call_stop = true;
  },

  /**
   * 整個滾輪轉完畢停止後，呼叫的動作
   * @from this.tween_down
   */
  spin_end() {
    this.reset_symbol_position();
    this.call_stop_sound();
    this.show_reel_stop();
    this.focus_effect_active(false);
    this.reel_frame.one_reel_done(this.ID);
  },

  spin_stop() {
    this.reset_all_symbol();
    // this.reset_symbol_position();
    this.show_end_payline();
    this.focus_effect_active(false);
    this.status.state = eState.stop;
  },

  // 重置 symbol 位置
  reset_symbol_position() {
    for (var idx in this.symbol) {
      idx = parseInt(idx);
      this.symbol[idx].y = this.array_y[idx];
    }
  },

  reset_all_symbol() {
    for (var idx in this.symbol) {
      var sym_node = this.symbol[idx];
      sym_node.sym.show_normal();
      sym_node.sym.Expansion.height = 1;
      sym_node.active = true;
      sym_node.y = this.array_y[idx];
    }
  },

  /**
   * 設定滾輪
   * @param {*} reel_data
   * @from reel_frame.spin_response
   */
  set_reel(reel_data) {
    this.status.reel_data = this.fill_reel_data(reel_data);
    this.status.reel_idx = this.status.last_idx;
  },

  /**
   * 決定本排內容 ( 包含看不到的地方 )
   */
  fill_reel_data(reel_data) {
    var new_reel_data = reel_data;

    // 大圖標處理 只需要處理頭跟尾

    // 先處理頭
    var sym_id = reel_data[0];
    var sym = cc.machine.all_symbols_component[sym_id];

    if (sym != null && sym.is_big_symbol() == true) {
      // 是大圖標

      var height = sym.Expansion.height;
      // 往下檢查
      var down = 0;
      for (var j = 1; j < height; j++) {
        if (reel_data[j] != sym_id) break; // 不同就不處理
        down++;
      }

      height -= down;
      for (var j = 1; j < height; j++) {
        var idx = -j;
        new_reel_data[idx] = sym_id;
      }
    }

    // 在處理尾
    var sym_id = reel_data[reel_data.length - 1];
    var sym = cc.machine.all_symbols_component[sym_id];
    if (sym != null && sym.is_big_symbol() == true) {
      var last = reel_data.length - 1;
      var height = sym.Expansion.height;
      var down = 0;
      for (var j = 1; j < height; j++) {
        var idx = last - j;
        if (reel_data[idx] != sym_id) break;
        down++;
      }

      height -= down;
      for (var j = 1; j < height; j++) {
        var idx = last + j;
        new_reel_data[idx] = sym_id;
      }
    }

    // 剩下就放小圖標，塞好塞滿
    var min = this.status.first_idx;
    var max = this.status.last_idx + 1;

    for (var i = min; i < max; i++) {
      if (new_reel_data[i] != null) continue;
      new_reel_data[i] = parseInt(cc.machine.get_random_symbol(true));
    }

    return new_reel_data;
  },

  /**
   * 顯示得分功能
   * @param {*} symbol
   */
  symbol_show_payline(symbol, is_show_all_line) {
    if (symbol == null) return;
    // 將物件移動到得分層
    if (cc.machine.payline.ShowPaylineNode != null)
      cc.machine.move_node(symbol, cc.machine.payline.ShowPaylineNode);

    if (is_show_all_line) {
      if (symbol.sym == null) return;
      return symbol.sym.show_all_payline();
    }
    return symbol.sym.show_one_payline();
  },

  /**
   * 外部需求，指定 symbol 顯示中獎
   * @param {*} symbol_id
   */
  symbol_id_show_payline(symbol_id, is_show_all_line) {
    var symbols = this.query_symbol();

    for (var idx in symbols) {
      var symbol = symbols[idx];
      if (symbol.link != null) continue;
      if (symbol.sym == null) continue;
      if (symbol.sym.SymbolID == null) continue;
      var id = parseInt(symbol.sym.SymbolID);
      if (id != symbol_id) continue;
      this.symbol_show_payline(symbol, is_show_all_line);
    }
  },

  /**
   *
   * @param {*} idx 顯示位置
   */
  show_payline(idx, is_show_all_line) {
    var symbols = this.query_symbol();
    var symbol = symbols[idx];
    if (symbol == null) return;
    if (symbol.link != null) symbol = symbol.link;

    if (this.ShowPaylineDelay == 0)
      return this.symbol_show_payline(symbol, is_show_all_line);

    this.scheduleOnce(function () {
      return this.symbol_show_payline(symbol, is_show_all_line);
    }, this.ShowPaylineDelay);
  },

  /**
   * 取消得分顯示
   */
  show_end_payline() {
    for (var idx in this.symbol) {
      var symbol = this.symbol[idx];
      symbol.sym.show_normal();
      if (symbol.parent == this.node) continue;
      cc.machine.move_node(symbol, this.node);
    }
  },

  /**
   * 查詢 排列帶上的 symbol node
   * @return { array } symbol nodes
   */
  query_symbol() {
    var first_idx = this.status.first_idx;
    if (this.HideCount > 0) {
      first_idx += this.HideCount + 1;
    } else {
      first_idx += 1;
    }
    var max = first_idx + this.Length;
    var symbols = [];
    for (var i = first_idx; i < max; i++) {
      symbols.push(this.symbol[i]);
    }

    return symbols;
  },

  query_link_symbol() {
    var first_idx = this.status.first_idx;
    if (this.HideCount > 0) {
      first_idx += this.HideCount + 1;
    } else {
      first_idx += 1;
    }
    var max = first_idx + this.Length;
    var symbols = [];
    for (var i = first_idx; i < max; i++) {
      var target_symbol =
        this.symbol[i].active == true ? this.symbol[i] : this.symbol[i].link;
      symbols.push(target_symbol);
    }

    return symbols;
  },

  /**
   * 停輪後，此輪symbol中含有出場動態，即顯示
   * @from this.spin_end
   */
  show_reel_stop() {
    var symbols = this.query_symbol();
    var count = 0;
    for (var idx in symbols) {
      if (symbols[idx].sym.RollingNode)
        symbols[idx].sym.RollingNode.active = false;
      if (symbols[idx].sym.ReelStopNode == null) continue;
      count++;
      symbols[idx].sym.show_reel_stop();
    }
    this.show_end_payline();

    if (count == 0) return;
    this.scheduleOnce(this.end_reel_stop, this.SymbolReelStopDuration);
  },

  /**
   * 關閉停輪動態
   * @from this.show_reel_stop
   */
  end_reel_stop() {
    var symbols = this.query_symbol();
    for (var idx in symbols) {
      if (symbols[idx].sym.ReelStopNode == null) continue;
      symbols[idx].sym.ReelStopNode.active = false;
    }
  },

  /**
   * 執行聽牌
   */
  call_focus() {
    // if ( this.status.state != eState.spining ) return;
    if (this.notFocus == true) return this.call_stop();
    this.status.state = eState.focus;
    this.status.focus = true;
    this.status.tween_down_speed *= this.FocusSpeed;
    this.focus_effect_active(true);

    for (var idx in this.symbol) {
      if (this.symbol[idx].sym.RollingNode)
        this.symbol[idx].sym.RollingNode.active = false;
    }
    this.call_stop();
  },

  /**
   *
   * @param {*} bool
   */
  focus_effect_active(bool) {
    if (bool == true) {
      if (this.FocusNode != null) this.FocusNode.active = true;
      if (this.FocusSound != null) cc.Sound.PlaySound(this.FocusSound);
    } else {
      if (this.FocusNode != null) this.FocusNode.active = false;
    }
  }
});
