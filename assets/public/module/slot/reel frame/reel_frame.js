var eState = cc.Enum({
  // 停止中
  stop: 0,

  // 滾輪中
  spining: 1,

  // 拿到Server結果，準備停輪
  begin_stop: 2
});

cc.Class({
  extends: cc.Component,

  properties: {
    SymbolSize: {
      default: new cc.Size(0, 0),
      displayName: "預設 Symbol 大小"
    },

    Reels: {
      type: [cc.Node],
      default: [],
      displayName: "滾輪帶"
    },

    MinSpinTime: {
      default: 300,
      displayName: "最低滾輪需時(毫秒"
    },

    AllReelDoneContinueTime: {
      default: 0.1,
      displayName: "停輪後停滯畫面多久"
    }
  },

  /**
   * 將滾輪 component 整理出來，並且通知 preload
   */
  load_reels() {
    this.reel = [];
    this.reel_size = [];

    this.status = {
      state: eState.stop, // 目前spin狀態
      spining: {}, // 個別轉輪狀態 { id: false, }
      spin_time: {}, // 停輪所需時間 { id: time, }
      //last_timestamp: 0,      // 上次處理停輪判斷的時間
      focus: false, // 聽牌中 { boolean }
      callstop: {}, // 叫停中滾輪
      //begin_timestamp: 0,     // 開始 Spin 的時間紀錄，用於滾輪最少滾一秒的檢查
      reel_data: null, // 滾輪結果資料
      stop_symbol_count: {}, // 已經停輪的 symbol 內容
      force_stop: false
    };

    for (var i = 0; i < this.Reels.length; i++) {
      var _node = this.Reels[i];
      if (_node == null) continue;

      var _com = _node.getComponent("one_reel");
      if (_com == null) continue;

      _com.reel_frame = this;
      this.reel.push(_com);

      _com.preload();
      this.reel_size.push(_com.Length);
    }
  },

  /**
   * 檢查資料是否有誤
   * @from this.start()
   */
  smart_check() {
    if (this.SymbolSize.width < 1)
      return cc.notify_fail("(ReelFrame) Symbol 寬度設置錯誤");
    if (this.SymbolSize.height < 1)
      return cc.notify_fail("(ReelFrame)Symbol 高度設置錯誤");
    if (this.Reels.length < 1) return cc.notify_fail("(ReelFrame)未設置滾輪區");
  },

  start() {
    this.smart_check();
    this.scheduleOnce(function () {
      this.load_reels();
    }, 0.1);
  },

  update(dt) {
    if (this.status == null) return;
    if (this.status.state == eState.stop) return;
    this.begin_stop(dt);
  },

  /**
   * 從 machine 控制，開始spin
   */
  start_spin() {
    this.status.spining = {};
    this.status.spin_time = {};
    this.status.call_stop = {};
    this.status.stop_symbol_count = {};

    for (var id in this.reel) {
      var reel = this.reel[id];

      this.status.spining[id] = true; // 先設為 true, 停輪後為 false;
      this.status.spin_time[id] = reel.Time; // 轉輪時間控制
      this.status.callstop[id] = false;
      reel.do_spin();
    }

    this.status.state = eState.spining;
    this.status.focus = -1;
    this.status.force_stop = false;
  },

  /**
   * 定時呼叫停輪
   */
  begin_stop(dt) {
    if (this.status.state == eState.stop) return;

    for (var id in this.status.spin_time) {
      if (this.status.spining[id] == false) continue;
      if (this.status.state == eState.begin_stop)
        this.status.spin_time[id] -= 1000 * dt;
      if (this.status.focus != -1) continue;
      if (this.status.callstop[id] == true) continue;
      if (this.status.spin_time[id] >= 0) continue;
      this.call_stop_reel(id);
    }
  },

  /**
   * 呼叫停輪
   * @param {*} id 指定的輪盤編號
   * @param { bool } force 強制停輪(不作聽牌)
   */
  call_stop_reel(id, force = false) {
    if (this.status.spining[id] == false) return; // 已經停了
    if (this.status.callstop[id] == true) return; // 叫過了
    if (this.status.focus != -1) return; // 聽牌中

    // 檢查是否中斷處理
    if (cc.machine.one_reel_done_break(id) == true) return false;

    this.status.force_stop = force;
    this.reel[id].call_stop();
    this.status.spin_time[id] = 0;
    this.status.callstop[id] = true;

    return true;
  },

  call_focus_reel(reel_id) {
    this.reel[reel_id].call_focus();
    return true;
  },

  call_focus_point() {
    // 先看看要不要聽牌
    if (this.check_focus_active() == false) return false;
    var call_stop_id = -1,
      min_stop_id = -1;
    // 找出正在停的滾輪
    for (var id in this.status.spining) {
      if (this.status.spining[id] == false) continue; // 已經停輪的不處理
      this.reel[id].begin_focus();
      if (min_stop_id == -1) min_stop_id = id;
      if (this.status.callstop[id] == false) continue;
      if (call_stop_id == -1) call_stop_id = id;
    }

    // 如果沒有正在停的, 找最小的編號滾輪
    if (call_stop_id == -1) call_stop_id = min_stop_id;
    this.status.focus = call_stop_id;
    return this.call_focus_reel(call_stop_id);
  },

  is_all_reel_stop() {
    for (var id in this.status.spining) {
      if (this.status.spining[id] == true) return false;
    }

    return true;
  },

  /**
   * 單一滾輪已經停輪
   * @param {*} reel_id 滾輪編號
   * @from one_reel
   */
  one_reel_done(reel_id) {
    this.status.spining[reel_id] = false;
    this.merge_stop_symbol_count(reel_id);

    cc.machine.call_controller("one_reel_done", reel_id);

    if (this.is_all_reel_stop() == false) {
      this.call_focus_point(); // 聽牌處理
      return;
    }
    return this.all_reel_done(); // 全部已停輪
  },

  /**
   * 全部停輪
   * @from this.one_reel_done
   */
  all_reel_done() {
    this.status.state = eState.stop;
    if (this.AllReelDoneContinueTime > 0)
      return this.scheduleOnce(function () {
        cc.machine.all_reel_done();
      }, this.AllReelDoneContinueTime);
    return cc.machine.all_reel_done();
  },

  // 全部急停
  call_stop_all() {
    for (var id in this.status.callstop) {
      this.call_stop_reel(id, true);
    }
  },

  /**
   * 檢查是否要進行聽牌
   * @param {json} reel_data 盤面結果
   * @return {boolean} true : false
   */
  check_focus_active() {
    if (cc.machine.status.bonus_gaming == true) return false;
    if (this.status.force_stop == true) return false;
    if (this.status.focus != -1) return true;
    if (
      cc.machine.call_controller(
        "reel_frame_check_focus",
        cc.machine.focus_symbol
      ) == true
    )
      return true;
    if (cc.machine.focus_symbol == null) return false;

    for (var sym in cc.machine.focus_symbol) {
      var focus_count = cc.machine.focus_symbol[sym];
      if (this.status.stop_symbol_count[sym] == null) continue;
      if (focus_count != this.status.stop_symbol_count[sym]) continue;

      return true;
    }

    return false;
  },

  /**
   * 停輪時，紀錄已停輪的 symbol 個數
   * @param {*} reel_id
   * @todo stop_symbol_count用於處理聽牌效果的開關
   */
  merge_stop_symbol_count(reel_id) {
    var stop_symbol_count = this.status.stop_symbol_count;
    var reel_data = this.status.reel_data[reel_id];
    if (reel_data == null) return;
    for (var idx = 0; idx < reel_data.length; idx++) {
      var sym = reel_data[idx];
      if (stop_symbol_count[sym] == null) stop_symbol_count[sym] = 0;
      stop_symbol_count[sym]++;
    }

    this.status.stop_symbol_count = stop_symbol_count;
  },

  /**
   * 取得 Server 結果，開始停輪
   * @param {json} reel_data
   */
  spin_response(reel_data) {
    this.status.reel_data = reel_data; // 先設定
    this.status.state = eState.begin_stop; // 設定狀態

    for (var idx in this.reel) {
      var data = [].concat(reel_data[idx]);
      this.reel[idx].set_reel(data);
    }

    if (cc.machine.status.dont_quick_stop == true) return;
    this.call_stop_all();
  },

  /**
   * 播放得獎特效
   * @param { json } payline { count: 2, line:[2, 1, 0, 1, 2], odds: 600, score: 300, symbol: 5 }
   * @param {boolean} show_score 是否顯示單線分數
   * @param {boolean} is_show_all_line 是否為全線播放
   */
  show_payline(payline, show_score, is_show_all_line) {
    if (payline == null) return;
    if (payline["reel"] != null)
      return this.show_payline_way(payline, show_score, is_show_all_line);
    return this.show_payline_line(payline, show_score, is_show_all_line);
  },

  /**
   * 依照中獎位置，播放特效
   * @param {*} payline
   * @param {*} show_score
   * @param {*} is_show_all_line
   */
  show_payline_way(payline, show_score, is_show_all_line) {
    for (var reel in payline["reel"]) {
      for (var idx in payline["reel"][reel]) {
        if (payline["reel"][reel][idx] == null) continue;
        this.reel[reel].show_payline(idx, is_show_all_line);
      }
    }

    if (show_score) this.show_way_line_score(payline);
  },

  show_way_line_score(payline) {
    var score_label = cc.machine.payline.ShowOneLineScoreLabel;
    var first = -1;
    for (var idx in payline["reel"][0]) {
      if (payline["reel"][0][idx] == null) continue;
      first = idx;
      break;
    }

    var symbol = this.reel[0].query_symbol()[first];

    GW.move_node(score_label.node, symbol);

    score_label.node.x = 0;
    score_label.node.y = 0;
    score_label.string = payline["score"];
  },

  /**
   * 依照中獎線，播放得獎特效
   * @param {*} payline
   * @param {*} show_score
   * @param {*} is_show_all_line
   */
  show_payline_line(payline, show_score, is_show_all_line) {
    var count = payline["count"] + 1;
    var lines = payline["line"];

    for (var i = 0; i < count; i++) {
      if (lines[i] == null) break;
      this.reel[i].show_payline(lines[i], is_show_all_line);
    }

    if (show_score) this.show_one_line_score(payline);
  },

  /**
   * 顯示單線分數
   * @param {*} payline
   */
  show_one_line_score(payline) {
    var score_label = cc.machine.payline.ShowOneLineScoreLabel;
    var symbol_idx = payline["line"][0];
    var reel_idx =
      payline["reverse"] == null || payline["reverse"] == false
        ? 0
        : this.reel.length - 1;
    var symbol = this.reel[reel_idx].query_symbol()[symbol_idx];

    cc.machine.move_node(score_label.node, symbol);

    score_label.node.x = 0;
    score_label.node.y = 0;
    score_label.string = payline["score"];

    //為大 symbol 則判斷 index 是否位於此輪下方不可視範圍，若為是則將分數 y 拉高一個 symbol 高度。
    if (symbol.sym.Expansion.height < 2 || symbol.index + 1 == symbol_idx)
      return;
    score_label.node.y = symbol.height * (symbol.sym.Expansion.height - 1);
  },

  /**
   * 結束播放得分，調整恢復盤面
   */
  show_end_payline() {
    for (var idx in this.reel) {
      this.reel[idx].show_end_payline();
    }
    cc.machine.payline.ShowOneLineScoreLabel.string = "";
  },

  /**
   * 指定單一 symbol 編號，做得分效果
   * @param {number} symbol_id
   * @param {boolean} is_show_all_line
   */
  symbol_id_show_payline(symbol_id, is_show_all_line) {
    for (var idx in this.reel) {
      this.reel[idx].symbol_id_show_payline(symbol_id, is_show_all_line);
    }
  }
});
