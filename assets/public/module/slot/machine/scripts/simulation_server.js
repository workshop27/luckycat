cc.Class({
    extends: cc.Component,

    properties: {
        BetList : {
            type: [cc.Float],
            default: [],
            //default: [0.1,0.2,0.5,1,2,5,10,20,50,100],
            tooltip: "BetList 押注額列表",
        },
    },

    onLoad () {
        this.Balance = 5000,
        cc.Server = this;
        this.init();
    },

    enter_game() {
        var packet = {
            Bet: this.BetList,
            TotalLines : 40,
            User: {
                Balance:5000,
            }
        };
        cc.machine.enter_game_response(packet); // 回傳給Client
    },

    /**
     * 一個滾輪的亂數位置取得
     * @param { int[] } wheel 排列帶
     * @param { int } count 取得數量
     * @return { int[] } reel_frame 結果
     */
    random_reel_frame(wheel, count) {
        if ( wheel == null ) return null;
        if ( count < 1 ) return null;

        let len = wheel.length;
        let idx = Math.floor(Math.random()*len);
        var dwheel = wheel.concat(wheel);
        var one_reel_frame = [];

        for(let i=0;i<count;i++) {
            let symbol = dwheel[idx+i];
            one_reel_frame = one_reel_frame.concat([symbol]);
        }

        return one_reel_frame;
    },

    make_reel_fame(reelCombination) {
        var ReelCombination = this[reelCombination];
        var reelSize = this.ReelSize;
        if ( this.freeGameMode == true ) reelSize = this.FreeReelSize;

        var reel_frame = new Array(reelSize.length);
        for(let i=0;i<reelSize.length;i++) {
            let len = reelSize[i];
            let wheel = ReelCombination[i];
            var one_reel_frame = this.random_reel_frame(wheel, len);
            reel_frame[i] = one_reel_frame;
        }

        return reel_frame;
    },



    spin(total_bet) {
        // 先製作一般spin
        var result = this.make_normal_spin();

        // 檢查是否有中BonusGame
        result = this.check_bonus_game(result);
        console.log("spin",[this.Balance,total_bet,result.result['total_score']]);
        this.Balance = Math.floor((this.Balance * 1000) - (total_bet * 1000) + (result.result['total_score'] * 1000))/1000;

        result.result.last_credit = this.Balance;

        var data = {"data":JSON.stringify(result)};

        this.scheduleOnce(function(){
            cc.machine.receive_package("spin_response", data);
        },1);
        
    },

    /**
     * 檢查是否有進入BonusGame
     * @param {*} result 
     */
    check_bonus_game(result) {
        
        var symbol_data = result['result']["all_round_data"][0]['symbol'];

        var keys = Object.keys(symbol_data);
        for(let idx in keys) {
            var sym = keys[idx];
            if ( this.symbol[sym] == null ) continue;
            if ( this.symbol[sym]['type'] == null ) continue;
            
            var bonus_module = this.symbol[sym]['type'];
            if ( this[bonus_module] == null ) continue;
            
            var tmp = this[bonus_module](sym, symbol_data[sym], result);
            if ( tmp != null ) return tmp;
        }

        return result;
    },

    /**
     * 製作一次一般spin
     */
    make_normal_spin() {
        var result = {
            "result" : {
                "all_round_data" : {
                    0 : {},
                },

                "bet" : 1,
                "last_credit" : 0,
                "round_count" : 1,
                "sn" : "12345678",
                "total_score" : 0,
                "type" : "normal",
            },
        };

        result['result']["all_round_data"][0] = this.make_spin('ReelCombination');
        result['result']['total_score'] = result['result']["all_round_data"][0]['total_score'];
        return result;
    },

    //event_make_spin(spin_result) { return null; },

    /**
     * 
     * @param {*} reelCombination // 排列帶名稱
     */
    make_spin(reelCombination='ReelCombination') {

        if (this['break_make_spin'] != null) {
            var tmp = this['break_make_spin'](reelCombination);
            if ( tmp != null ) return tmp;
        }

        var reel_frame = this.make_reel_fame(reelCombination);
        var symbol_data = this.reckon_symbol_data(reel_frame);
        var payline_data = this.reckon_payline_data(symbol_data);
        var total_score = this.reckon_total_score(payline_data);
        var spin_result = {
            "payline" : payline_data,
            "reel_frame" : reel_frame,
            "symbol": symbol_data,
            "total_score" : total_score,
        };

        var tmp = this.event_make_spin(spin_result);
        if ( tmp === null ) return spin_result;
        return tmp;
    },

    /**
     * 計算總分
     * @param {*} payline 
     */
    reckon_total_score(payline) {
        var keys = Object.keys(payline);
        var total_score = 0;
        for(let idx in keys) {
            var key = keys[idx];
            var score = payline[key]['score'] * 1000;

            total_score += score;
        }

        return total_score / 1000;
    },
    
    /**
     * 整理 symbol 資料
     * @param {*} reel_frame 盤面資料 
     * @return {*} symbol data
     */
    reckon_symbol_data(reel_frame) {
        var symbol_data = {};
        for(let i=0;i<reel_frame.length;i++) {
            for(let j=0;j<reel_frame[i].length;j++) {
                var sym = reel_frame[i][j];
                symbol_data[sym] = this.reckon_one_symbol_data(sym,reel_frame);
            }
        }

        return symbol_data;
    },

    /**
     * 整理單一 symbol 資料
     * @param {*} sym 
     * @param {*} reel_frame 
     */
    reckon_one_symbol_data(sym, reel_frame) {
        var sym_data = { 
            'reel': new Array(reel_frame.length), 
            'count' : new Array(reel_frame.length),
        };
        for(let i=0;i<reel_frame.length;i++) { 
            sym_data['reel'][i] = new Array(reel_frame[i].length);
            sym_data['count'][i] = 0;

            for(let j=0;j<reel_frame[i].length;j++) {
                sym_data['reel'][i][j] = false;

                if ( this.check_seem_symbol(sym, reel_frame[i][j]) ) {
                    sym_data['reel'][i][j] = true;
                    sym_data['count'][i]++;
                } 
            }
        }

        return sym_data;
    },

    check_seem_symbol(sym, target) {
        // console.log("checj_seem_symbol", [sym, target]);
        if ( sym === target ) return true;
        // console.log("checj_seem_symbol step 1");
        if ( target != this.symbol.wild ) return false;
        // console.log("checj_seem_symbol step 2");
        if ( this.symbol[sym] == null ) return false;
        // console.log("checj_seem_symbol step 3");
        if ( this.symbol[sym]['score'] == null ) return false;
        // console.log("checj_seem_symbol step 4");
        if ( this.symbol[sym]['type'] == null ) return true;
        // console.log("checj_seem_symbol step 5");

        return false;
    },

    // 合併兩個json
    json_merge(arr1, arr2) {
        if ( arr2 == null ) return arr1;

        keys = Object.keys(arr2);
        if ( keys.length == 0 ) return arr1;

        var new_array = {};

        for(var idx in keys) {
            key = keys[idx];
            data = arr2[key];
            new_array[key] = data;
        }

        keys = Object.keys(arr1);
        for(var idx in keys) {
            key = keys[idx];
            data = arr1[key];
            new_array[key] = data;
        }

        return new_array;
    },

    /**
     * 整理中線資料
     * @param {*} reel_frame 
     */
    reckon_payline_data(symbol_data, otherline=null) {
        var payline = {};
        var keys = Object.keys(symbol_data);
        // console.log(keys, symbol_data);
        for(var idx in keys) {
            var symbol = keys[idx];
            // console.log(idx, symbol, keys);
            payline = this.reckon_symbol_payline_data(symbol, symbol_data[symbol], payline, otherline);
        }

        return payline;
    },

    /**
     * 
     * @param {*} sym 
     * @param {*} reel_frame 
     */
    reckon_symbol_payline_data(sym, one_symbol_data, line_payline, otherline=null) {
        if (this.symbol[sym] == null) return line_payline;

        var score_line = this.symbol[sym].score;
        if ( score_line == null ) return line_payline;

        var score = 0;
        var count;
        var line;
        var reelLine = this.ReelLine;
        if ( otherline != null && this[otherline] != null) {
            reelLine = this[otherline];
        }
        for(let _line=0;_line<reelLine.length; _line++) {
            for(let x=0;x<reelLine[_line].length;x++) {
                let y = reelLine[_line][x];
                if ( one_symbol_data.reel[x][y] != true ) break;
                score = score_line[x];
                count = x;
                line = _line;
            }

            if ( score > 0 ) {
                // console.log(line, this.ReelLine[_line]);
                line_payline[line] = {
                    'count'  : count, 
                    'amount' : one_symbol_data['count'],
                    'line'   : reelLine[line],
                    'symbol' : sym,
                    'score'  : score,
                    'odds'   : score,
                    'number' : sym,
                    'name'   : this.symbol[sym].name,
                };
            }
        }
        return line_payline;
    },


    /**
     * 模擬 Server 收到封包
     * @param {} command 
     * @param {*} packet 
     */
    receive_package(command, packet) {
        if ( this[command] == undefined ) return;
        this[command](packet);
    },
});
