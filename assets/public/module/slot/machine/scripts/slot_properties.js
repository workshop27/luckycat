cc.Class({
    extends: cc.Component,

    properties: {
        /**
         * 與伺服器確認遊戲內容模組
         */
        GameName: {
            displayName: "遊戲模組名稱 ModuleID",
            default : "",
        },

        Name: {
            displayName: "遊戲英文名稱",
            default: "",
        },

        SpinSound : {
            type : cc.AudioClip,
            default: null,
            displayName: "Spin音效",
        },

        /**
         * 遊戲規則模組，例如是算線、算位、或是其他特殊規則
         */
        PayLines : {
            type: cc.Node,
            displayName: "得分規則模組 Payline",
            default : null,
        },

        /**
         * 滾輪盤
         */
        ReelFrame : {
            type: cc.Node,
            displayName : "主要滾輪物件 ReelFrame",
            default : null,
        },

        /**
         * 控制列
         */
        ControllBar : {
            type : cc.Node,
            displayName : "操作控制區 ControllBar",
            default : null
        },

        /**
         * Symbol 物件群
         */
        SymbolParentNode: {
            type: cc.Node,
            displayName: "Symbols Node Parent",
            default : null,
        },

        /**
         * Big Win 控制
         */
        BigWinControl:{
            type: cc.Node,
            displayName: "Big Win Control",
            default : null,
        },

        /**
         * 本遊戲的其他設置
         */
        MachineController: {
            type: cc.Node,
            displayName: "遊戲控制模組",
            default: null,
            tooltip : "客製化本遊戲的程式部分",
        },

        ControllerComponentName: {
            default: "",
            displayName: "控制模組的 Component Name",
        },
        
    },

    // other event

    // JP plugin
    jp_response(response) { return this.call_controller('jp_response', response); },

    /*************************
     * Controller break
     *************************/

    all_reel_done_break() {
        if ( this.call_controller('all_reel_done_break') == true ) return true;
        return false;
    },

    /**
     * 決定是否中斷，在按下spin後的動作
     * @param {*} state 目前狀態
     */
    push_spin_button_break(state) {
        if ( this.call_controller('push_spin_button_break', state) == true )  return true;
        return false;
    },

    /**
     * 決定是否拿到 Server 回應後中斷程式往下執行 (中斷給滾輪答案、滾輪沒拿到答案不會停輪)
     * @param { json } result Server回應內容
     */
    spin_response_break(result) {
        if ( this.call_controller('spin_response_break', result) == true ) return true;
        if ( this.payline.spin_response_break(result) == true ) return true;
        return false;
    },

    /**
     * 
     */
    all_reel_done_break() {
        if ( this.call_controller('all_reel_done_break') == true) return true;
        return false;
    },

    /**
     * 決定是否要中斷單一滾輪停止
     * @param { int } reel_id 轉輪編號
     * @from payline.callstop
     * @return true 中斷目前編號的停輪
     * @return false 繼續停輪
     */
    one_reel_done_break(reel_id) {
        if ( this.call_controller('one_reel_done_break', reel_id) == true ) return true;
        if ( this.payline.one_reel_done_break(reel_id) == true ) return true;
        return false;
    },

     /**
      * 決定是否要中斷全線播放
      * @param { json } round_data 得分資料
      * @return { boolean } true 中斷全線播放
      * @from payline.show_all_payline
      */
    show_all_payline_break(round_data) {
        if ( this.call_controller('show_all_payline_break') == true ) return true;
        return false;
    },

     /**
      * 決定是否中斷單線播放
      * @param { number } line 線號
      * @param { json } payline 得分資料
      * @param { boolean } show_score 是否顯示單線分數
      */
    show_one_payline_break(line, payline, show_score) {
        if ( this.call_controller('show_one_payline_break') == true ) return true;
        return false;
    },

     /**
      * 決定是否中斷 spin 執行
      * @return { boolean } 中斷 spin 執行
      */
     spin_break() { 
        if ( this.call_controller('spin_break') == true ) return true; 
        return false;
    },

    /**
     * 是否中斷，回到遊戲初始
     */
    back_to_normal_break() {
        if ( this.call_controller('back_to_normal_break') == true ) return true;
        return false;
    },

    check_bonus_game_break() {
        if ( this.call_controller('check_bonus_game_break') == true ) return true;
        return false;
    },

    spin_assign_round_data_break() {
        if ( this.call_controller('spin_assign_round_data_break') == true ) return true;
        return false;
    },
    

    /********************************************
     * Controller event
    *********************************************/
    event_spin() { return this.call_controller('spin'); },
    event_init() { return this.call_controller('init'); },
    event_enter_game_response(data) { return this.call_controller('enter_game_response', data); },
    event_register_protocol() { return this.call_controller('register_protocol'); },
    event_push_spin_button(state) { return this.call_controller('push_spin_button',state); },
    event_spin_response(result) { return this.call_controller('spin_response',result); },
    event_before_all_reel_done() { return this.call_controller('before_all_reel_done'); },
    event_all_reel_done() { return this.call_controller('all_reel_done'); },
    event_back_to_normal() { return this.call_controller('back_to_normal'); },
    event_check_bonus_game() { return this.call_controller('check_bonus_game'); },
    event_spin_assign_round_data() { return this.call_controller('spin_assign_round_data'); },
    event_start_auto_spin() { return this.call_controller('start_auto_spin'); },
    event_check_auto_spin() { return this.call_controller('check_auto_spin'); },
    event_stop_auto_spin() { return this.call_controller('stop_auto_spin'); },
    event_spin_credit_not_enough() { return this.call_controller('spin_credit_not_enough'); },

    /** payline **/
    event_show_all_payline(round_data) { return this.call_controller('show_all_payline', round_data); },
    event_show_one_payline(payline) { return this.call_controller('show_one_payline', payline); },

    /** bet */
    event_change_bet (bet_idx) { return this.call_controller('change_bet', bet_idx); },

    
});
