var properties = require("./slot_properties");
var common = require("./slot_common");

var machine_state = { Lock: 999, Normal:0, Spin:1, SpinResponse:2, CallStop:3, Payline:4,  };
var mState = cc.Enum(machine_state);

var machine_type = { Normal:null, LockSpin:0x01, LockAll:0x02, };
var eType = cc.Enum(machine_type);

var user = {
    credit : 50000,

    refresh(value) {
        // console.log(value);
        cc.machine.call_control_bar("set_balance", value);
    },
};

cc.Class({
    extends: properties,
    
    get_random(length) {
        return Math.floor(Math.random() * length);   
    },

    /**
     * 取得亂數 symbol 編號
     * @param { bool } small 是不是小圖標
     */
    get_random_symbol(small=false) {
        var allsymbol;
        if ( small == true ) {
            allsymbol = this.small_symbols;
        } else {
            allsymbol = this.normal_symbols;
        }
        
        var _ran = this.get_random(allsymbol.length);
        var _id = allsymbol[_ran];
        return _id;
    },

    /**
     * 檢查machine設定資料是否正確
     * @from this.init()
     */
    smart_check() {
        //if ( this.GameName == "" ) return cc.notify_fail("沒有設定遊戲模組 (GameName) 設定, 請檢查相關設定");

        if ( this.PayLines == null ) return cc.notify_fail("沒有設定得分規則模組 (PayLines) 設定, 請檢查相關設定");
        if ( this.load_component(this.PayLines, 'payline') == false ) return false;

        if ( this.ReelFrame == null ) return cc.notify_fail("沒有設定主要滾輪物件 (ReelFrame) 設定, 請檢查相關設定");
        if ( this.load_component(this.ReelFrame, 'reel_frame') == false ) return false;

        if ( this.ControllBar == null ) return cc.notify_fail("沒有設定操作控制區 (ControllBar) 設定, 請檢查相關設定");
        if ( this.load_component(this.ControllBar, 'slot_control_bar') == false ) return false;

        if ( this.SymbolParentNode == null ) return cc.notify_fail("沒有設定Symbol物件群 (SymbolParentNode) 設定, 請檢查相關設定");
        
        //if ( this.BigWinControl == null ) return cc.notify_fail("沒有設定Big Win控制 (BigWinControl) 設定, 請檢查相關設定");
        //if ( this.load_component(this.BigWinControl, 'bigwin_controller') == false ) return false;
        return true;
    },

    /**
     * 模擬呼叫 Server 功能
     * @param {*} command 
     * @param {*} packet 
     */
    call_server_function(command, packet) {
        if ( command == null ) return;

        cc.Server.receive_package(command, packet);
    },

    /**
     * 載入模組
     * @param {*} p_node 
     * @param {*} p_comName 
     */
    load_component(p_node, p_comName) {
        if ( p_node == null ) return cc.notify_fail("無法取得模組所屬物件, 請檢查相關設定:"+p_comName);

        var _component = p_node.getComponent(p_comName);
        if ( _component == null ) return cc.notify_fail("無法取得模組"+p_comName+", 請檢察相關設定:"+p_node.uuid);

        this[p_comName] = _component;
        _component['machine'] = this;

        if ( _component['preload'] != undefined ) _component.preload();
        return true;
    },

    init() {
        if ( this.smart_check() != true ) return; // 初始資料檢查與設定

        this.register_machine(); // 定義 machine 就是這台
        this.init_status();      // 資料初始化
        this.load_symbol();      // Symbol 設定
        // this.event_init();
    },

    start() {
        this.init(); // 初始化設定
        this.scheduleOnce(function(){ // 假裝連上Server
            cc.Server.enter_game();
        },0.1);
    },

    init_status() {
        this.mState = mState;

        this.status = {
            state           : mState.Lock,   // 收到 enter_game 才會打開
            info            : null,          // enter_game 會收到的內容
            type            : eType.LockAll, // 狀態 normal, busy
            round_data      : null,          // Spin 結果會存放在這
            callstop        : false,         // 急停
            bonus_done      : false,         // 是否執行完 bonus
            bonus_gaming    : false,         // 是否正在執行 bonus
            bonus_module    : null,          // 正在進行的 bonus module
            round_idx       : 0,             // 目前執行到第幾盤
            auto_spin_times : 0,             // 目前剩餘的auto spin次數
            free_spin_times : 0,             // 目前剩餘的free spin次數
            retrigger_times : 0,             // 額外獲得的free spin次數
            dont_quick_stop : true,          // 快速停止模式
            now_bet_idx     : 0,             // 目前押注額位置
            bet_list        : [],            // 押注額數列 
            total_lines     : 1,
        };

        /**
         * 客製化製作區
         */
        if ( this.MachineController != null && this.ControllerComponentName != "") {
            var controller = this.MachineController.getComponent(this.ControllerComponentName);
            if ( controller != null ) this.controller = controller;
        } 
   },

   /**
    * 模擬Client接收封包
    * @param {*} command 
    * @param {*} packet 
    */
   receive_package(command, packet) {
       // console.log("receive_package", [command, packet]);
       if ( command == null ) return;
       if ( this[command] == undefined ) return;
       var data = JSON.parse(packet.data);
       // console.log(data);
       return this[command]( data );
   },

    /**
     * 玩家人物進入Server遊戲模組，收到的回傳封包
     * @param {*} data 資料內容放進 info
     */
    enter_game_response(data) {
        // console.log("enter_game_response", data);
        this.status.info = data;
        this.status.bet_list = data.Bet;
        this.status.total_lines = data.TotalLines;
        user.credit = data.User.Balance;
        this.call_control_bar("set_balance", data.User.Balance);
        this.register_bet();
        if ( this.status.state == mState.Lock) {
            this.status.state = mState.Normal;
            this.status.type = eType.Normal;
        }

        return this.event_enter_game_response(data);
    },

    /**
     * 改變 bet 值
     * @param {*} idx BET位置idx
     */
    change_bet(idx) {
        this.status.now_bet_idx = idx;
    },

    /**
     * 單注分數
     * @return { number } 分數
     */
    bet_value() {
        var bet_index = this.status.now_bet_idx;
        var bet_list = this.status.bet_list;
        return bet_list[bet_index];
    },

    /**
     * 總壓分數
     * @return { number } 分數
     */
    total_bet_value() {
        return this.bet_value() * this.status.total_lines;
    },

    /**
     * 設定 Server 傳來的 Bet 值
     * @from this.enter_game_response
     */
    register_bet() { this.call_control_bar("change_bet", 0); },

    register_machine() {
        cc.machine = this;
    },

    preload() { 
        this.init();
        this.common = common;
    },

    /**
     * 登陸 Symbol 物件
     * @from this.init()
     * @private
     * @todo 根據 this.SymbolParentNode 的子物件 
     * @todo 將所有內容有 symbol 的 component 做登陸動作
     * @todo lair 登陸
     * @todo machine 登陸
     */
    load_symbol() {
        
        var _symbols = this.SymbolParentNode.getChildren();
        var machine = cc.machine;
        this.small_symbols = [];    // 小圖標 symbol, 大小區分出來
        this.normal_symbols = [];   // 空陣列先準備好, 給滾輪表演時隨機取出使用
        this.all_symbols = [];      // 所有的 symbol 資料, 包含 hide
        this.bonus_symbol = {};     // Bonus Game 模組設定
        this.focus_symbol = {};     // 有聽牌效果 symbol
        this.all_symbols_component = {};

        _symbols.forEach(node => {
            var _symbol = node.getComponent("symbol");
            if ( _symbol == null ) return;
            _symbol.register_node();

            machine.all_symbols.push(_symbol.SymbolID);
            machine.all_symbols_component[_symbol.SymbolID] = _symbol;

            machine.payline.load_symbol(_symbol);
            machine.call_controller("load_symbol",_symbol);

            if ( _symbol.hide != true ) machine.normal_symbols.push(_symbol.SymbolID);
            if ( _symbol.is_big_symbol() != true ) this.small_symbols.push(_symbol.SymbolID);
            if ( _symbol.BonusRoute != null ) this.load_bonus_route(_symbol);
            if ( _symbol.FocusCount > 1 ) this.focus_symbol[_symbol.SymbolID] = _symbol.FocusCount;
        });
    },

    /**
     * 遊戲暫停
     * @param {boolean} active true 暫停 / false 取消
     * @todo 停止遊戲任何運行 (設定busying 控制條操作)
     */
    pause(active) {
        if ( active == true ) this.status.type = eType.LockSpin;
        else this.status.type = eType.Normal;

        return active;
    },

    is_normal() {
        if ( this.status.state == mState.Normal ) return true;
        return false;
    },

    check_credit_enough() {
        if (this.is_credit_enough() == false) {
            this.stop_auto_spin();  //auto spin 次數歸 ０
            this.controller_bar.open_custom_ui(0, {
                'title':i18n.get_language_data('common101010'), 
                'button':i18n.get_language_data('common100002'),
                'message':i18n.get_language_data('common200002'),
                'show_close':false,
            });
            cc.machine.controller_bar.set_custom_ui_click_func(0, function() { return GW.reload_game(); });
            return false;
        }
        return true;
    },

    is_credit_enough() {
        return user.credit >= this.total_bet_value();
    },

    /*********************************
     * Spin 流程
    *********************************/

    /**
     * 點擊 Spin Button
     */
    push_spin_button() {
        if ( this.push_spin_button_break(this.status.state) == true ) return;
        if ( this.status.bonus_gaming == true ) return;
        if ( this.status.type != eType.Normal ) return;
        if ( cc.big_win_controller != null && cc.big_win_controller.node.active == true ) return;
        
        switch(this.status.state) {
            case mState.Normal:
                this.begin_spin();
                break;

            case mState.Spin: 
                if ( this.status.callstop == true) return; // 已經急停
                this.slot_control_bar.change_spin_stop_disable();
                this.reel_frame.call_stop_all();
                break;

            case mState.Payline:
                this.payline.call_stop();
                break;
        }

        return this.event_push_spin_button();
    },

    /**
     * 呼叫 Server 取得 Spin 結果
     */
    call_spin_to_server() {
        if ( this.status.bonus_gaming == true ) return;

        this.slot_control_bar.set_win_score_label(0);
        this.call_server_function("spin", this.total_bet_value());
    },

    /**
     * 按下 Spin 按鈕執行，預設spin資料
     */
    begin_spin() {
        if ( this.status.bonus_gaming == true ) return;
        if ( this.status.state != mState.Normal ) return;
        if ( this.check_credit_enough() == false ) return;
        this.call_controller("begin_spin");
        
        user.credit -= this.total_bet_value();
        user.refresh(user.credit);

        this.status.round_idx = 0;
        return this.spin();
    },

    /**
     * 執行 Spin 動作，
     */
    spin() {
        if ( this.spin_break() == true ) return; // 中斷 spin
        this.status.state = mState.Spin;
        this.reel_frame.start_spin();
        this.slot_control_bar.change_spin_stop();
        if ( this.SpinSound != null ) cc.Sound.PlaySound(this.SpinSound, false);

        this.call_spin_to_server();
        return this.event_spin();
    },
    
    /**
     * @param {JSON} result Server回應內容
     * result: {
     *      cmd: 封包號碼
     *      data: 盤面內容
     * }
     */
    spin_response(reel_response) {
        // var reel_response = JSON.parse(result['data']);
        reel_response['result']['total_score'] = parseFloat(reel_response['result']['total_score']);

        var tmp = this.call_controller("receive_spin_response", reel_response);
        if ( tmp != null && tmp != false ) reel_response = tmp;
        
        this.status.round_data = reel_response;

        // 是否要中斷往下執行
        if ( this.spin_response_break(reel_response) == true ) return;
        
        // 通知拿到答案
        this.reel_frame.spin_response(reel_response['result']['all_round_data'][0]['reel_frame']);

        return this.event_spin_response(reel_response);
    },

    /**
     * 從目前有的 round_data 指定一個盤面
     * @param {*} idx 
     * @todo 不在正常內執行
     * @todo free spin 或是 消去型 使用
     */
    spin_assign_round_data(idx) {
        if ( cc.machine.spin_assign_round_data_break() == true ) return;
        var reel_response = machine.status.round_data;

        // console.log('spin_assign_round_data:'+idx);
        // 通知拿到答案
        cc.machine.reel_frame.spin_response(reel_response['result']['all_round_data'][idx]['reel_frame']);
        return cc.machine.event_spin_assign_round_data();
    },

    /**
     * 預載 Bonus Game component
     * @param {symbol} symbol component
     * @from load_symbol 
     */
    load_bonus_route(symbol) {
        var bonusRoute = symbol.BonusRoute;
        if ( bonusRoute['target'] == null ) return null;
        if ( bonusRoute['handler'] == null ) return null;
        if ( bonusRoute['_componentName'] == null ) return null;

        var component = bonusRoute['target'].getComponent(bonusRoute['_componentName']);
        if ( component == null ) return null;
        if ( component[bonusRoute['handler']] == null ) return null;

        cc.machine[bonusRoute['_componentName']] = component;
        this.bonus_symbol[symbol.SymbolID] = bonusRoute;
    },

    /**
     * 呼叫 bonus game 模組
     * @from 設定在 symbol BonusRoute
     */
    bonus_route(symbol_id, param) {
        if ( this.bonus_symbol[symbol_id] == null ) return null;
        var bonusRoute = this.bonus_symbol[symbol_id];

        if ( bonusRoute['target'] == null ) return null;
        if ( bonusRoute['handler'] == null ) return null;
        if ( bonusRoute['_componentName'] == null ) return null;
        if ( param == null ) return null;
        if ( param['bonus_count'] == null ) return null;

        var component = bonusRoute['target'].getComponent(bonusRoute['_componentName']);
        if ( component == null ) return null;
        if ( component[bonusRoute['handler']] == null ) return null;

        return component[bonusRoute['handler']](symbol_id, param);
    },

    /**
     * 檢查是否進入 Bonus Game
     * @return {boolean} true: 是否要執行bonus
     */
    check_bonus_game(execute_bonus_game = true) {
        if ( this.check_bonus_game_break() == true ) return true;
        if ( this.status.bonus_gaming == true ) return false;       // 正在執行
        if ( this.status.bonus_done == true ) return false;         // 已經執行完畢
        
        var all_round_data = this.status.round_data['result']['all_round_data']; // 盤面資料
        var round_idx = this.status.round_idx;                                   // 第幾輪
        var round_data = all_round_data[round_idx];                              // 取出資料
        var symbol = round_data['bonus_symbol'];
        var type = round_data['type'];

        if ( symbol == null ) return false;
        if ( type == null ) return false;
        if (execute_bonus_game) {
            this.bonus_route(symbol, round_data);
            this.event_check_bonus_game();
            // this.controller_bar.deactive_auto_spin_button();
        }
        return true;
    },

    move_node(target, to_node) {
        if ( target == null ) return;
        if ( to_node == null ) return;

        var world_pos = target.parent.convertToWorldSpaceAR(target.getPosition());
        var to_pos = to_node.convertToNodeSpaceAR(world_pos);
        target.setParent(to_node);        
        target.setPosition(to_pos);
    },

    /**
     * 回傳是否有 BigWin 
     */
    check_big_win() {
        if ( cc.big_win_controller == null ) return false;
        return cc.big_win_controller.check_have_big_win(this.total_bet_value(), cc.machine.status.round_data.result.total_score);
        //return this.bigwin_controller.check_big_win(machine.status.round_data.result.total_score);
    },

    /**
     * @from payline show
     * 執行 Bigwin
     */
    run_big_win() {
        return cc.big_win_controller.run_big_win(this.total_bet_value(), cc.machine.status.round_data.result.total_score);
    },

    /**
     * 全部停輪會呼叫這裡
     */
    all_reel_done() {
        this.event_before_all_reel_done();
        if ( this.all_reel_done_break() == true ) return;
        if ( this.check_bonus_game() == true ) return;
        // check_big_win 以後留給 payline 呼叫
        // if ( this.check_big_win() == true ) return;
        this.status.state = mState.Payline;
        
        if ( this.call_controller("event_all_reel_done") != false ) return;
        // 把結果帶給payline
        this.payline.all_reel_done(this.status.round_data.result['all_round_data'][this.status.round_idx]);

        return this.event_all_reel_done();
    },
    
    /**
     * payline 結束分數播放，回到machine控制
     */
    show_end_payline_done() {
        this.back_to_normal();
    },

    back_to_normal() {
        if ( this.back_to_normal_break() == true ) return;
        this.status.state = mState.Normal;
        user.credit = this.status.round_data.result.last_credit;

        if (this.status.bonus_gaming == false){
            //更新 Credit，若在 Bonus Game 中則等到 push_end_ui 後執行。
            // console.log("back_to_normal", user.credit);
            user.refresh(user.credit);
        }

        this.run_auto_spin();
        this.slot_control_bar.change_spin_start();
        return this.event_back_to_normal();
    },

    run_auto_spin() {
        if ( this.status.auto_spin_times == 0 ) return;
        if ( this.status.bonus_gaming == true ) return;
        this.scheduleOnce(function(){
            if ( cc.big_win_controller.node.active == true) return this.run_auto_spin();
            return this.begin_spin();
        },1);
    },

    call_controller(method, args) {
        this.call_control_bar(method, args);
        // 先看 bonus module 有沒有要執行
        if ( this.call_bonus_controller(method, args) == true ) return true;
        if ( this.controller == null ) return false;
        if ( this.controller[method] == null ) return false;
        return this.controller[method](args);
    },

    /**
     * 執行 Bonus 模組事件
     * @param {*} method 
     * @param {*} args 
     * @from call_controller
     */
    call_bonus_controller( method, args ) {
        if ( this.status.bonus_done == true ) return false;
        if ( this.status.bonus_module == null ) return false;
        if ( this.status.bonus_module[method] == null ) return false;
        return this.status.bonus_module[method](args);
    },

    call_control_bar (method, args) {
        if (this.slot_control_bar == null) return false;
        if ( this.slot_control_bar[method] == null ) return false;
        return this.slot_control_bar[method](args);
    },

    /**
     * 指定 id 播放中獎效果
     * @param {number} symbol_id 
     * @param {boolean} is_show_all_line 
     */
    symbol_id_show_payline(symbol_id, is_show_all_line) {
        return this.reel_frame.symbol_id_show_payline(symbol_id, is_show_all_line);
    },

    /**
     * 遮罩開關
     * @param {*} active 
     */
    active_mask(active) { return this.payline.active_mask(active); },

    // ********************************************
    //      自動玩設置
    // ********************************************

    is_auto_spin() { return this.status.auto_spin_times == 0 ? false : true; },

    start_auto_spin(times) {
        // console.log('start auto spin:'+times);
        this.status.auto_spin_times = times;
        var state = this.check_auto_spin();
        if (state == true) this.event_start_auto_spin();
    },

    stop_auto_spin() {
        this.status.auto_spin_times = 0;
        this.event_stop_auto_spin();
    },

    check_auto_spin() {
        if ( this.status.state != mState.Normal) return false;
        if ( this.status.bonus_gaming == true ) return false;
        if ( this.is_auto_spin() == false ) return false;
        
        this.scheduleOnce(() => { 
            this.begin_spin();
            this.event_check_auto_spin();
        }, 0.5);
        return true;
    },

    calculate_response(response) {
        var result = JSON.parse(response['data']);
        // console.log(result);

    },

    network_checking() { return this.controller_bar.open_network_checking_ui(); },
    network_success() { return this.controller_bar.close_network_checking_ui(); }
});
