cc.Class({
  extends: cc.Component,

  properties: {
    /**
     * 押注額設定
     */
    BetValue: {
      // 切換押注額的顯示
      type: cc.Label,
      default: null
    },

    BetProgressBar: {
      // 押注額下面那根棒子
      type: cc.ProgressBar,
      default: null
    },

    TotalLines: 40,

    TotalBetValue: {
      // 總押注額顯示
      type: cc.Label,
      default: null
    },

    WinValue: {
      // 贏分顯示
      type: cc.Label,
      default: null
    },

    BalanceValue: {
      type: cc.Label,
      default: null
    },

    TotalWinValue: {
      type: cc.Label,
      default: null
    },

    FreeSpinsValue: {
      type: cc.Label,
      default: null
    },

    FgInfoNode: {
      type: cc.Node,
      default: null
    },

    SpinStartNode: {
      type: cc.Node,
      default: null
    },

    SpinStopNode: {
      type: cc.Node,
      default: null
    },

    SpinButton: {
      type: cc.Button,
      default: null
    },

    SpinLabel: {
      type: cc.Label,
      default: null
    },

    AutoStartAni: {
      type: cc.Animation,
      default: null
    },

    AutoStartNode: {
      type: cc.Node,
      default: null
    },

    AutoStopNode: {
      type: cc.Node,
      default: null
    },

    AutoTimesNode: {
      type: cc.Node,
      default: null
    },

    AutoTimesLabel: {
      type: cc.Label,
      default: null
    },

    ClickSound: {
      type: cc.AudioClip,
      default: null
    }
  },

  playClickSound() {
    if (this.ClickSound == null) return;
    cc.Sound.PlaySound(this.ClickSound, false);
  },

  clickAutoTimes(evt, times) {
    console.log(times);
    this.AutoStartNode.active = false;
    this.AutoStopNode.active = true;
    this.autoUp = false;
    this.AutoStartAni.play("autoDown");
    // cc.machine.status.auto_spin_times = times;
    cc.machine.start_auto_spin(times);
    this.playClickSound();
  },

  clickAutoStart() {
    if (this.autoUp == true) {
      this.autoUp = false;
      this.AutoStartAni.play("autoDown");
    } else {
      this.autoUp = true;
      this.AutoStartAni.play("autoUp");
    }
    this.playClickSound();
  },

  display_auto_spin() {
    var times = this.machine.status.auto_spin_times;
    if (times == 0) {
      this.AutoStartNode.active = true;
      this.AutoStopNode.active = false;
      this.AutoTimesNode.active = false;
      return;
    }

    if (times == -1) {
      this.AutoTimesLabel.string = "∞";
    } else {
      this.AutoTimesLabel.string = times;
      this.machine.status.auto_spin_times--;
    }
    this.AutoStopNode.active = true;
    this.AutoStartNode.active = false;
    this.AutoTimesNode.active = true;
    return;
  },

  stop_auto_spin() {
    this.machine.status.auto_spin_times = 0;
    return this.display_auto_spin();
  },

  change_spin_start() {
    if (cc.machine.status.bonus_gaming == true) {
      this.SpinStartNode.active = false;
      this.SpinStopNode.active = false;
      return;
    }
    this.SpinStartNode.active = true;
    this.SpinStopNode.active = false;
    return this.display_auto_spin();
    // this.SpinButton.interactable = true;
  },

  change_spin_stop() {
    this.SpinStartNode.active = false;
    this.SpinStopNode.active = true;
    return this.display_auto_spin();
    // this.SpinButton.interactable = false;
  },

  change_spin_stop_disable() {
    return this.display_auto_spin();
    // this.SpinButton.interactable = true;
  },

  start() {
    this.WinValue.string = "";
    this.SpinLabel.string = "";
    this.AutoTimesNode.active = false;
  },

  set_balance(value) {
    this.BalanceValue.string = value;
  },

  /**
   * 設定免費遊戲剩餘次數
   * @param {*} value
   */
  set_free_spins(value) {
    this.FreeSpinsValue.string = value;
  },

  /**
   * 設定免費遊戲總贏分
   * @param {*} value
   */
  set_total_win(value) {
    this.TotalWinValue.string = value;
  },

  /**
   * 按下 Spin 按鈕
   */
  spin_button() {
    cc.machine.push_spin_button();
  },

  /**
   * 設定贏分顯示
   * @param {*} value
   */
  set_win_score_label(value) {
    if (value == 0) return (this.WinValue.string = "");
    this.WinValue.string = value;
  },

  /*****************************
   * Bet Control
   ****************************/

  /**
   * 切換Bet
   * @param {*} index
   */
  change_bet(index) {
    if (cc.machine.status.bonus_gaming == true) return;
    if (index == null) return;
    cc.machine.change_bet(index);

    this.BetValue.string = cc.machine.bet_value();
    this.TotalBetValue.string = cc.machine.total_bet_value();

    return this.change_bet_progress_bar(index);
  },

  /**
   * @from this.change_bet(index)
   * @param { number } index 押注額位置
   */
  change_bet_progress_bar() {
    var index = cc.machine.status.now_bet_idx;
    var max = cc.machine.status.bet_list.length - 1;
    var persent = index / max;
    this.BetProgressBar.progress = persent;
  },

  /**
   * @from 按鈕事件: Canvas/Control Bar/Content/Bet/One Bet/add
   *                Canvas/Control Bar/Content/Bet/One Bet/reduce
   * @param { number } value 1, -1
   */
  add_bet_index(event, str) {
    var value = parseInt(str);
    var now_index = cc.machine.status.now_bet_idx + value;
    var max = cc.machine.status.bet_list.length - 1;
    if (now_index < 0) now_index = 0;
    if (now_index >= max) now_index = max;

    this.change_bet(now_index);
    this.playClickSound();
  },

  /**
   * 切換到最高押注額
   * @from 按鈕事件
   */
  change_max_bet() {
    var max = cc.machine.status.bet_list.length - 1;
    this.playClickSound();
    return this.change_bet(max);
  }
});
