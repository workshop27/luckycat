
cc.Class({
    extends: cc.Component,

    properties: {
        background_music : {
            type: cc.AudioClip,
            default: null,
            displayName: "背景音樂",
        },
    },

    start() {
        this.status = {
            total_score : 0,    // 累積總分
            before_free_game_round_data : null, // 在 free_game 之前的盤面
            is_retrigger : false,
        };
    },

    /**
     * 開始 Bonus Game
     */
    begin_free_game(symbol, round_data) {
        // 先記住進 free game 之前的盤面
        this.status.before_free_game_round_data = round_data;

        machine.active_mask(true);
        machine.symbol_id_show_payline(symbol, false);
        
        if (round_data['is_retrigger'] == null) {
            machine.status.bonus_gaming = true;
            machine.status.bonus_module = this;
            machine.status.bonus_done = false;
        }

        this.scheduleOnce(function(){
            machine.active_mask(false);
            machine.reel_frame.show_end_payline();
            sound.stopMusic();
            if (round_data['is_retrigger'] != null) {
                machine.status.retrigger_times = machine.status.round_data.result.all_round_data[machine.status.round_idx]['bonus_count'];
                machine.status.free_spin_times += machine.status.retrigger_times;
                return machine.call_controller('open_retrigger_ui');;
            }
            machine.status.free_spin_times = machine.status.round_data['result']['all_round_data'][0]['bonus_count']
            return machine.call_controller('open_free_spin_ui'); // 通知控制程式做後續動作
        }, 3);
    },

    start_free_game() {
        sound.playMusic(this.background_music);
        this.scheduleOnce(function(){ return this.do_spin(); },1);
    },

    do_spin() {
        machine.status.round_idx++;
        machine.spin();
        machine.spin_assign_round_data(machine.status.round_idx);
    },

    show_all_payline() { machine.payline.call_stop(); },

    back_to_normal() {
        if ( machine.status.round_idx == 0 ) return false;
        
        this.scheduleOnce(function(){ 
            if ( this.check_end_bonus() == true) return;
            this.do_spin(); 
        },1);
        return true;
    },

    check_end_bonus() {
        if ( machine.status.round_idx < machine.status.round_data.result.round_count-1 ) return false;

        this.scheduleOnce(function(){
            sound.stopMusic();
            // console.log("free game end free game");
            machine.call_controller('open_end_free_spin_ui'); 
        }, 1);
        
        return true;
    },

    end_game() {
        machine.status.bonus_gaming = false;
        machine.status.bonus_module = null;
        machine.status.bonus_done = false;
        machine.back_to_normal();
    },

});
