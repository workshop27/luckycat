
var score_level = { Low : 0, Middle : 1, High : 2};
var level = cc.Enum(score_level);

cc.Class({
    extends: cc.Component,

    properties: {
        key_audio:{
            type: cc.AudioClip,
            default : null,
            displayName : '選取的聲音',
        },

        total_win_label : {
            type: cc.Label,
            default: null,
            displayName: "總分顯示 Label",
        },

        pick_count_max: {
            type: cc.Label,
            default: null,
            displayName: "最大選取數量 Label",
        },

        pick_count_remain: {
            type: cc.Label,
            default: null,
            displayName: "剩餘選取數量 Label"
        },
    },

    reset_pick_data() {
        var bonus_data = machine.status.round_data.result['all_round_data'][1];
        //console.log(bonus_data);
        this.status = {
            max_count : bonus_data['count'],
            now_count : bonus_data['count'],
            list : bonus_data['pick_list'],
            total_score : bonus_data['total_score'],
            score : 0,
            picked : 0,
        };
    },

    /**
     * 開始遊戲入口
     * @param {*} symbol 
     * @param {*} param 
     */
    begin_bonus_game(symbol, param) {
        this.reset_pick_data();
        machine.active_mask(true);
        machine.symbol_id_show_payline(symbol, false);
        
        machine.status.bonus_gaming = true;
        machine.status.bonus_module = this;
        machine.status.bonus_done = false;

        this.scheduleOnce(function(){
            machine.active_mask(false);
            machine.reel_frame.show_end_payline();
            return machine.call_controller('open_start_bonus_game_ui'); // 通知控制程式做後續動作
        }, 3);
    },

    /**
     * 選取一個結果
     * @param {*} sys_pick 是不是玩家點的(系統全開功能)
     */
    pick_one(sys_pick=false) {
        if ( sys_pick == false ) {
            if ( this.status.now_count < 1 ) return null;
            sound.PlaySound(this.key_audio);
        }
        var idx = this.status.picked;
        var pick_score = this.status.list[idx]; 

        if ( sys_pick == false ) {
            this.status.score = this.status.score.add_float(pick_score);
            this.status.now_count --;
        }

        this.status.picked ++;
        this.refresh_label(sys_pick == false ? pick_score : null);

        return pick_score;
    },

    refresh_label(pick_score = null) {
        if ( this.total_win_label != null )   this.total_win_label.string = this.status.score;
        if ( this.pick_count_max != null )    this.pick_count_max.string = this.status.max_count;
        if ( this.pick_count_remain != null ) this.pick_count_remain.string = this.status.now_count;
        if (pick_score == null) return;
        pick_score = parseFloat(pick_score);
        if ( pick_score != null && pick_score > 0 ) {
            machine.controller_bar.add_win_score_label(pick_score);
        }
    },

    end_game() {
        machine.status.bonus_gaming = false;
        machine.status.bonus_module = null;
        machine.status.bonus_done = false;
        machine.back_to_normal();
    },
    
});
