cc.Class({
  extends: cc.Component,

  properties: {
    SymbolID: "",

    hide: {
      default: false,
      displayName: "隱藏",
      tooltip: "在滾輪的時候不會出現",
    },

    NormalNode: {
      type: cc.Node,
      default: null,
      displayName: "* 一般狀態圖示",
    },

    PaylineNode: {
      type: cc.Node,
      default: null,
      displayName: "* 得獎動態",
      tooltip: "播放獎項時開啟，播放完畢則關閉",
    },

    AllPaylineNode: {
      type: cc.Node,
      default: null,
      displayName: "全線得分動態",
      tooltip: "可不指定，預設為單線動態",
    },

    FgFsNode: {
      type: cc.Node,
      default: null,
      displayName: "FG FS特效",
      tooltip: "可不指定，預設為單線動態",
    },

    ReelStopNode: {
      type: cc.Node,
      default: null,
      displayName: "停輪動態",
      tooltip: "停輪時播放動態，可不指定",
    },

    RollingNode: {
      type: cc.Node,
      default: null,
      displayName: "滾動模組",
      tooltip: "在Spin後開啟，停輪時關閉",
    },

    StopRollingNode: {
      type: cc.Node,
      default: null,
      displayName: "停輪模組",
      tooltip: "停輪時開啟，模組自行關閉",
    },

    Expansion: {
      default: new cc.Size(1, 1),
      displayName: "Symbol 佔用大小",
      tooltip:
        "寬度若是2以上，相對 one_reel 需要置換支援多軸 one_reel，高度則不限",
    },

    StopSound: {
      type: cc.AudioClip,
      default: null,
      displayName: "出場音效",
    },

    PaylineSound: {
      type: cc.AudioClip,
      default: null,
      displayName: "得分音效",
    },

    特殊Symbol設定: undefined,
    BonusSetting: null,
    BonusRoute: {
      type: cc.Component.EventHandler,
      default: null,
      displayName: "額外遊戲入口程式",
    },

    FocusCount: {
      default: -1,
      displayName: "出現多少個數會聽牌, -1表示不聽牌",
    },

    BigSymbolAni: {
      type: cc.Node,
      default: [],
      displayName: "大圖標動態設定",
    },
  },

  onEnable() {
    this.node.sym = this;
  },

  /**
   * 警告訊息
   * @param {string} error_msg
   * @private
   */
  notify_fail(error_msg) {
    alert(error_msg);
    console.error(error_msg);
    return false;
  },

  /**
   * 檢查初始資料
   * @todo 在machine初始化時，會執行Symbol登錄與檢查
   * @from machine.init()
   * @returns { bool } 檢查通過: true, 資料遺漏或失敗: false
   * @private
   */
  smart_check() {
    if (this.SymbolID == "" || this.SymbolID == null)
      return this.notify_fail("沒有設定SymbolID, Node UUID:" + this.node.uuid);
    if (this.NormalNode == null)
      return this.notify_fail(
        "沒有設定一般狀態圖示(NormalNode) ID:" +
          this.SymbolID +
          ", Node UUID:" +
          this.node.uuid
      );
    if (this.PaylineNode == null)
      return this.notify_fail(
        "沒有設定得獎(PaylineNode) ID:" +
          this.SymbolID +
          ", Node UUID:" +
          this.node.uuid
      );
    if (this.AllPaylineNode == null) this.AllPaylineNode = this.PaylineNode;
    return true;
  },

  /**
   * 登陸 symbol
   */
  register_node() {
    if (this.smart_check() != true) return;
    cc.lair.register_node(this.SymbolID, this.node);
  },

  remove() {
    this.node.link = null;
    var symbol_id = cc.machine.call_controller("remove_symbol_id", this);
    cc.lair.put_node(this.node, symbol_id);
    this.show_normal();
  },

  is_big_symbol() {
    if (this.Expansion.width > 1) return true;
    if (this.Expansion.height > 1) return true;
    return false;
  },

  show_all_payline() {
    if (this.node.link != null) return;
    if (this.RollingNode != null) this.RollingNode.active = false;
    this.NormalNode.active = false;
    this.AllPaylineNode.active = true;
    if (this.FgFsNode !== null) {
      this.FgFsNode.active = false;
    }
  },

  show_one_payline() {
    if (this.node.link != null) return;
    this.NormalNode.active = false;
    this.PaylineNode.active = true;
    if (this.RollingNode != null) this.RollingNode.active = false;
    if (this.PaylineSound != null) cc.Sound.PlaySound(this.PaylineSound);
  },

  show_reel_stop() {
    if (this.ReelStopNode == null) return;
    this.ReelStopNode.active = true;
  },

  show_normal() {
    this.NormalNode.active = true;
    this.AllPaylineNode.active = false;
    this.PaylineNode.active = false;

    if (this.FgFsNode !== null) {
      var fgSymbol = cc.machine.status.round_data.result.fg_symbol + 100 + "";
      if (this.SymbolID === fgSymbol) {
        this.FgFsNode.scaleX = 0.25;
        this.FgFsNode.scaleY = 0.25;
        this.FgFsNode.actice = true;
      } else {
        this.FgFsNode.actice = false;
      }
    }

    if (this.RollingNode != null) this.RollingNode.active = false;
    if (this.ReelStopNode != null) this.ReelStopNode.active = false;
    if (this.BigSymbolAni.length > 1) {
      this.change_big_symbol(0);
    }
  },

  change_big_symbol(index) {
    if (this.BigSymbolAni == null) return;
    if (this.BigSymbolAni.length == 0) return;

    for (var idx in this.BigSymbolAni) {
      var sym = this.BigSymbolAni[idx];
      sym.active = false;
    }

    this.BigSymbolAni[index].active = true;
  },
});
