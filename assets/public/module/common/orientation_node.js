var orientation_node_data = cc.Class({
    name : "orientation_node_data",
    properties : {
        source_node : {
            type : cc.Node,
            default : null,
            displayName : "來源節點",
        },
        _portrait_node : {
            type : cc.Node,
            default : null,
            displayName : "直版參考節點",
        },
        landspace_node : {
            type : cc.Node,
            default : null,
            displayName : "橫版參考節點",
        },
    },
});


cc.Class({
    extends: cc.Component,

    properties: {  
        all_node : {
            type : [orientation_node_data],
            default : [],
            displayName : "遊戲需要橫直轉換的節點",
        },
    },

    start () {
        this.add_orientation_node(); 

    },

    add_orientation_node() {
        if ( this.all_node.length == 0 ) return;
        if ( cc.orientation == null ) this.scheduleOnce(this.add_orientation_node, 1);
        
        cc.orientation.add_sorce_node(this.all_node);
        this.all_node.forEach(element => {
            cc.orientation.change_node(element);
        });
    },


});
