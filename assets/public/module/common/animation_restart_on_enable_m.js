
cc.Class({
    extends: cc.Animation,

    properties: {
        dealy_play : {
            default: 0,
            displayName: '延遲多久播放',
        },
    },

    onEnable() {
        
        if ( this.defaultClip == null ) return;

        this.stop();
        this.setCurrentTime = 0;
        
        var _name = this.defaultClip.name;
        this.getAnimationState(_name).time = 0;
        this.getAnimationState(_name).sample();

        if ( this.dealy_play == 0 ) return this.play();
        return this.scheduleOnce(function() { this.play(); }, this.dealy_play);
    },

    



});
