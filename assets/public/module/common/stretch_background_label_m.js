// Learn cc.Class:
//  - [Chinese] https://docs.cocos.com/creator/manual/zh/scripting/class.html
//  - [English] http://docs.cocos2d-x.org/creator/manual/en/scripting/class.html
// Learn Attribute:
//  - [Chinese] https://docs.cocos.com/creator/manual/zh/scripting/reference/attributes.html
//  - [English] http://docs.cocos2d-x.org/creator/manual/en/scripting/reference/attributes.html
// Learn life-cycle callbacks:
//  - [Chinese] https://docs.cocos.com/creator/manual/zh/scripting/life-cycle-callbacks.html
//  - [English] https://www.cocos2d-x.org/docs/creator/manual/en/scripting/life-cycle-callbacks.html

cc.Class({
    extends: cc.Label,

    properties: {
        stretch_string: {
            get() { return this.string; },
            set(value) {
                this.string = value;
                // return this.stretch_background();
                // 
                // 0是下一個frame就執行的意思
                this.scheduleOnce(function(){return this.stretch_background();},0); 
            },
        },

        add_widht: {
            default: 0,
            displayName: '加寬',
        },

        background_node : {
            type: cc.Node,
            default: null,
            displayName: '背景圖或背景物件',
        },
    },

    /**
     * 拉伸背景圖功能
     * 當 strech_string 被設定時，會依據label的寬度拉伸 background_node的寬度
     */
    stretch_background() {
        if ( this.background_node == null ) return;
        if ( this.string == "" ) return this.background_node.active = false;

        this.background_node.active = true;
        return this.background_node.width = this.node.width + this.add_widht;
    },

});
