cc.Class({
    extends: sp.Skeleton,

    properties : {
        on_enable_play : {
            default : true,
            displayName : "onEnable是否重新播放",
        },
        play_compeleted_deactivate : {
            default : false,
            displayName : "播放完畢是否關閉顯示",
        },
        delay:{
            default:0,
            displayName:"延遲播放",
            tooltip:"0 秒則馬上播放，並非延遲一禎。"
        }
        
    },

    onEnable () {
        cc.RenderComponent.prototype.onEnable.call(this);
        if (this.on_enable_play == false) return;
        //console.log(this.defaultAnimation);
        if ( this.delay <= 0) this.setAnimation(0, this.defaultAnimation, this.loop);
        else{
            this.scheduleOnce(()=> { this.setAnimation(0, this.defaultAnimation, this.loop); }, this.delay);
        }
    },

    onLoad () {
        this.setCompleteListener(() => { this.compelete_listener(); });
    },

    compelete_listener () {
        if (this.play_compeleted_deactivate == false) return;
        this.node.active = false;
    },
});
