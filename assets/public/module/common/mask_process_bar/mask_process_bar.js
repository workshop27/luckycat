
cc.Class({
    extends: cc.Component,

    properties: {

        background : {
            type: cc.Node,
            default: null,
        },

        mask : {
            type: cc.Mask,
            default: null,
        },

        value: { // 進度值 0 ~ 1
            type: 'Float',
            default: 0,
            range: [0,1,0.01],
            min : 0,
            max: 1,
            slide : true,
            notify: function() { this._updateBarStatus(); },
        },
    },

    _updateBarStatus() {
        if ( this.mask == null ) return;
        if ( this.background == null ) return;
        
        var max_width = this.background.width;
        this.mask.node.x = -max_width/2;
        this.mask.node.width = max_width;
        


        var width = max_width * this.value;
        this.mask.node.width = width;
    },

    start() {
        
    }
});