
var sprite_type = { 
    Sprite:0, 
    ParticleSystem:1, 
    b_script:2,
};

// var load_altas = require("load_altas");

cc.Class({
    extends: cc.Component,

    properties: {
       altas:{
           type:cc.SpriteAtlas,
           default:null,
           displayName:"圖集"
       },
       s_type:{
            type:cc.Enum(sprite_type),
            default:sprite_type.Sprite,
            displayName:"圖片元件類型"
       },
       second_rate:{
            type:cc.Integer,
            default:12,
            displayName:"fps"
       },       
    },

    onLoad () {
        this.sprite_frames = [];
        this.sprite_node = this.node;
        this.sprite_idx = 0;
        this.s_type_str = cc.Enum(sprite_type)['__enums__'][this.s_type]['name'];
        this.sprite_provider = null;
        switch(this.s_type){
            case sprite_type.Sprite:
                this.sprite_provider = this.sprite_node.getComponent(cc.Sprite);
                break;
            case sprite_type.ParticleSystem:
                this.sprite_provider = this.sprite_node.getComponent(cc.ParticleSystem);
                break;
            default:
                this.sprite_provider = this.sprite_node.getComponent(this.s_type_str);
                break;
        }
        if(this.sprite_provider == null){
            throw new Error("必須為"+this.sprite_node.name+"物件添加"+this.s_type_str+"元件！");
            return;
        }
        if(this.sprite_provider['spriteFrame'] == undefined){
            throw new Error(this.s_type_str+"元件必須要含有spriteFrame類型的屬性，或spriteFrame類型的屬性必須要有值");
            return;
        }
    },

    start () {
        this.sprite_frames = this.altas.getSpriteFrames();
        
        this.schedule(this.change,1.0 / this.second_rate);
    },

    change () {
        // switch(this.s_type){
        //     case sprite_type.ParticleSystem:
        //         this.sprite_provider.
        //         break;
        // }
        this.change_sprite(this.sprite_frames[this.sprite_idx++]);
        if(this.sprite_idx >= this.sprite_frames.length)
        this.sprite_idx = 0;
    },

    change_sprite(new_sprite_frame){
        this.sprite_provider.spriteFrame = null;
        this.sprite_provider.spriteFrame = new_sprite_frame;
    },
});
