var all_node_data = cc.Class({
    name : "orientation_data",
    properties : {
        source_node : {
            type : cc.Node,
            default : null,
            displayName : "來源節點",
        },
        _portrait_node : {
            type : cc.Node,
            default : null,
            displayName : "直版參考節點",
        },
        landspace_node : {
            type : cc.Node,
            default : null,
            displayName : "橫版參考節點",
        },
    },
});


/**
 * 提供遊戲在直屏與橫屏 遊戲畫面切換功能
 */
cc.Class({
    extends : cc.Component,
    properties : {
        canvas_comp : { 
            type : cc.Canvas,
            default : null,
        },
        all_node : {
            type : [all_node_data],
            default : [],
            displayName : "遊戲需要橫直轉換的節點",
        },

        /**
         * 本遊戲的其他設置
         */
        MachineController: {
            type: cc.Node,
            displayName: "遊戲控制模組",
            default: null,
            tooltip : "客製化本遊戲的程式部分",
        },

        ControllerComponentName: {
            default: "",
            displayName: "控制模組的 Component Name",
        },
        
        /* MainNode : {
            type: cc.Node,
            default: null,
            // displayName : '主要遊戲功能物件',
        },

        LandspaceNode : {
            type: cc.Node,
            default : null,
            // displayName : '橫屏顯示畫面',
        },

        PortraitNode : {
            type: cc.Node,
            default: null,
            // displayName: '直屏顯示畫面',
        }, */

    },

    onLoad () {
        // this = this;
        this.canvas_comp.node.opacity = 0;
        
        var _orientation = cc.sys.isMobile ? this.check_mobile_orientation() : this.check_desktop_orientation();
        this.current_orientation = _orientation;
        var _orientation_type = (_orientation == 'Portrait') ? cc.macro.ORIENTATION_PORTRAIT : cc.macro.ORIENTATION_PORTRAIT;
        cc.view.setOrientation(_orientation_type);
    },

    onDestroy(){
        window.removeEventListener("orientationchange",this.orientation_change_event,false);
    },
    
    start () {
        /* if ( this.MainNode == null ) return cc.error('沒有設定主要遊戲功能物件');
        if ( this.LandspaceNode == null ) return cc.error('沒有設定橫屏畫面物件');
        if ( this.PortraitNode == null ) return cc.error('沒有設定直屏畫面物件'); */
        
        /**
         * 客製化製作區
         */
        if ( this.MachineController != null && this.ControllerComponentName != "") {
            var controller = this.MachineController.getComponent(this.ControllerComponentName);
            if ( controller != null ) this.controller = controller;
        }
        this.orientation_change_event = ()=> {this.on_orientation_change();}
        cc.view.setResizeCallback( this.orientation_change_event);
        window.addEventListener("orientationchange", this.orientation_change_event, false);
        this.save_source(this.all_node);

        this.scheduleOnce(() => {
            this.init_orientation();
            this.init = false;
        }, 0);

        cc.orientation = this;
    },

    init_orientation () {
        var _orientation = cc.sys.isMobile ? this.check_mobile_orientation() : this.check_desktop_orientation();
        this.set_orientation(_orientation);
        this.canvas_comp.node.opacity = 255;
    },

    check_mobile_orientation () {
        if ((window.orientation % 180) != 0) { return 'Landscape'; }
        return 'Portrait';
    },

    check_desktop_orientation() {
        if ( cc.view._frameSize.width > cc.view._frameSize.height ) return 'Landscape';
        return 'Portrait';
    },

    on_orientation_change () {
        //console.log("on_orientation_change");
        var _orientation = cc.sys.isMobile ? this.check_mobile_orientation() : this.check_desktop_orientation();
        if ( this.current_orientation == _orientation ) return;
        this.current_orientation = _orientation;
        this.set_orientation(_orientation);
    },

    set_orientation (_orientation) {
        var _orientation_type = (_orientation == 'Portrait') ? cc.macro.ORIENTATION_PORTRAIT : cc.macro.ORIENTATION_LANDSCAPE;
        // console.log('_orientation_type : ' + _orientation_type);
        cc.view.setOrientation(_orientation_type);
        this.set_canvas(_orientation);
        this.set_objects(_orientation);
    },

    set_canvas (_orientation) {
        var _cur_size = (_orientation == 'Portrait') ? new cc.Size(768, 1366) : new cc.Size(1366, 768);
        this.canvas_comp.designResolution = _cur_size;

        if (this.canvas_comp.fitHeight == true && this.canvas_comp.fitWidth == true) return;
        var _cur_fit_height = (_orientation == 'Portrait');
        this.canvas_comp.fitHeight = _cur_fit_height;
        this.canvas_comp.fitWidth = ( !_cur_fit_height );
    },

    set_objects (_orientation) {
        var _children = this.all_node;
        _children.forEach(child => {
            this.change_node(child, _orientation);
            /*
            if (child == null) return;
            var control_node = child.source_node;
            if (control_node == null) return;
            var data = (_orientation == 'Portrait') ? control_node.source : child.landspace_node;
            control_node.position = data.position;
            control_node.angle    = data.angle;
            control_node.scaleX   = data.scaleX;
            control_node.scaleY   = data.scaleY;
            control_node.anchorX  = data.anchorX;
            control_node.anchorY  = data.anchorY;
            control_node.width    = data.width;
            control_node.height   = data.height;
            control_node.color    = data.color;
            control_node.opacity  = data.opacity;*/
        });
        if (this.controller == null) return;
        if (this.controller.on_orientation_change == null) return;
        this.controller.on_orientation_change(_orientation);
    },

    change_node(child, _orientation=null) {
        if ( _orientation == null) _orientation = this.current_orientation;
        if (child == null) return;
        var control_node = child.source_node;
        if (control_node == null) return;

        var data = (_orientation == 'Portrait') ? control_node.source : child.landspace_node;
        control_node.position = data.position;
        control_node.angle    = data.angle;
        control_node.scaleX   = data.scaleX;
        control_node.scaleY   = data.scaleY;
        control_node.anchorX  = data.anchorX;
        control_node.anchorY  = data.anchorY;
        control_node.width    = data.width;
        control_node.height   = data.height;
        control_node.color    = data.color;
        control_node.opacity  = data.opacity;
    },

    save_source(all_data) { 
        for(var child in all_data) {
            this.save_source_node(all_data[child].source_node);
        }
    },

    save_source_node(node) {
        if (node == null) return;
        var save_data = {
            position : node.position,
            angle    : node.angle,
            scaleX   : node.scaleX,
            scaleY   : node.scaleY,
            anchorX  : node.anchorX,
            anchorY  : node.anchorY,
            width    : node.width,
            height   : node.height,
            color    : node.color,
            opacity  : node.opacity,
        };
        node.source = save_data;
    },

    add_sorce_node(all_data) {
        console.log('orientation_node',all_data);
        this.save_source(all_data);
        this.all_node = this.all_node.concat(all_data);
    }


});