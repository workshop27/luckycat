cc.Class({
    extends: cc.Label,

    properties: {
        //支援小數點第幾位
        decimal :{
            min : 0,
            max : 4,
            default: 2,
            displayName : "支持小數點幾位",
            notify : function() {
                if ( this.decimal > 4 ) this.decimal = 4;
                if ( this.decimal < 0 ) this.decimal = 0;
            },
        },              
        scroll_total_time : 3.0,     //總裩分時數(s)
        start_value : 1,             //滾分起始分數
        end_value : 100,             //結束滾分分數

        

    },
    //是否已達結束滾分條件
    is_scroll_finish: function (){
        return this.current_value >= this.end_value;
    },

    onLoad: function () {
        this.tick_call = null;
        this.node.scroller = this;
        this.target = null;
        this.finish_callback = null;
        this.current_value = this.start_value;            //分數
        this.range = this.end_value - this.start_value;   //範圍
        this.run = false;                               //開始滾分
        // this.final_run = false;                         //快速結束滾分
        this.current_scroll_time = this.scroll_total_time;//滾分總時數暫存
        this.scroll_time_min = 0.5;
        this.scroll_time_max = this.scroll_total_time;

        //註冊輸入事件
        // cc.systemEvent.on(cc.SystemEvent.EventType.KEY_DOWN, this.onKeyDown, this);
    },

    update: function (dt) {
        this.value_calculate(dt);        
    },
    //開始滾分
    start_scroll: function(newstart_value = this.start_value,newend_value = this.end_value,finish_action = null, target = null, tick_call = null){
        this.run = true;  
        this.fasting = false;
        this.target = target;
        this.tick_call = tick_call;
        this.finish_callback = finish_action;
        this.start_value = newstart_value;
        this.end_value = newend_value;
        this.range = this.end_value - this.start_value;   //範圍

        this.current_value = this.start_value;
        this.current_scroll_time = this.scroll_total_time;
    },
    //停止滾分，並且以new_scroll_scroll_total_time的秒數來滾完接下來的分數
    stop_scroll: function(new_scroll_total_time = this.scroll_time_min){
        this.current_scroll_time = new_scroll_total_time;
        //限制指派的滾分時間不得的小於 this.scroll_time_min
        //限制指派的滾分時間不得的大於 this.scroll_time_max
        if(this.current_scroll_time < this.scroll_time_min){
            this.current_scroll_time = this.scroll_time_min;
        }else if(this.current_scroll_time > this.scroll_time_max){
            this.current_scroll_time = this.scroll_time_max;
        }
    },
    //滾分
    value_calculate: function(dt){
        if(this.run){
            if(this.is_scroll_finish()){
                this.value_calculate_final(this.end_value);
            }else{
                this.current_value += this.get_interval(this.current_scroll_time,dt); 
                this.label_show(this.current_value);
                if ( this.tick_call != null ) this.tick_call(this.current_value);
            }
        }
    },
    //計算完畢
    value_calculate_final: function(message){
        this.label_show(message);
        if(this.finish_callback!=null)
        this.finish_callback(this.target);
        this.run = false;
    },
    //取得每個frame的區間差
    get_interval: function(scroll_total_time,dt){
        // let interval = (this.range * dt) / scroll_total_time;

        if ( this.fasting == true ) {
            let range = this.end_value - this.current_value + 1;
            let interval = Math.floor( range * dt ) + 2;
            return interval;
        }

        if ( this.range > 0 ) return 1;
        return -1;
        // return interval;
    },

    call_fast() { 
        this.fasting = true; 
    },

    //分數呈現
    label_show: function(message){
        message *= 1.0; 
        this.string = message.format_money(this.decimal);
    },

    //按下事件
    // onKeyDown: function (event) {
    //     switch(event.keyCode) {
    //         case cc.macro.KEY.a:
    //             this.start_scroll(50,100,()=>{console.log("123")});
    //             break;
    //         case cc.macro.KEY.s:
    //             this.stop_scroll();
    //             break;
    //     }
    // },
});