
cc.Class({
    extends: cc.Label,

    properties: {
        '滾分設定' : undefined,

        start_value : {
            default: 0,
        },

        end_value : {
            default: 0,
        },

        /**
         * 幾秒內滾完分數
         */
        rolling_time : {
            default: 1,
            max : 60,
            min : 0.5,
            displayName : "幾秒內滾完數字",
            notify : function() {
                if ( this.rolling_time > 60 ) this.rolling_time = 60;
                if ( this.rolling_time < 0.5 ) this.rolling_time = 0.5;
            },
        },

        /**
         * 支持小數點幾位
         */
        decimal : {
            min : 0,
            max : 5,
            default: 2,
            displayName : "支持小數點幾位",
            notify : function() {
                if ( this.decimal > 5 ) this.decimal = 5;
                if ( this.decimal < 0 ) this.decimal = 0;
            },
        },

        /**
         * 每秒幾禎
         */
        tick_frame : {
            default : 20,
            max : 60,
            min : 1,
            displayName : "每秒幾禎",
            notify : function() {
                if ( this.tick_frame > 60 ) this.tick_frame = 60;
                if ( this.tick_frame < 1 ) this.tick_frame = 1;
            },
        },

        rolling_end_event : {
            type: cc.Component.EventHandler,
            default : null,
            displayName : "結束後回傳",
        },
    },

    // 快速後，剩下的一秒內播完
    fast() {
        if ( this.status.isfast == true ) return;
        this.status.isfast = true;
        
        this.start_value = this.now_value;
        this.start_rolling(true);
    },

    /**
     * 開始轉
     */
    start_rolling(isfast=false) {
        // if ( this.start_value == this.end_value ) return;
        // console.log(Date.now()+':'+this.node.parent._name, this.end_value);

        var rolling_time = this.rolling_time;
        if ( isfast == true ) rolling_time = 1;
        else this.status.isfast = false;

        this.status.difference = this.end_value - this.start_value;
        this.status.end_value = this.end_value;
        this.status.divisor = rolling_time * this.tick_frame;
        if ( this.status.divisor < 1 ) this.status.divisor = 1;
        var interval = Math.floor(this.status.difference / this.status.divisor * 100000) / 100000;
        
        if ( Math.abs(interval) >= 10 ) { 
            interval = Math.floor(interval);
        } 
        // console.log('interval', interval);

        this.status.interval = interval;
        this.status.now_value = this.start_value;
        this.status.now_tick = 0;
        this.status.now_tick_idx = 0;
        this.next_tick();
        this.status.rolling = true;
    },

    /**
     * 入口 function
     * @param {*} to 滾分分數
     * @param {*} from 
     */
    rolling(to, from=null) {
        if ( to == from ) return;
        if ( from == null ) from = Number.parseFloat(this.string);
        this.start_value = from;
        this.end_value = to;

        return this.start_rolling();
    },

    /**
     * 執行中
     * @param {*} dt 
     */
    rolling_number(dt) {
        if ( this.pass_tick(dt) == false ) return;

        if ( this.count_value() == false ) return;

        return this.end_rolling();
    },

    /**
     * 計算跑下一禎的時間
     */
    next_tick() {
        this.status.now_tick_idx++;
        this.status.now_tick_done = this.status.now_tick_idx * (1 / this.tick_frame);
    },

    /**
     * 檢查是否可以跑下一禎
     */
    pass_tick(dt) {
        this.status.now_tick= this.status.now_tick.add_float(dt);
        if ( this.status.now_tick < this.status.now_tick_done ) return false;

        this.next_tick();
        return true;
    },

    /**
     * 數值處理
     */
    count_value() {
        var now_value = this.status.now_value;
        var interval = this.status.interval;

        this.status.now_value = now_value.add_float(interval);
        this.string = this.status.now_value.thousands(5);
        
        if ( interval > 0 && this.status.now_value >= this.status.end_value ) return true;
        else if ( interval < 0 && this.status.now_value <= this.status.end_value ) return true;

        return false;
    },

    /**
     * 結束滾分
     */
    end_rolling() {
        this.status.now_value = this.status.end_value;
        this.start_value = this.status.end_value;
        this.status.rolling = false;
        this.string = this.status.end_value.thousands(5);
        
        return this.callback_event();
    },

    /**
     * 
     */
    callback_event() {
        if ( this.rolling_end_event == null ) return;

        var event = this.rolling_end_event;
        // console.log(event);

        if ( event.target == null ) return;
        if ( event._componentName == null ) return;
        if ( event.handler == null ) return;

        return GW.event_emit(event);
    },

    init() {
        this.status = {
            isfast : false,
            now_value: 0,       // 滾分時目前數字
            end_value: 0,       // 結束數字
            interval: 0,        // 滾分時的數字間隔
            fast_stop: false,   // 有沒有啟動快速停止

            difference : 0,     // end 減 start 的差數
            now_tick : 0,       // 禎數累積判斷
            now_tick_idx: 0,    // 時間判斷階段

            rolling : false,    // 開關
        };
    },

    onLoad() {
        this.init();
    },

    start() {
        return this.node.rolling_number = this; // 方便給外部使用
    },

    update (dt) {
        if ( this.status.rolling == false ) return;
        return this.rolling_number(dt);
    },
});
