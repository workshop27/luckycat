cc.Class({
    extends: cc.Component,

    properties: {
        target_particle_system : {
            type : cc.ParticleSystem,
            default : null,
            displayName : "目標粒子特效",
        },
        delay:{
            type:cc.Float,
            default:0,
            displayName:"延遲播放",
        },
        auto_close:{
            default:false,
            displayName:"播放完後自動關閉",
        },
        auto_stop_time:{
            type:cc.Float,
            default:1,
            displayName:"自動停止發射時間",
            tooltip:"請勿大於關閉節點時間"
        },
        auto_close_time:{
            type:cc.Float,
            default:1,
            displayName:"自動關閉節點時間",
            tooltip:"請勿小於停止發射時間"
        }
    },

    onEnable () {
        if (this.target_particle_system == null) return;
        if ( this.delay > 0 ) this.scheduleOnce(()=> { this.play(); }, this.delay);
        else this.play();
        
        if ( this.delay <= 0 ) this.delay = 0;

        if (!this.auto_close) return;
        // 停止生產
        this.scheduleOnce(()=>{
            this.target_particle_system.stopSystem();
        },this.auto_stop_time + this.delay);
        // 關閉節點
        this.scheduleOnce(()=>{
            this.target_particle_system.node.active = false;
        },this.auto_close_time + this.delay);
    },

    play(){
        this.target_particle_system.resetSystem();
    },
});
