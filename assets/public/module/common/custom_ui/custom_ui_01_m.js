
cc.Class({
    extends: cc.Component,

    properties: {
        Title_Label: {
            type : cc.Label,
            default: null,
        },

        Title_i18n: "",
            
        Message_Label : {
            type: cc.Label,
            default: null
        },

        Message_i18n: "",

        Button_Label: {
            type: cc.Label,
            default: null,
        },

        Button_i18n: "",

        Close_Button: {
            type: cc.Node,
            default: null,
        },
        
    },

    init() {
        this.inited = true;
        if ( this.Title_Label ) this.i18nTitle = this.Title_Label.node.getComponent('i18n_Label');
        if ( this.Message_Label ) this.i18nMessage = this.Message_Label.node.getComponent('i18n_Label');
        if ( this.Button_Label ) this.Button_i18n = this.Button_Label.node.getComponent('i18n_Label');
    },

    open_ui(ui_data) {
        if ( this.inited != true ) this.init();
        // console.log(ui_data);

        if ( ui_data['title']   && this.Title_Label != null   ) this.set_message(this.Title_Label, ui_data['title']);
        if ( ui_data['message'] && this.Message_Label != null ) this.set_message(this.Message_Label, ui_data['message']);
        if ( ui_data['button']  && this.Button_Label != null  ) this.set_message(this.Button_Label, ui_data['button']);
        
        if ( ui_data['show_close'] != null && this.Close_Button != null  ) {
            this.Close_Button.active = (ui_data['show_close'] == true) ? true : false;
        }
    },

    // 設定click後回傳物件
    set_click_event(call_back_data) { 
        this.click_event = call_back_data;
    },

    // 設定click執行func
    set_click_function(c_func) {
        this.click_func = c_func;
    },

    click() {
        machine.controller_bar.close_ui();

        if ( this.click_event != null ) {
            var c_class = this.click_event[0];
            var c_func = this.click_event[1];
            var c_param = this.click_event[2];
            return c_class[c_func](c_param);
        }

        if ( this.click_func != null ) {
            var func = this.click_func;
            func();
        }
    },

    set_message(label, message) {
        label.string = message;
    }

});
