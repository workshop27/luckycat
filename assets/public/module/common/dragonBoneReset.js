// Learn cc.Class:
//  - https://docs.cocos.com/creator/manual/en/scripting/class.html
// Learn Attribute:
//  - https://docs.cocos.com/creator/manual/en/scripting/reference/attributes.html
// Learn life-cycle callbacks:
//  - https://docs.cocos.com/creator/manual/en/scripting/life-cycle-callbacks.html

cc.Class({
    extends: cc.Component,

    properties: {
        playTimes : -1,
        
        
    },

    onEnable() {
        var dragonBone = this.node.getComponent('dragonBones.ArmatureDisplay');
        var ani = dragonBone.animationName;
        dragonBone.playAnimation(ani, this.playTimes);
    },

});
