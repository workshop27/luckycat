cc.Class({
  extends: cc.Component,

  properties: {
    mainGameMusic: {
      type: cc.AudioClip,
      displayName: "主要背景音樂",
      default: null
    },

    backgroundMusic: {
      default: [],
      type: [cc.AudioClip],
      displayName: "背景音樂"
    },

    backgroundMusicVolume: {
      default: 1,
      type: cc.Float,
      displayName: "背景音樂音量"
    },

    soundEffect: {
      default: [],
      type: [cc.AudioClip],
      displayName: "音效",
      tooltip: "將音效依序拉進來"
    },

    spinSound: {
      type: cc.AudioClip,
      displayName: "Spin音效",
      tooltip: "Spin音效",
      default: null
    },

    publicKeySound: {
      default: null,
      type: cc.AudioClip,
      displayName: "按鍵音效"
    },

    publicGetMoney: {
      default: null,
      type: cc.AudioClip,
      displayName: "歸分音效"
    }
  },

  playKeySound() {
    if (this.publicKeySound == null) return;
    return this.PlaySound(this.publicKeySound);
  },

  playGetMoney() {
    if (this.publicGetMoney == null) return;
    return this.PlaySound(this.publicGetMoney);
  },

  playScatterSound() {
    if (this.publicScatterSound == null) return;
    return this.PlaySound(this.publicScatterSound);
  },

  preload() {
    this.register_code();
  },

  onLoad() {
    this.nowMusic = -1;
    this.isStatsPlayBGM = true;
    this.isStatsPlaySound = true;
    this.check_audio_context_state();
    cc.Sound = this;
  },

  check_audio_context_state() {
    this.schedule(
      () => {
        if (
          /* cc.sys.isMobile &&  */ cc.sys.__audioSupport.context.state ==
          "suspended"
        ) {
          cc.sys.__audioSupport.context.resume();
        }
        if (cc.sys.__audioSupport.context.state == "running") return;
      },
      0.5,
      10,
      0
    );
  },

  start() {
    this.isStatsPlayBGM = true;
    this.isStatsPlaySound = true;
    if (this.mainGameMusic != null) {
      this.backgroundMusic[-1] = this.mainGameMusic;
      this.scheduleOnce(this.playMainMusic, 1);
    }
  },

  register_code() {
    cc.GAME_WORLD.sound = this;
    GW.regist_code("sound", "var sound = cc.GAME_WORLD.sound;");
  },

  playMainMusic() {
    this.PlayBGM(-1);
  },

  stopMusic() {
    console.log("music:", this.nowBGMAudioID);
    if (this.nowBGMAudioID == null) return;
    cc.audioEngine.stop(this.nowBGMAudioID);
    this.nowBGMAudioID = null;
  },

  PlayBGM(index = 0) {
    if (index == this.nowMusic && this.nowBGMAudioID != null) return;
    this.nowMusic = index;
    if (this.isStatsPlayBGM == false) return;
    this.playMusic(this.backgroundMusic[index]);
  },

  playMusic(clip) {
    switch (clip.name) {
      case "maingame":
        this.nowMusic = 0;
        break;
      case "freegame":
        this.nowMusic = 1;
        break;
    }
    if (this.isStatsPlayBGM == false) return;
    if (clip == null) return;
    this.stopMusic();
    this.nowBGMAudioID = cc.audioEngine.play(
      clip,
      true,
      this.backgroundMusicVolume
    );
    console.log("music:", this.nowBGMAudioID);
  },

  changeMusicVolume(value) {
    cc.audioEngine.setVolume(this.nowBGMAudioID, value);
  },

  isEnableBGM(change = null) {
    if (change == null) return this.isStatsPlayBGM;

    this.isStatsPlayBGM = change;
    if (change == true) this.PlayBGM(this.nowMusic);
    else this.stopMusic();
    return change;
  },

  isEnableSound(change = null) {
    if (change == null) return this.isStatsPlaySound;
    this.isStatsPlaySound = change;
    //if (change == false) cc.audioEngine.stopAllEffects();

    return change;
  },

  PlaySoundEffect(number) {
    if (this.isStatsPlaySound == false) return;
    cc.audioEngine.playEffect(this.soundEffect[number], false);
  },

  PlaySound(clip, loop = false) {
    if (this.isStatsPlaySound == false) return;
    return cc.audioEngine.playEffect(clip, loop);
  },

  playSpinSound() {
    if (this.spinSound == null) return;
    this.PlaySound(this.spinSound);
  },

  stop_sound_effect(audio_id) {
    if (audio_id == null) return;
    cc.audioEngine.stopEffect(audio_id);
  }
});
