
// 一種語系對應一組 Label 設定及多組 Font
var language_cell = cc.Class({
    name:"language_cell",
    properties: {
        LanguageType:{
            default:"en-US",
            tooltip: "en-US, zh-TW, zh-CN ...",
        },
        add_FontSize:{
            type:cc.Float,
            default:0,
            displayName:"額外字體加減",
            tooltip:"以當前 Label 字體大小作為基準，當切換到此語言時，進行累加。",
        },
        add_LineHeight:{
            type:cc.Float,
            default:0,
            displayName:"額外行高加減",
            tooltip:"以當前 Label 行高高度作為基準，當切換到此語言時，進行累加。",
        },
    },
});
// 一個 Lable 對應一個多國語言 ID
var label_cell = cc.Class({
    name:"label_cell",
    properties: { 
        LanguageID: {
            default: "",
        },
        Label:{
            type:cc.Label,
            default:null,
        }
    },
});

cc.Class({
    extends: cc.Component,

    properties: {
        language_ary:{
            type:[language_cell],
            default:[],
            displayName:"Label 設定",
        },
        label_ary:{
            type:[label_cell],
            default:[],
            displayName:"欲列管的 Label",
            tooltip:"需要多國語言的 Label，可以 assign 進來做管理。",
        }
    },
 
    start(){
        this.node.setting = this;
        this.language_setting = {};
        if ( this.language_ary.length > 0) {
            this.language_ary.forEach(element => {
                this.language_setting[element.LanguageType] = element;
            });
        }
        if ( this.label_ary.length == 0 ) return;
        this.label_ary.forEach(element => {
            element.Label.node.LanguageID = element.LanguageID;
            element.Label.node.label = element.Label;
        });
        this.add_language_labels();
    },
    // 加入 i18n_m 中的列表列管
    add_language_labels() {
        if ( cc.Language == null ) return this.scheduleOnce(this.add_language_labels,1);
        this.label_ary.forEach(element => {
            cc.Language.add_language_label(element.Label.node);
        });
    },
});
