var language_node = cc.Class({
    name : 'language_node',
    properties : {
        LanguageType: {
            default: 'en-US',
            displayName: '語系代號',
            tooltip: "en-US, zh-TW, zh-CN ...",
        },

        LanguageNode: {
            type: cc.Node,
            default : null,
            displayName: '該系的UI',
        },
    },
});

cc.Class({
    extends: cc.Component,

    properties: {
        i18n_nodes : {
            type: [ language_node ],
            default: [],
            displayName: '語系UI對換表',
        },
    },

    disable_ui() { this.i18n_nodes.forEach(element => { element['LanguageNode'].active = false; }); },

    search_ui(type) {
        for(var idx in this.i18n_nodes) {
            var data = this.i18n_nodes[idx];
            if ( data['LanguageType'] == type ) return data['LanguageNode'];
        }

        return null;
    },

    change_i18n_ui() {
        if ( cc.Language == null ) return this.scheduleOnce(this.change_i18n_ui,0);
        var type = cc.Language.LanguageType;
        var node = this.search_ui(type);
        if ( node == null ) return;

        this.disable_ui();
        node.active = true;
    },

    start() { return this.change_i18n_ui(); },
    onLoad() { return this.change_i18n_ui(); },
    onEnable() { return this.change_i18n_ui(); },
});
