cc.Class({
    extends: cc.Component,
    
    properties: {

        LanguageType : {
            default: "en-US",
            displayName: "預設語言",
            tooltip: "en-US, zh-TW, zh-CN ..."
        },

        LanguageFiles: {
            type : [cc.String],
            default: [],
            displayName: "多國語言文字對應表",
        },

        LineEndMark:{
            default:"/EOL",
            displayName:"分行符號"
        },

        Label_Setting:{
            type: cc.Node,
            default:null,
            displayName:"Label Global 設定集",
            tooltip:"可不指派"
        },

        ImageFolder : {
            default: "",
            displayName: "美術字i18n目錄",
        },
    },

    GetURLParam() {
        console.log(cc.system);
        cc.system.parseURLParams();
        if (cc.system == null) return;
        if (cc.system.urlParams == null || cc.system.urlParams["lan"] == null) return;
        var url_lan = cc.system.urlParams["lan"][0];
        if (url_lan == null || url_lan == "") return;
        this.LanguageType = url_lan;
    },

    register_class() {
        if (document.contains(document.getElementById('i18n')) == true) {
            document.body.removeChild(document.getElementById('i18n'));
        }
        cc.Language = this;
        var js = document.createElement("script");
        js.type = "text/javascript";
        js.id = 'i18n';
        js.innerHTML = "var i18n = cc.Language;";
        document.body.appendChild(js);
    },

    start () {
        this.register_class();
        this.GetURLParam();
        if ( this.LanguageFiles.length < 1 ) return;
        this.language_data = {};
        this.remain_file_count = this.LanguageFiles.length;
        this.LanguageFiles.forEach(file => {
            cc.loader.loadRes('language/'+file, cc.TextAsset, this.parse_language_file);
        });
        //this.load_sprite_folder();
    },

    /**
     * 解析多國語言文字表
     * @private from this.start()
     * @param { string } err 讀取文字檔案，的錯誤訊息
     * @param { JSON } data 文字檔案的內容 
     * @todo 獲得檔案內容後會做解析動作，把資料都集中在 this.language_data 中
     */
    parse_language_file(err, data) {
        if ( data == null ) return;
        var str = data.text;
        var table = str.substring(str.search("#ID"), str.length); //從 #ID 作起點
        var keys = [];
        var len = table.split("\r\n");
        var len = table.split(i18n.LineEndMark);
        //if ( len.length < 3 ) len = table.split(i18n.LineEndMark + "\n");
        //if ( len.length < 3 ) len = table.split(i18n.LineEndMark);

        var lanStart = false; // 遇到 #ID 才會變成true
        len.forEach(line => {
            while(true) {
                var new_line = line.replace("\t\n", "");
                new_line = new_line.replace("\t\t", "");
                if ( new_line.charAt(0) == "\n" ) new_line = new_line.substring(0);
                if ( new_line.length == line.length ) break;
                line = new_line;
            }
            if ( lanStart == true ) {
                if ( line.charAt(0) == '#' ) return; // 註解欄位，不處理
                i18n.import_language_data(keys, line.split("\t"));
            } else {
                if ( line.indexOf("#ID") == -1 ) return;
                lanStart = true;
                keys = line.split("\t");
            }
        });
        i18n.remain_file_count--;
        if (i18n.remain_file_count == 0) i18n.change_all_label();
    },

    /**
     * 將資料匯入 this.language_data 中 
     * @private from this.parse_language_file 
     * @param { array } keys string 陣列，代號 
     * @param { array } data string 陣列，資料 
     */
    import_language_data(keys, data) {
        
        if ( data == null ) return;
        if ( data.length < 1 ) return;
        data[0] = data[0].replace("\n", "");
        data[0] = data[0].replace(" ", "");
        var _lanData = {};
        for(var i=1;i<keys.length;i++) {
            if ( keys[i].length < 1 ) continue;
            if ( keys[i] == " " ) continue;
            if ( i > data.length - 1) break;
            // 遇到 <br> 要斷行
            data[i] = data[i].replace("<br>", "\n");
            if (data[i].startsWith("\"")) data[i] = data[i].substring(1, data[i].length); 
            if (data[i].endsWith("\"")) data[i] = data[i].substring(0, data[i].length - 1);
            _lanData[keys[i]] = data[i];
        }
        var id = data[0];
        id = id.replace("\r","");
        id = id.replace("\n","");
        id = id.replace(" ","");
        if ( id.charAt(0) == '#' ) return; // 註解欄位，不處理
        this.language_data[id] = _lanData;
    },

    /**
     * 透過文字編號與語言狀態(this.LanguageType)，取得文字
     * @param {string} id 文字編號
     */
    get_language_data(id) {
        if ( this.language_data[id] == null ) return null;
        return this.language_data[id][this.LanguageType];
    },

    /**
     * 當有多國語言文字的 Label 被打開時，會主動加入列管
     * @private @from Component.start
     * @param { LanguageLabel Component } label LanguageLabel 
     */
    add_language_label(label) {
        if ( label == null ) return;
        if ( this.label_list == null ) this.label_list = [ label ];
        else {
            if ( this.label_list.indexOf(label) != -1 ) return;
            else this.label_list.push(label);
        }

        if ( this.isDone != true) return;
        
        return this.change_label(label);
    },

    /** 
     * 將指定的 LanguageLabel 轉換語言
     * @param { LanguageLabel Component } label 
    */
    change_label(label) {
        if ( label == null ) return;

        var id = label.LanguageID;
        if ( id.length < 1 || id == null ) return;

        var content = this.get_language_data(id);
        if ( content == null ) return;
        label.label.string = content;

        if ( this.Label_Setting == null ) return;
        if ( this.Label_Setting.setting.language_setting == null || this.Label_Setting.setting.language_setting.length == 0 ) return;
        if ( this.Label_Setting.setting.language_setting[i18n.LanguageType] == null ) return;
        label.label.fontSize += this.Label_Setting.setting.language_setting[i18n.LanguageType].add_FontSize;
        label.label.lineHeight += this.Label_Setting.setting.language_setting[i18n.LanguageType].add_LineHeight;
    },

    /**
     * 更改列管中所有的語言
     */
    change_all_label() { 
        this.isDone = true;
        
        if ( this.label_list == null ) return;
        if ( this.label_list.length == 0 ) return;

        this.label_list.forEach(label => { i18n.change_label(label); });
    },


    load_sprite_folder() {        
        cc.loader.downloader.loadSubpackage("/", function (err) {
            if (err) {
                return console.error(err);
            }
            console.log('load subpackage successfully.');
        });
    },

    add_language_sprite(sprite) {
        
        if ( sprite == null ) return;
        if ( this.sprite_list == null ) this.sprite_list = [sprite];
        else this.sprite_list.push(sprite);

        if ( this.isDone != true ) return;
        this.change_sprite(sprite);
    },

    chabge_sprite(sprite) {
        if ( sprite == null ) return;

    },
});
