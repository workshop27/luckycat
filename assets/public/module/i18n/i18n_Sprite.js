
cc.Class({
    extends: cc.Component,

    properties: {
        file : {
            default: "",
            displayName: "i18n目錄下的檔案名稱",
        },
    },

    start () { 
        this.sprite = this.node.getComponent(cc.Sprite);
        if (this.sprite == null) {
            let parent = this.node.parent == null ? "" : "(父物件為" + this.node.parent.name +")";
            let msg = "物件 \"" + this.node.name + "\"" + parent + " 使用到 i18n_Sprite.js 卻未賦予 cc.Sprite 元件！";
            alert(msg);
            throw new Error(msg);            
        }
        this.add_language_sprite();
    },

    add_language_sprite() {
        if ( cc.Language == null ) return this.scheduleOnce(this.add_language_sprite,1);
        cc.Language.add_language_sprite(this);
    },
});
