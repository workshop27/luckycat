
cc.Class({
    extends: cc.Component,

    properties: {
        LanguageID: "",
    },

    start () {
        this.node.label = this.node.getComponent(cc.Label);
        this.node.LanguageID = this.LanguageID;
        if (this.node.label == null){
            let parent = this.node.parent == null ? "" : "(父物件為" + this.node.parent.name + ")";
            let msg = "物件 \"" + this.node.name + "\"" + parent + " 使用到 i18n_Label.js 卻未賦予 cc.Label 元件！";
            alert(msg);
            throw new Error(msg); 
        } 
        this.add_language_label();
    },

    add_language_label() {
        if ( cc.Language == null ) return this.scheduleOnce(this.add_language_label,0);
        cc.Language.add_language_label(this.node);
    },
    
});
