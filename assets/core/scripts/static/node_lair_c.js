
/**
 * 物件池系統
 */
var lair = cc.Class({
    extends: cc.Component,

    register_code() {
        cc.lair = this;
    },
    
    onLoad() {
        this.register_code();
        this.lairData = {};
        this.baseNode = {};
    },

    /**
     * 登陸物件與編號
     * @param { string } node_id 編號
     * @param { cc.Node } base_node 主體物件
     */
    register_node(node_id, base_node) {
        this.baseNode[node_id] = base_node;
    },

    /**
     * 複製物件
     * @param { string } node_id 編號
     * @return { cc.Node } 實體物件
     * @private 請使用 take_node 
     */
    clone_node(node_id) {
        if ( this.baseNode[node_id] == null ) return null;
        var node = cc.instantiate(this.baseNode[node_id]);
        node.id = node_id;
        return node;
    },

    /**
     * 取出物件
     * @param { string } node_id 物件編號
     * @return { cc.Node } 實體物件
     */
    take_node(node_id) {
        if ( this.lairData[node_id] == null || this.lairData[node_id].length == 0 ) {
            return this.clone_node(node_id);
        }
        var first = this.lairData[node_id][0];
        this.lairData[node_id].shift();
        return first;
    },

    /**
     * 放入物件
     * @param { cc.Node } node 物件
     * @param { string | null } node_id 物件編號
     */
    put_node(node, node_id=null) {
        if ( node == null ) return;
        if ( node_id == null ) node_id = node.id;
        node.active = false;
        node.parent = this.node;
        node.x = 0;
        node.y = 0;

        if ( this.lairData[node_id] == undefined ) {
            this.lairData[node_id] = [node];
            
        } else {
            this.lairData[node_id].push(node);
        }
    },



});
