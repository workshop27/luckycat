// Learn cc.Class:
//  - https://docs.cocos.com/creator/manual/en/scripting/class.html
// Learn Attribute:
//  - https://docs.cocos.com/creator/manual/en/scripting/reference/attributes.html
// Learn life-cycle callbacks:
//  - https://docs.cocos.com/creator/manual/en/scripting/life-cycle-callbacks.html

cc.Class({
    extends: cc.Component,

    properties: {
        MusicOn: {
            type: cc.Node,
            default: null,
        },

        MusicOff: {
            type: cc.Node,
            default: null,
        },

        SoundOn: {
            type: cc.Node,
            default: null,
        },

        SoundOff: {
            type: cc.Node,
            default: null,
        },
    },

    start () {
        cc.Option = this;
    },

    click_music_button() {
        var on = cc.Sound.isEnableBGM(!cc.Sound.isStatsPlayBGM);
        if ( on == true ) {
            this.MusicOff.active = false;
            this.MusicOn.active = true;
        } else {
            this.MusicOff.active = true;
            this.MusicOn.active = false;
        }
    },

    click_sound_button() {
        var on = cc.Sound.isEnableSound(!cc.Sound.isStatsPlaySound);
        if ( on == true ) {
            this.SoundOff.active = false;
            this.SoundOn.active = true;
        } else {
            this.SoundOff.active = true;
            this.SoundOn.active = false;
        }
    },

});
