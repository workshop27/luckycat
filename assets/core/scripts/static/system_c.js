Number.prototype.format_money = function(decimal=5, localstyle=null) {
    if ( localstyle == null ) localstyle = { style: 'decimal', maximumFractionDigits: decimal,};
    // return parseFloat(this.toFixed(decimal)).toLocaleString(localstyle);
    return this.toLocaleString('en', localstyle);
};

Number.prototype.thousands = Number.prototype.format_money;

Number.prototype.add_float = function(num2, num1=this) {
    return num1.accAdd(num2, num1);
};

Number.prototype.accDiv = function(arg2,arg1=this) { 
    var t1=0,t2=0,r1,r2; 
    try {
        t1=(arg1.toString().split(".")[1] || '').length;
    } catch(e){} 
    try {
        t2=(arg2.toString().split(".")[1] || '').length;
    } catch(e){} 
        
    r1=Number(arg1.toString().replace(".",""));
    r2=Number(arg2.toString().replace(".",""));
    return (r1/r2)*Math.pow(10,t2-t1); 
}

//乘法
Number.prototype.accMul = function(arg2,arg1=this) { 
    var m=0,s1=arg1.toString(),s2=arg2.toString(); 
    try {
        m+=(s1.split(".")[1] || '').length;
    } catch(e){} 
    try {
        m+=(s2.split(".")[1] || '').length;
    } catch(e){} 
    return Number(s1.replace(".",""))*Number(s2.replace(".",""))/Math.pow(10,m);
}

//加法
Number.prototype.accAdd = function(arg2,arg1=this) { 
    var r1,r2,m; 
    try{
        r1=(arg1.toString().split(".")[1] || '').length;
    } catch(e){r1=0} 
    try{
        r2=(arg2.toString().split(".")[1] || '').length;
    } catch(e){r2=0} 
    m=Math.pow(10,Math.max(r1,r2));
    var arg1_m = Number(arg1).accMul(m);
    var arg2_m = Number(arg2).accMul(m);
    var arg_add = arg1_m + arg2_m;
    var total = arg_add / m;
    return total;
} 

//減法
Number.prototype.accSubtr = function(arg2,arg1=this) {
    var r1,r2,m,n;
    try {
        r1=(arg1.toString().split(".")[1] || '').length;
    } catch(e){r1=0}
    try {
        r2=(arg2.toString().split(".")[1] || '').length;
    } catch(e){r2=0}
    m=Math.pow(10,Math.max(r1,r2));
    n=(r1>=r2)?r1:r2;
    arg1 = Number(arg1);
    arg2 = Number(arg2);
    return Number(((arg1*m-arg2*m)/m).toFixed(n));
}

Number.prototype.float_count = function(type, value2) {
    var result;
    switch (type) {
        case '+' :
            result = this.accAdd(value2);
            break;
        case '-' :
            result = this.accSubtr(value2);
            break;
        case '*' :
            result = this.accMul(value2);
            break;
        case '/' :
            result = this.accDiv(value2);
            break;
        default  :
            result = null;
            console.error("Unable to resolve operation \"" + type + "\" type");
            break;
    }
    return result;
}

cc.Class({
    
    extends: cc.Component,
    properties: {},
    os : null, // 裝置系統 PC or Andorid or IOS
    urlParams : null,

    onLoad() {
        cc.notify_fail = function(msg) {
            console.error(msg);
            alert(msg);
        };
    },

    start() {
        // 建議從這裡處理連線功能
    },

    /**
     * 回傳目前裝置是否是手機
     */
    is_mobile() {
        if ( this.os == cc.sys.OS_ANDROID ) return true;
        if ( this.os == cc.sys.OS_IOS ) return true;

        return false;
    },

    /**
     * 解析網頁GET內容
     */
    parseURLParams() {
        var url = window.location.search;
        var queryStart = url.indexOf("?") + 1,
            queryEnd = url.indexOf("#") + 1 || url.length + 1,
            query = url.slice(queryStart, queryEnd - 1),
            pairs = query.replace(/\+/g, " ").split("&"),  //replace(/\+/g, " ")???
            parms = {}, i, n, v, nv;

        //以下看不懂
        if (query === url || query === "") return;

        for (i = 0; i < pairs.length; i++) {
            nv = pairs[i].split("=", 2);
            n = decodeURIComponent(nv[0]);
            v = decodeURIComponent(nv[1]);

            if (!parms.hasOwnProperty(n)) parms[n] = [];
            parms[n].push(nv.length === 2 ? v : null);
        }
        this.urlParams = parms;
        console.log("params:");
        console.log(parms);
        return parms;
    },
});
