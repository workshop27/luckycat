cc.Class({
    extends: cc.Component,

    properties: {
        Progress : {
            type: cc.ProgressBar,
            default:null,
        }
    },

    start () {
        cc.loading = this;
        console.log("start", this.Progress);
        cc.director.preloadScene("LuckyCat", this.onProgress, this.enterGame);
    },

    enterGame() {
        cc.loading.scheduleOnce(function(){
            cc.director.loadScene("LuckyCat");
        },1);
    },

    onProgress(completeCount, totalCount, item) {
        var process = (completeCount/totalCount);
        cc.loading.Progress.progress = process;
    }

});
