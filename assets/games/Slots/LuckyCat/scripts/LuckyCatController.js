cc.Class({
  extends: cc.Component,

  properties: {
    BeginFreeGameUI: {
      type: cc.Node,
      default: null,
    },

    BeginFreeGameLabel: {
      type: cc.Label,
      default: null,
    },

    EndFreeGameUI: {
      type: cc.Node,
      default: null,
    },

    EndFreeGameTotalScore: {
      type: cc.Label,
      default: null,
    },

    MainGameReel: {
      type: cc.Node,
      default: null,
    },

    FreeGameReel: {
      type: cc.Node,
      default: null,
    },

    MainGameSymbolSize: {
      type: cc.Size,
      default: null,
    },

    FreeGameSymbolSize: {
      type: cc.Size,
      default: null,
    },

    MainGameBG: {
      type: cc.Node,
      default: null,
    },

    FreeGameBG: {
      type: cc.Node,
      default: null,
    },

    FreeGameInfo: {
      type: cc.Node,
      default: null,
    },

    ReelShakeAnime: {
      type: cc.Node,
      default: null,
    },

    Reel0Effect: {
      type: [cc.Node],
      default: [],
    },

    Reel1Effect: {
      type: [cc.Node],
      default: [],
    },

    Reel2Effect: {
      type: [cc.Node],
      default: [],
    },

    Reel3Effect: {
      type: [cc.Node],
      default: [],
    },

    Reel4Effect: {
      type: [cc.Node],
      default: [],
    },

    FreeReel0Effect: {
      type: [cc.Node],
      default: [],
    },

    FreeReel1Effect: {
      type: [cc.Node],
      default: [],
    },

    FreeReel2Effect: {
      type: [cc.Node],
      default: [],
    },

    FreeReel3Effect: {
      type: [cc.Node],
      default: [],
    },

    FreeReel4Effect: {
      type: [cc.Node],
      default: [],
    },

    FreeReel5Effect: {
      type: [cc.Node],
      default: [],
    },

    FG_SYMBOL_PARENT: {
      type: cc.Node,
      default: null,
    },

    FG_CHANGE: {
      type: cc.Node,
      default: null,
    },

    FG_RANSYMBOL: {
      type: cc.Node,
      default: null,
    },

    FG_SYMBOL_DOWN: {
      type: [cc.Node],
      default: [],
    },

    MASK: {
      type: [cc.Node],
      default: [],
    },

    PAYLINE_LINE: {
      type: [cc.Node],
      default: [],
    },

    MainGameBGSound: {
      type: cc.AudioClip,
      default: null,
    },

    FreeGameBGSound: {
      type: cc.AudioClip,
      default: null,
    },

    ReelEffectSound: {
      type: cc.AudioClip,
      default: null,
    },

    ClickSound: {
      type: cc.AudioClip,
      default: null,
    },

    EnterFGSymbolSound: {
      type: cc.AudioClip,
      default: null,
    },

    EnterFGSound: {
      type: cc.AudioClip,
      default: null,
    },

    WaveBoxSound: {
      type: cc.AudioClip,
      default: null,
    },

    SymbolDownSound: {
      type: cc.AudioClip,
      default: null,
    },

    TotalSound: {
      type: cc.AudioClip,
      default: null,
      displayName: "TotalWin音效",
    },

    RollingSound: {
      type: cc.AudioClip,
      default: null,
      displayName: "滾分音效",
    },
  },

  start() {
    this.MAIN_BIG_FIX_X = [0, 0, 61, 119, 176];
    this.MAIN_BIG_FIX_Y = [0, 110, 110, 110, 110];
    this.FREE_BIG_FIX_X = [0, 0, 32, 66, 110, 125];
    this.FREE_BIG_FIX_Y = [0, 0, 0, 0, 0];

    this.close_symbol_change_effect();

    this.AllReelEffect = {
      0: this.Reel0Effect,
      1: this.Reel1Effect,
      2: this.Reel2Effect,
      3: this.Reel3Effect,
      4: this.Reel4Effect,
    };

    this.FreeReelEffect = {
      0: this.FreeReel0Effect,
      1: this.FreeReel1Effect,
      2: this.FreeReel2Effect,
      3: this.FreeReel3Effect,
      4: this.FreeReel4Effect,
      5: this.FreeReel5Effect,
    };

    this.scheduleOnce(function() {
      this.register_fg_symbol();
    }, 1);
  },

  register_fg_symbol() {
    cc.Sound.playMusic(this.MainGameBGSound);
    if (this.FG_SYMBOL_PARENT == null) return;
    var childs = this.FG_SYMBOL_PARENT.children;
    for (var idx in childs) {
      var symbol = childs[idx];
      // console.log(symbol);
      cc.lair.register_node(symbol.sym.SymbolID, symbol);
    }

    // console.log(cc.lair);
  },

  onLoad() {
    this.BeginFreeGameUI.active = false;
    this.FreeGameReel.active = false;
  },

  close_reel_effect(reelEffect) {
    if (reelEffect == null) return;
    for (var index in reelEffect) {
      reelEffect[index].active = false;
    }
  },

  close_symbol_change_effect() {
    this.close_reel_effect(this.Reel0Effect);
    this.close_reel_effect(this.Reel1Effect);
    this.close_reel_effect(this.Reel2Effect);
    this.close_reel_effect(this.Reel3Effect);
    this.close_reel_effect(this.Reel4Effect);
    this.close_reel_effect(this.FreeReel0Effect);
    this.close_reel_effect(this.FreeReel1Effect);
    this.close_reel_effect(this.FreeReel2Effect);
    this.close_reel_effect(this.FreeReel3Effect);
    this.close_reel_effect(this.FreeReel4Effect);
    this.close_reel_effect(this.FreeReel5Effect);
  },

  begin_free_game_ui(symbol, param) {
    cc.Sound.PlaySound(this.EnterFGSymbolSound);
    cc.machine.reel_frame.symbol_id_show_payline(symbol, false);
    cc.machine.status.bonus_gaming = true;
    this.FG_RANSYMBOL.active = false;
    this.FG_CHANGE.active = false;
    cc.Sound.stopMusic();

    this.scheduleOnce(function() {
      cc.machine.payline.show_end_payline(true);
      this.BeginFreeGameLabel.string = param["bonus_count"];
      this.BeginFreeGameUI.active = true;
      this.scheduleOnce(function() {
        this.change_fg(param["bonus_count"]);
      }, 3);
    }, 3);
  },

  change_fg(roundNum) {
    cc.Sound.PlaySound(this.EnterFGSound);
    this.FG_CHANGE.active = true;
    this.scheduleOnce(function() {
      this.MainGameReel.active = false;
      this.FreeGameReel.active = true;
      this.MainGameBG.active = false;
      this.FreeGameBG.active = true;
      this.BeginFreeGameUI.active = false;
      // 設定FG遊戲總贏分
      cc.machine.slot_control_bar.set_total_win(0);
      // 設定免費遊戲次數
      cc.machine.slot_control_bar.set_free_spins(roundNum);
      // 打開FG剩餘次數與總贏分面板
      this.FreeGameInfo.active = true;
      cc.machine.payline.Mask = this.MASK[1];
      cc.machine.payline.Mask.active = false;
      cc.machine.payline.LinesParent = this.PAYLINE_LINE[1];
      cc.machine.payline.load_line();

      this.scheduleOnce(function() {
        cc.Sound.PlaySound(this.WaveBoxSound);
        var fg_dragon_effect = this.FG_RANSYMBOL.getComponent(
          dragonBones.ArmatureDisplay
        );
        var symbol = cc.machine.status.round_data.result.fg_symbol;
        var aniSymbolNumber = [
          "50",
          "51",
          "52",
          "53",
          "54",
          "55",
          "56",
          "57",
          "58",
        ];
        fg_dragon_effect.animationName = aniSymbolNumber[symbol];
        this.FG_RANSYMBOL.active = true;

        this.scheduleOnce(function() {
          cc.Sound.PlaySound(this.SymbolDownSound);
          for (var idx in this.FG_SYMBOL_DOWN) {
            this.FG_SYMBOL_DOWN[idx].active = false;
          }
          this.FG_SYMBOL_DOWN[symbol].active = true;
          this.FG_SYMBOL_DOWN[0].parent.active = true;
          this.scheduleOnce(function() {
            cc.Sound.playMusic(this.FreeGameBGSound);
            this.start_free_game(roundNum);
          }, 2);
        }, 4);
      }, 4);
    }, 3);
  },

  start_free_game(roundNum) {
    this.FG_RANSYMBOL.active = false;
    this.FG_CHANGE.active = false;
    cc.machine.reel_frame = this.FreeGameReel.getComponent("change_reel");
    this.free_game_score = 0;
    this.result = cc.machine.status.round_data.result;

    this.setSymbolBgDisable();

    cc.tween(this.node)
      .delay(2)
      .call(() => {
        this.spin_round(1, roundNum);
      })
      .start();
  },

  begin_end_free_game_ui() {
    cc.Sound.stopMusic();
    cc.Sound.PlaySound(this.TotalSound);
    this.MainGameReel.active = true;
    this.FreeGameReel.active = false;
    cc.machine.reel_frame = this.MainGameReel.getComponent("change_reel");
    this.MainGameBG.active = true;
    this.FreeGameBG.active = false;
    this.FG_SYMBOL_DOWN[0].parent.active = false;

    cc.machine.payline.Mask = this.MASK[0];
    cc.machine.payline.Mask.active = false;
    cc.machine.payline.LinesParent = this.PAYLINE_LINE[0];
    cc.machine.payline.load_line();

    this.EndFreeGameUI.active = true;
    let sound_id = cc.Sound.PlaySound(this.RollingSound, true);
    let score = this.EndFreeGameTotalScore.getComponent("scroll_number_m");
    score.start_scroll(
      0,
      this.free_game_score,
      () => {
        cc.Sound.stop_sound_effect(sound_id);
      },
      this,
      (value) => {
        // this.EndFreeGameTotalScore.string = value;
      }
    );
  },

  close_end_free_game_ui() {
    cc.audioEngine.stopAllEffects();
    cc.Sound.PlaySound(this.ClickSound);
    this.BeginFreeGameUI.active = false;
    this.EndFreeGameUI.active = false;
    // 關閉FG剩餘次數與總贏分面板
    this.FreeGameInfo.active = false;
    cc.machine.status.bonus_gaming = false;
    cc.machine.status.bonus_done = true;
    this.scheduleOnce(function() {
      // if ( cc.machine.check_big_win() == true ) cc.machine.run_big_win();
      cc.machine.status.bonus_done = false;
      cc.machine.back_to_normal();
      cc.Sound.playMusic(this.MainGameBGSound);
    }, 1);
  },

  check_free_game_end(idx, roundNum) {
    idx++;
    var result = cc.machine.status.round_data.result;
    if (result["all_round_data"][idx] == null)
      return this.begin_end_free_game_ui();

    var reel_frame = cc.machine.reel_frame.status.reel_data;
    var total_score = result["all_round_data"][idx]["total_score"];
    // if (total_score > 1) {
    //   this.change_symbol_back(reel_frame, false);
    // } else {
    //   this.change_symbol_back(reel_frame, true);
    // }

    return this.scheduleOnce(function() {
      this.spin_round(idx, roundNum);
    }, 1);
  },

  spin_response_break(reel_response) {
    // console.log('spin_response_break', reel_response);
    return false;
  },

  spin_round(idx, roundNum) {
    // cc.machine.payline.show_end_payline(true);
    roundNum -= 1;
    cc.machine.slot_control_bar.set_free_spins(roundNum);
    cc.machine.reel_frame.start_spin();
    cc.Sound.PlaySound(this.ClickSound);
    var result = cc.machine.status.round_data.result;
    // 贏分數字歸零
    cc.machine.slot_control_bar.set_win_score_label(0);
    this.scheduleOnce(function() {
      // 執行Spin
      cc.machine.reel_frame.spin_response(
        result["all_round_data"][idx]["reel_frame"]
      );
      this.change_reel(idx);

      this.scheduleOnce(function() {
        var total_score = result["all_round_data"][idx]["total_score"];
        if (total_score < 1) {
          this.close_symbol_change_effect();
          var reel_frame = result["all_round_data"][idx]["reel_frame"];
          this.change_symbol_back(reel_frame, true);

          //  判斷是否加局
          var fSNumber = this.isAddRound(reel_frame);
          if (fSNumber !== 0) {
            roundNum += fSNumber;
            this.playFgFSShow(idx, roundNum);
          } else {
            cc.tween(this.node)
              .delay(1)
              .call(() => {
                this.check_free_game_end(idx, roundNum);
              })
              .start();
          }
        } else {
          this.free_game_score += total_score;
          this.scheduleOnce(function() {
            this.close_symbol_change_effect();
            /** 設定盤面  一次給所有SPIN結果 NG +FG*/
            var reel_frame = result["all_round_data"][idx]["reel_frame"];
            this.change_symbol_back(reel_frame, false);
            // 顯示單局贏分
            cc.machine.slot_control_bar.set_win_score_label(total_score);
            // 顯示總贏分
            cc.machine.slot_control_bar.set_total_win(this.free_game_score);
            // 打開FG滾輪遮罩
            cc.machine.payline.active_FgMask(true);
            cc.machine.payline.show_all_payline(result["all_round_data"][idx]);
            cc.machine.payline.call_stop();

            this.scheduleOnce(function() {
              // 一秒之後 關閉得分效果
              cc.machine.payline.active_FgMask(false);
              cc.machine.payline.show_end_payline(true);
              cc.machine.reel_frame.show_end_payline();

              // 判斷是否加局
              var fSNumber = this.isAddRound(reel_frame);
              if (fSNumber !== 0) {
                roundNum += fSNumber;
                this.playFgFSShow(idx, roundNum);
              } else {
                cc.tween(this.node)
                  .delay(1)
                  .call(() => {
                    this.check_free_game_end(idx, roundNum);
                  })
                  .start();
              }
            }, 3);
          }, 1);
        }
      }, 1);
    }, 2);
  },

  /** 只播那個特殊SYMBOL */
  playFgFSShow(idx, roundNum) {
    cc.machine.slot_control_bar.set_free_spins(roundNum);
    cc.machine.reel_frame.symbol_id_show_payline("109", false);

    cc.tween(this.node)
      .delay(3)
      .call(() => {
        cc.machine.payline.show_end_payline(true);
        this.check_free_game_end(idx, roundNum);
      })
      .start();
  },

  // 判斷是否加局
  isAddRound(reel_frame) {
    var fSNumber = 0;
    reel_frame.forEach((data) => {
      data.forEach((symbol) => {
        if (symbol === 9) {
          fSNumber += 1;
        }
      });
    });
    return fSNumber;
  },

  put_symbol_id(data) {
    if (cc.machine.status.bonus_gaming == false) return null;
    var reel_id = data[0];
    var sym_id = Number.parseInt(data[1]);
    if (sym_id >= 100) return sym_id;
    sym_id += 100;
    return sym_id;
  },

  /** 放置Symbol */
  put_symbol(data) {
    if (data == null) return;
    if (data[0] == null) return;
    if (data[1] == null) return;

    var reel_id = data[0];
    var sym_node = data[1];

    // 判斷MG OR FG
    var nowState = "FG";
    if (cc.machine.reel_frame.Reels[reel_id].parent.name == "Background") {
      nowState = "NG";
    }

    var big = cc.machine.reel_frame.Reels[reel_id].big;
    var sym = sym_node.getComponent("symbol");

    var size;
    if (cc.machine.status.bonus_gaming != true) {
      size = new cc.Size(
        this.MainGameSymbolSize.width,
        this.MainGameSymbolSize.height
      );
    } else {
      var fgSymbol = cc.machine.status.round_data.result.fg_symbol + 100 + "";
      var light = sym_node.getChildByName("FG_CASE_X5_ske");
      size = new cc.Size(
        this.FreeGameSymbolSize.width,
        this.FreeGameSymbolSize.height
      );

      if (light != null) {
        if (sym.SymbolID == fgSymbol && this.FG_CHANGE.active != true) {
          light.active = true;

          var scaleMap = [0.25, 0.25, 0.5, 1, 2];
          var fixPos = [0, 0, 30, 70, 160, 310];
          var _scale = scaleMap[big];
          light.scaleX = _scale;
          light.scaleY = _scale;
          light.x = fixPos[big];
          //light.y = fixPos[big];
          //console.log("scale:", [ _scale ]);
        } else {
          light.scaleX = 0.25;
          light.scaleY = 0.25;
          light.active = false;
        }
      }
    }

    if (big > 1) {
      size.width *= big;
      size.height *= big;
      sym.Expansion = new cc.Size(big, big);
    } else {
      sym.Expansion = new cc.Size(1, 1);
    }
    // this.close_big_symbol(sym);
    if (big > 1) {
      this.change_symbol_size(sym_node, size, big);
    }

    if (big > 1) {
      var idx = big - 2;
      // 判斷 MG FG 狀態
      var _allReelEffect = this.AllReelEffect;
      if (nowState === "FG") {
        _allReelEffect = this.FreeReelEffect;
      }
      if (_allReelEffect[reel_id][idx].active != true) {
        _allReelEffect[reel_id][idx].active = true;
        cc.Sound.PlaySound(this.ReelEffectSound);
      }
    }
  },

  // close_big_symbol(symbol_comp) {
  //   if (symbol_comp == null) return;
  //   if (symbol_comp.BigSymbolAni == null) return;
  //   if (symbol_comp.BigSymbolAni.length == 0) return;

  //   for (var idx in symbol_comp.BigSymbolAni) {
  //     symbol_comp.BigSymbolAni[idx].active = false;
  //   }
  // },

  /** 調整大小 */
  change_symbol_size(sym_node, size, big) {
    var fixX = 0;
    var fixY = 0;
    if (big == null) big = 0;
    if (cc.machine.status.bonus_gaming == true) {
      fixX = this.FREE_BIG_FIX_X[big];
      fixY = this.FREE_BIG_FIX_Y[big];
      // fixX = 0;
      // fixY = 0;
    } else {
      fixX = this.MAIN_BIG_FIX_X[big];
      fixY = this.MAIN_BIG_FIX_Y[big];
    }
    var symbol_comp = sym_node.getComponent("symbol");
    // if ( big == 0 ) symbol_comp.change_big_symbol(0);
    symbol_comp.show_normal();

    symbol_comp.BigSymbolAni[0].active = true;

    symbol_comp.AllPaylineNode.children[0].width = size.width;
    symbol_comp.AllPaylineNode.children[0].height = size.height;
    symbol_comp.AllPaylineNode.children[0].x = fixX;
    // symbol_comp.AllPaylineNode.children[0].y = fixY;

    symbol_comp.RollingNode.children[0].width = size.width;
    symbol_comp.RollingNode.children[0].height = size.height;
    symbol_comp.RollingNode.children[0].x = fixX;
    // symbol_comp.RollingNode.children[0].y = fixY;

    let light = symbol_comp.node.getChildByName("FG_CASE_X5_ske");
    if (light) {
      var scaleMap = [0.25, 0.25, 0.5, 1, 2];
      var fixPos = [0, 0, 30, 70, 160, 310];
      light.scaleX = scaleMap[big];
      light.scaleY = scaleMap[big];
      light.x = fixPos[big];
    }

    if (symbol_comp.ReelStopNode !== null) {
      symbol_comp.ReelStopNode.children[0].width = size.width;
      symbol_comp.ReelStopNode.children[0].height = size.height;
    }

    if (symbol_comp.PaylineNode !== null) {
      symbol_comp.PaylineNode.children[0].width = size.width;
      symbol_comp.PaylineNode.children[0].height = size.height;
    }
  },

  spin_response(result) {
    // console.log("spin_response", result);

    //變大震動
    for (
      let i = 0;
      i < result["result"]["all_round_data"]["0"]["change"].length;
      i++
    ) {
      if (result["result"]["all_round_data"]["0"]["change"][i] > 1) {
        this.setMgShakeAnime("shake");
        break;
      }
    }

    // 如果有中FG要預告動畫
    // if (result.result.type == "bonus") {
    //   this.setMgShakeAnime("shake");
    // }
    this.change_reel(0);
  },

  /**
   *
   * @param {*} reel_idx
   */
  change_reel(reel_idx) {
    var data = cc.machine.status.round_data.result.all_round_data[reel_idx];
    var big_reel = data.change;

    if (big_reel == null) return;

    var idx = 0;
    var closeReel = {};
    for (var i = 0; i < big_reel.length; i++) {
      if (big_reel[i] == 1) {
        idx++;
        continue;
      }
      for (var j = 0; j < big_reel[i]; j++) {
        if (j == 0) {
          cc.machine.reel_frame.Reels[idx].big = big_reel[i];
        } else {
          closeReel[idx] = cc.machine.reel_frame.reel[idx];
          cc.machine.reel_frame.Reels[idx].opacity = 128;
        }
        idx++;
      }
    }

    this.scheduleOnce(function() {
      var keys = Object.keys(closeReel);
      for (var idx in keys) {
        var id = keys[idx];
        var reel = closeReel[id];
        // console.log(reel);
        cc.machine.reel_frame.one_reel_done(id);
        // reel.node.getComponent(cc.Animation).resume();
        reel.node.active = false;
        reel.node.opacity = 255;
      }
    }, 1);
  },

  /**
   *目前盤面
   * @param {*} reel_frame 目前盤面
   */
  change_symbol_back(reel_frame, isScEffect) {
    let nodeReels = cc.machine.reel_frame.reel;

    for (let i = 0; i < nodeReels.length; i++) {
      let singleReel = nodeReels[i];
      /** 復原 */
      singleReel.node.big = 1;
      singleReel.node.active = true;
      singleReel.node.opacity = 255;
      singleReel.spin_stop();

      if (cc.machine.status.bonus_gaming === false) {
        nodeReels[i].put_reel_symbol(reel_frame[i]);
      } else {
        let fgSymbol = cc.machine.status.round_data.result.fg_symbol;

        /** 根據資料直接設定整行Symbol */
        for (let j = 0; j < reel_frame[i].length; j++) {
          var symNum = 100 + Number.parseInt(reel_frame[i][j]);
          var node = nodeReels[i].set_symbol(j, symNum);
          node.width = this.FreeGameSymbolSize.width;
          node.height = this.FreeGameSymbolSize.height;

          if (isScEffect) {
            if (reel_frame[i][j] === fgSymbol) {
              node.getChildByName("FG_CASE_X5_ske").active = true;
              node.getChildByName("FG_CASE_X5_ske").scaleX = 0.25;
              node.getChildByName("FG_CASE_X5_ske").scaleY = 0.25;
            } else {
              node.getChildByName("FG_CASE_X5_ske").active = false;
            }
          } else {
            node.getChildByName("FG_CASE_X5_ske").active = false;
          }
        }
      }
    }
  },

  all_reel_done_break() {
    // 如果有中 bouns 旁邊物件要跳一下
    if (cc.machine.check_bonus_game(false) == true) {
      this.setMgShakeAnime("jump", 1);
    } else {
      this.setMgShakeAnime("idle", 1);
    }

    if (cc.machine.status.round_data.result.total_score == 0) return;
    this.close_symbol_change_effect();

    var reel_frame = cc.machine.reel_frame.status.reel_data;
    this.change_symbol_back(reel_frame, true);
  },

  begin_spin() {
    if (cc.machine.status.round_data == null) return;
    var reel_frame =
      cc.machine.status.round_data.result.all_round_data[0].reel_frame;
    this.change_symbol_back(reel_frame, false);
    this.close_symbol_change_effect();
    cc.Sound.PlaySound(this.ClickSound);
  },

  one_reel_done(reel_id) {
    var big = cc.machine.reel_frame.Reels[reel_id].big;
    if (big == 1) return;

    var reel = cc.machine.reel_frame.reel[reel_id];
    var symbols = reel.symbol;

    var checkBig = big - 1;
    for (var i = 0; i < reel.status.last_idx; i++) {
      var sym = symbols[i];
      if (checkBig != 0) {
        checkBig--;
        sym.active = false;
        continue;
      }
      checkBig = big - 1;
      sym.active = true;
    }

    for (var idx in symbols) {
      if (symbols[idx].active == false) continue;

      var fixX = 0;
      var fixY = 0;

      if (cc.machine.status.bonus_gaming == true) {
        fixX = this.FREE_BIG_FIX_X[big];
        fixY = this.FREE_BIG_FIX_Y[big];
      } else {
        fixX = this.MAIN_BIG_FIX_X[big];
        fixY = this.MAIN_BIG_FIX_Y[big];
      }

      var symbol_comp = symbols[idx].sym;
      symbol_comp.change_big_symbol(big - 1);
    }
  },

  setSymbolBgDisable() {
    cc.log("setSymbolBgDisable");

    for (let i = 0; i < cc.machine.reel_frame.reel.length; i++) {
      let symbols = cc.machine.reel_frame.reel[i].symbol;
      for (let j = 0; symbols.length; j++) {
        let symbol = symbols[j];
        if (symbol.active == false) continue;

        var symbol_comp = symbol.sym;
        var light = symbol_comp.node.getChildByName("FG_CASE_X5_ske");
        if (light != null) {
          light.x = 0;
          light.y = 0;
          light.scaleX = 0.25;
          light.scaleY = 0.25;
          light.active = false;
        }
      }
    }
  },

  // 設定MG SHAKE動畫
  setMgShakeAnime(name, times = -1) {
    var bones = this.ReelShakeAnime.getComponent(dragonBones.ArmatureDisplay);
    bones.playAnimation(name, times);
  },
});
