var simulation = require("../../../../public/module/slot/machine/scripts/simulation_server");
cc.Class({
  extends: simulation,

  init() {
    // 滾輪區大小 3x5
    this.ReelSize = [4, 4, 4, 4, 4, 4];
    this.FreeReelSize = [8, 8, 8, 8, 8, 8];

    this.ChangeReelRate = {
      //ReelCombinationTest : { rate: 130, change: null, combination : 'ReelCombination', },
      ReelCombination: {
        rate: 1000,
        change: [2, 1, 1, 1, 1],
        combination: "ReelCombination",
      },

      // 2x2 normal
      ReelCombination1: {
        rate: 3,
        change: [1, 1, 1, 1, 2],
        combination: "ReelCombination1",
      },
      ReelCombination2: {
        rate: 3,
        change: [1, 1, 1, 2, 1],
        combination: "ReelCombination2",
      },
      ReelCombination3: {
        rate: 3,
        change: [1, 1, 2, 1, 1],
        combination: "ReelCombination1",
      },
      ReelCombination4: {
        rate: 3,
        change: [1, 2, 1, 1, 1],
        combination: "ReelCombination1",
      },
      ReelCombination5: {
        rate: 3,
        change: [2, 1, 1, 1, 1],
        combination: "ReelCombination1",
      },
      ReelCombination6: {
        rate: 3,
        change: [1, 1, 2, 2],
        combination: "ReelCombination1",
      },
      ReelCombination7: {
        rate: 3,
        change: [1, 2, 1, 2],
        combination: "ReelCombination1",
      },
      ReelCombination8: {
        rate: 3,
        change: [2, 1, 1, 2],
        combination: "ReelCombination1",
      },
      ReelCombination9: {
        rate: 3,
        change: [1, 2, 2, 1],
        combination: "ReelCombination1",
      },
      ReelCombination10: {
        rate: 3,
        change: [2, 1, 2, 1],
        combination: "ReelCombination1",
      },
      ReelCombination11: {
        rate: 3,
        change: [2, 2, 1, 1],
        combination: "ReelCombination1",
      },
      ReelCombination12: {
        rate: 3,
        change: [2, 2, 2],
        combination: "ReelCombination1",
      },

      // 3x3
      ReelCombination101: {
        rate: 10,
        change: [1, 2, 3],
        combination: "ReelCombination1",
      },
      ReelCombination102: {
        rate: 3,
        change: [2, 1, 3],
        combination: "ReelCombination1",
      },
      ReelCombination103: {
        rate: 3,
        change: [1, 1, 1, 3],
        combination: "ReelCombination1",
      },
      ReelCombination104: {
        rate: 13,
        change: [1, 1, 3, 1],
        combination: "ReelCombination1",
      },
      ReelCombination105: {
        rate: 3,
        change: [2, 3, 1],
        combination: "ReelCombination1",
      },
      ReelCombination106: {
        rate: 3,
        change: [1, 3, 1, 1],
        combination: "ReelCombination1",
      },
      ReelCombination107: {
        rate: 3,
        change: [1, 3, 2],
        combination: "ReelCombination1",
      },
      ReelCombination108: {
        rate: 3,
        change: [3, 2, 1],
        combination: "ReelCombination1",
      },
      ReelCombination109: {
        rate: 3,
        change: [3, 1, 2],
        combination: "ReelCombination1",
      },
      ReelCombination110: {
        rate: 3,
        change: [3, 1, 1, 1],
        combination: "ReelCombination1",
      },
      ReelCombination111: {
        rate: 3,
        change: [3, 3],
        combination: "ReelCombination1",
      },

      // 4x4
      ReelCombination201: {
        rate: 3,
        change: [1, 1, 4],
        combination: "ReelCombination1",
      },
      ReelCombination202: {
        rate: 3,
        change: [2, 4],
        combination: "ReelCombination1",
      },
      ReelCombination203: {
        rate: 3,
        change: [1, 4, 1],
        combination: "ReelCombination1",
      },
      ReelCombination204: {
        rate: 3,
        change: [4, 1, 1],
        combination: "ReelCombination1",
      },
      ReelCombination205: {
        rate: 3,
        change: [4, 2],
        combination: "ReelCombination1",
      },

      // 5x5
      ReelCombination301: {
        rate: 3,
        change: [1, 5],
        combination: "ReelCombination1",
      },
      ReelCombination301: {
        rate: 3,
        change: [5, 1],
        combination: "ReelCombination1",
      },

      // 6x6
      ReelCombination301: {
        rate: 3,
        change: [6],
        combination: "ReelCombination1",
      },

      // Free Game1x1
      FreeReelCombination: {
        rate: 0,
        change: [1, 1, 1, 1, 2],
        combination: "FreeReelCombination",
      },
    };

    // 排列帶
    this.ReelCombination = {
      0: [0, 2, 3, 4, 0, 5, 6, 0],
      1: [1, 2, 3, 4, 5, 6, 7, 8, 1, 8, 1],
      2: [1, 2, 3, 1, 2, 6, 4, 5, 6, 7, 2],
      3: [1, 9, 3, 9, 3, 9, 6, 7, 9, 1, 3],
      // 4 : [1,9,4,3,3,3,3,3,3,3,5],
      4: [1, 9, 4, 9, 9, 9, 9, 9, 9, 9, 5],
      5: [1, 9, 9, 9, 9, 9, 9, 9, 7, 9, 8],
      // 5 : [1,9,9,3,3,3,3,3,3,3,8],
    };

    this.FreeReelCombination = {
      0: [0, 1, 2, 3, 4, 5, 6, 7],
      1: [0, 1, 2, 3, 4, 5, 6, 7, 8],
      2: [0, 1, 2, 3, 4, 5, 6, 7, 8],
      3: [0, 1, 2, 3, 4, 5, 6, 7, 8],
      4: [0, 1, 2, 3, 4, 5, 6, 7, 8],
      5: [0, 1, 2, 3, 4, 5, 6, 7, 8],
    };

    this.ReelCombination1 = {
      //
      0: [0, 1, 2, 3, 4, 5, 6, 7],
      1: [0, 1, 2, 3, 4, 5, 6, 7, 8],
      2: [0, 1, 2, 3, 4, 5, 6, 7, 8],
      3: [0, 1, 2, 3, 4, 5, 6, 7, 8],
      4: [0, 1, 2, 3, 4, 5, 6, 7, 8],
      5: [0, 1, 2, 3, 4, 5, 6, 7, 8],
    };

    this.ReelCombination2 = {
      //
      0: [0, 1, 2, 3, 4, 5, 6, 7],
      1: [0, 1, 2, 3, 4, 5, 6, 7, 8],
      2: [0, 0, 0, 0, 0, 0, 0, 0, 0, 0],
      3: [0, 0, 0, 0, 0, 0, 0, 0, 0],
      4: [0, 0, 0, 0, 0, 0, 0, 0, 0],
      5: [0, 0, 0, 0, 0, 0, 0, 0, 0],
    };

    this.symbol = {
      wild: 8, // 百搭編號

      0: {
        name: "M50", // 名稱, 只參考用
        score: [0, 0, 50, 100, 500], // 分數配置
      },

      1: {
        name: "M51",
        score: [0, 0, 40, 80, 100],
      },

      2: {
        name: "M52",
        score: [0, 0, 30, 60, 80],
      },

      3: {
        name: "M53",
        score: [0, 0, 20, 40, 60],
      },

      4: {
        name: "M54",
        score: [0, 0, 10, 30, 50],
      },

      5: {
        name: "M55",
        score: [0, 0, 8, 20, 40],
      },

      6: {
        name: "M56",
        score: [0, 0, 6, 8, 20],
      },

      7: {
        name: "M57",
        score: [0, 0, 3, 6, 9],
      },

      8: {
        name: "Wild",
        score: null,
      },

      9: {
        name: "FS",
        score: null,
        type: "free_game",
        round: [
          0,
          0,
          0,
          0,
          0,
          5,
          5,
          5,
          5,
          5,
          5,
          5,
          5,
          5,
          5,
          15,
          16,
          17,
          18,
          19,
          20,
          21,
          22,
          23,
          24,
          25,
          26,
          27,
          28,
          29,
          30,
          31,
          32,
          33,
          34,
          35,
          36,
          37,
          38,
          39,
          40,
        ], // free game 局數
      },
    };

    this.ReelLine = [
      // 40 線的配置
      [0, 0, 0, 0, 0, 0],
      [1, 1, 1, 1, 1, 1],
      [2, 2, 2, 2, 2, 2],
      [3, 3, 3, 3, 3, 3],
      [1, 2, 3, 3, 2, 1],
      [2, 1, 0, 0, 1, 2],
      [0, 1, 2, 2, 1, 0],
      [3, 2, 1, 1, 2, 3],
      [1, 0, 0, 0, 0, 1],
      [2, 3, 3, 3, 3, 2],
      [0, 1, 1, 1, 1, 0],
      [3, 2, 2, 2, 2, 3],
      [1, 2, 2, 2, 2, 1],
      [2, 1, 1, 1, 1, 2],
      [1, 0, 1, 2, 3, 2],
      [2, 3, 2, 1, 0, 1],
      [0, 0, 1, 2, 3, 3],
      [3, 3, 2, 1, 0, 0],
      [0, 1, 0, 1, 0, 1],
      [3, 2, 3, 2, 3, 2],
      [1, 2, 3, 2, 1, 0],
      [2, 1, 0, 1, 2, 3],
      [0, 1, 2, 3, 2, 1],
      [3, 2, 1, 0, 1, 2],
      [1, 0, 0, 1, 2, 3],
      [2, 3, 3, 2, 1, 0],
      [0, 1, 1, 2, 2, 3],
      [3, 2, 2, 1, 1, 0],
      [0, 1, 2, 3, 2, 2],
      [3, 2, 1, 0, 1, 1],
      [1, 2, 3, 2, 1, 1],
      [2, 1, 0, 1, 2, 2],
      [1, 0, 1, 0, 1, 0],
      [2, 3, 2, 3, 2, 3],
      [1, 1, 2, 3, 2, 1],
      [2, 2, 1, 0, 1, 2],
      [0, 1, 0, 1, 2, 3],
      [3, 2, 3, 2, 1, 0],
      [1, 2, 1, 0, 1, 0],
      [2, 1, 2, 3, 2, 3],
    ];

    this.FGReelLine = [
      [0, 0, 0, 0, 0, 0],
      [1, 1, 1, 1, 1, 1],
      [2, 2, 2, 2, 2, 2],
      [3, 3, 3, 3, 3, 3],
      [1, 2, 3, 3, 2, 1],
      [2, 1, 0, 0, 1, 2],
      [0, 1, 2, 2, 1, 0],
      [3, 2, 1, 1, 2, 3],
      [1, 0, 0, 0, 0, 1],
      [2, 3, 3, 3, 3, 2],
      [0, 1, 1, 1, 1, 0],
      [3, 2, 2, 2, 2, 3],
      [1, 2, 2, 2, 2, 1],
      [2, 1, 1, 1, 1, 2],
      [1, 0, 1, 2, 3, 2],
      [2, 3, 2, 1, 0, 1],
      [0, 0, 1, 2, 3, 3],
      [3, 3, 2, 1, 0, 0],
      [0, 1, 0, 1, 0, 1],
      [3, 2, 3, 2, 3, 2],
      [1, 2, 3, 2, 1, 0],
      [2, 1, 0, 1, 2, 3],
      [0, 1, 2, 3, 2, 1],
      [3, 2, 1, 0, 1, 2],
      [1, 0, 0, 1, 2, 3],
      [2, 3, 3, 2, 1, 0],
      [0, 1, 1, 2, 2, 3],
      [3, 2, 2, 1, 1, 0],

      [0, 1, 2, 3, 2, 2],
      [3, 2, 1, 0, 1, 1],
      [1, 2, 3, 2, 1, 1],
      [2, 1, 0, 1, 2, 2],
      [1, 0, 1, 0, 1, 0],
      [2, 3, 2, 3, 2, 3],
      [1, 1, 2, 3, 2, 1],
      [2, 2, 1, 0, 1, 2],
      [0, 1, 0, 1, 2, 3],
      [3, 2, 3, 2, 1, 0],
      [1, 2, 1, 0, 1, 0],
      [2, 1, 2, 3, 2, 3],
      [4, 4, 4, 4, 4, 4],
      [5, 5, 5, 5, 5, 5],
      [6, 6, 6, 6, 6, 6],
      [7, 7, 7, 7, 7, 7],
      [5, 6, 7, 7, 6, 5],
      [6, 5, 4, 4, 5, 6],
      [4, 5, 6, 6, 5, 4],
      [7, 6, 5, 5, 6, 7],
      [5, 4, 4, 4, 4, 5],
      [6, 7, 7, 7, 7, 6],
      [4, 5, 5, 5, 5, 4],
      [7, 6, 6, 6, 6, 7],
      [5, 6, 6, 6, 6, 5],
      [6, 5, 5, 5, 5, 6],
      [5, 4, 5, 6, 7, 6],
      [6, 7, 6, 5, 4, 5],

      [4, 4, 5, 6, 7, 7],
      [7, 7, 6, 5, 4, 4],
      [4, 5, 4, 5, 4, 5],
      [7, 6, 7, 6, 7, 6],
      [5, 6, 7, 6, 5, 4],
      [6, 5, 4, 5, 6, 7],
      [4, 5, 6, 7, 6, 5],
      [7, 6, 5, 4, 5, 6],
      [5, 4, 4, 5, 6, 7],
      [6, 7, 7, 6, 5, 4],
      [4, 5, 5, 6, 6, 7],
      [7, 6, 6, 5, 5, 4],
      [4, 5, 6, 7, 6, 6],
      [7, 6, 5, 4, 5, 5],
      [5, 6, 7, 6, 5, 5],
      [6, 5, 4, 5, 6, 6],
      [5, 4, 5, 4, 5, 4],
      [6, 7, 6, 7, 6, 7],
      [5, 5, 6, 7, 6, 5],
      [6, 6, 5, 4, 5, 6],
      [4, 5, 4, 5, 6, 7],
      [7, 6, 7, 6, 5, 4],
      [5, 6, 5, 4, 5, 4],
      [6, 5, 6, 7, 6, 7],
      [3, 4, 3, 4, 3, 4],
      [3, 3, 4, 4, 3, 3],
      [3, 4, 5, 5, 4, 3],
      [7, 6, 5, 4, 5, 6],
      [3, 4, 4, 4, 4, 3],
    ];
  },

  spin(total_bet) {
    // 先製作一般spin
    var result = this.make_normal_spin();

    // 檢查是否有中BonusGame
    result = this.check_bonus_game(result);
    console.log("spin", [
      this.Balance,
      total_bet,
      result.result["total_score"],
    ]);
    this.Balance =
      Math.floor(
        this.Balance * 1000 -
          total_bet * 1000 +
          result.result["total_score"] * 1000
      ) / 1000;

    result.result.last_credit = this.Balance;

    var data = { data: JSON.stringify(result) };

    cc.machine.receive_package("spin_response", data);
  },

  change_big_symbol(
    reel_frame,
    new_frame,
    reel_idx,
    symbol_size,
    idx,
    one_reel_max
  ) {
    var count = 0;
    for (var i = 0; i < one_reel_max; i) {
      var symbol = reel_frame[idx][count];
      count++;
      var sizeX;
      var amount = 0;
      if (i == 0) sizeX = Math.floor(Math.random() * symbol_size);
      else sizeX = 0;
      sizeX = 0;
      for (sizeX; sizeX < symbol_size; sizeX++) {
        amount++;
        for (var sizeY = 0; sizeY < symbol_size; sizeY++) {
          var x = reel_idx - sizeX;
          var y = i + sizeY;
          new_frame[x][y] = symbol;
          // console.log('change_big_symbol for', [ i, x, y,symbol ]);
        }
      }
      i += amount;
    }

    return new_frame;
  },

  change_reel_frame(reel_frame, reel_type) {
    if (reel_type == "ReelCombination") {
      return reel_frame;
    }

    var change = this.ChangeReelRate[reel_type].change;
    var max = 4;
    if (this.freeGameMode == true) max = 8;
    var idx = -1;

    var new_frame = new Array(6);
    for (var i = 0; i < 6; i++) new_frame[i] = new Array(max);

    for (var i = 0; i < change.length; i++) {
      var symbol_size = change[i];
      idx += symbol_size;
      new_frame = this.change_big_symbol(
        reel_frame,
        new_frame,
        idx,
        symbol_size,
        i,
        max
      );
    }

    return new_frame;
  },

  break_make_spin(reelCombination = "ReelCombination") {
    var _ran = Math.floor(Math.random() * 100);

    var reel_type = "ReelCombination";
    var reel_frame;
    var key;
    //if ( this.freeGameMode != true ) {
    var keys = Object.keys(this.ChangeReelRate);

    for (var idx in keys) {
      _ran -= this.ChangeReelRate[keys[idx]].rate;
      if (_ran > 0) continue;
      key = keys[idx];
      reel_type = this.ChangeReelRate[key].combination;
      break;
    }

    if (this.freeGameMode == true && reel_type == "ReelCombination") {
      reel_type = "FreeReelCombination";
      key = "FreeReelCombination";
    }

    reel_frame = this.make_reel_fame(reel_type);
    reel_frame = this.change_reel_frame(reel_frame, key);

    /*
        } else {
            reel_type = "ReelCombinationFreeGame";
            key = "FreeReelCombination";
            reel_frame = this.make_reel_fame(reel_type);
        }*/

    var symbol_data = this.reckon_symbol_data(reel_frame);
    var payline_data;
    if (this.freeGameMode != true) {
      payline_data = this.reckon_payline_data(symbol_data);
    } else {
      payline_data = this.reckon_payline_data(symbol_data, "FGReelLine");

      var keys = Object.keys(payline_data);
      if (keys.length > 0)
        for (var i in keys) {
          var line = keys[i];
          var one_payline_data = payline_data[line];
          if (one_payline_data["symbol"] != this.FG_Symbol) continue;
          // console.log("got FG Doubole score", [one_payline_data]);
          payline_data[line]["score"] = payline_data[line]["score"] * 5;
        }
    }

    var total_score = this.reckon_total_score(payline_data);
    var spin_result = {
      change_reel: key,
      payline: payline_data,
      reel_frame: reel_frame,
      symbol: symbol_data,
      total_score: total_score,
      // "total_score" : 0,
      change: this.ChangeReelRate[key].change,
    };

    var tmp = this.event_make_spin(spin_result);
    if (tmp === null) return spin_result;
    return tmp;
  },

  event_make_spin(spin_result) {
    return null;
  },

  free_game(sym, symbol_data, result) {
    this.freeGameMode = true;

    var count = 0;
    for (var i = 0; i < symbol_data["count"].length; i++) {
      count += symbol_data["count"][i];
    }

    var free_spin = this.symbol[sym]["round"][count];
    if (free_spin == 0) return null;
    this.FG_Symbol = Math.floor(Math.random() * 8);
    // console.log("fg symbol:"+this.FG_Symbol);

    var total_score = result["result"]["total_score"] * 1000;
    for (var i = 1; i < free_spin + 1; i++) {
      // result['result']['all_round_data'][i] = this.make_spin('ReelCombinationFreeGame');
      result["result"]["all_round_data"][i] = this.break_make_spin(
        "FreeReelCombination"
      );
      var score = result["result"]["all_round_data"][i]["total_score"];
      total_score += score * 1000;
    }

    total_score = total_score / 1000;

    result["result"]["all_round_data"][0]["bonus_count"] = free_spin;
    result["result"]["all_round_data"][0]["bonus_symbol"] = sym;
    result["result"]["all_round_data"][0]["type"] = "enter_bonus";
    result["result"]["type"] = "bonus";
    result["result"]["round_count"] = free_spin + 1;
    result["result"]["total_score"] = total_score;
    result["result"]["fg_symbol"] = this.FG_Symbol;
    this.freeGameMode = false;
    this.FG_Symbol = null;
    return result;
  },
});
