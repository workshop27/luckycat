cc.Class({
    extends: cc.Component,

    properties: {
        Progress : {
            type: cc.ProgressBar,
            default:null,
        }
    },

    start () {
        cc.loading = this;
        console.log("start", this.Progress);
        cc.director.preloadScene("DNTK", this.onProgress, this.enterGame);
    },

    enterGame() {
        cc.loading.scheduleOnce(function(){
            cc.director.loadScene("DNTK");
        },1);
    },

    onProgress(completeCount, totalCount, item) {
        var process = (completeCount/totalCount);
        cc.loading.Progress.progress = process;
    }

});
