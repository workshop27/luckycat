var simulation = require("../../../../public/module/slot/machine/scripts/simulation_server");
cc.Class({
    extends: simulation,

    init() {
        // 滾輪區大小 3x5
        this.ReelSize = [3,3,3,3,3,5];

        // 排列帶
        
        this.ReelCombination = {
        //  0 : [2,3,4,5,6,7,9,9,9,9,9,9,],
            0 : [2,3,4,5,6,7],
            1 : [8,9,0,4,5,6,7,1,8,9,4,5,6,7,8,9,2,8,9,3,0,1,4,5,6,7,8,4,5,6,7],
            2 : [8,9,0,4,5,6,7,1,8,9,4,5,6,7,8,9,2,3,4,5,6,7,8,9,4,5,6,7],
            3 : [8,9,0,4,5,6,7,1,8,9,4,5,6,7,8,9,2,3,4,5,6,7,8,9,4,5,6,7],
            4 : [8,9,0,4,5,6,7,1,8,9,4,5,6,7,8,9,2,3,4,5,6,7,8,9,4,5,6,7],
            5 : [11,12,13,11,12,13,11,12,13,11,12,13,],
        };

        /*
        this.ReelCombination = {
            0 : [2,3,4,5,6,7,],
            1 : [0,4,9,5,6,7,1,4,5,6,7,2,9,3,0,1,4,9,5,6,7,8,4,5,6,7,9],
            2 : [0,4,9,5,6,7,1,4,5,6,7,2,3,9,4,5,6,7,8,4,9,5,6,7,9],
            3 : [0,4,9,5,6,7,1,4,5,6,7,2,9,3,4,5,6,7,8,4,9,5,6,7,9],
            4 : [0,4,9,5,6,7,1,4,5,6,7,2,9,3,4,5,6,7,8,4,9,5,6,7,9],
            5 : [11,12,13,11,12,13,11,12,13,11,12,13,],
        };*/

        /*
        this.ReelCombination = {
            0 : [1,2,3],
            1 : [8,4,4,9],
            2 : [8,4,4,9],
            3 : [8,4,4,9],
            4 : [8,4,4,9],

            // top Reel
            5 : [11,12,13,11,12,13,11,12,13,11,12,13,],
        };*/

        // FreeGame 
        this.ReelCombinationFreeGame = {
            0 : [2,3,4,5,6,7,],
            1 : [2,3,4,0,2,3,4,1,5,6,7,0,2,3,4,1,6,7,2,3,4,5,6,7,2,3,4,5,6,7,2,3,2,3,],
            2 : [1,6,7,0,2,3,1,4,5,6,7,10,2,3,4,5,6,7,0,2,3,10,2,3,4,5,6,7,0,3,4,5,6,7,4,5,4,5,],
            3 : [1,6,7,0,2,3,1,4,5,6,7,10,2,3,4,5,6,7,0,2,3,10,2,3,4,5,6,7,2,3,4,5,6,7,2,3,2,3,],
            4 : [1,6,7,0,2,3,1,4,5,6,7,10,2,3,4,5,6,7,0,2,3,10,2,3,4,5,6,7,2,3,4,5,6,7,],

            5 : [11,12,13,11,12,13,11,12,13,11,12,13,],
        };


        this.symbol = { 
            wild : 8, // 百搭編號

            0 : {
                name : "二郎神",            // 名稱, 只參考用 
                score : [0,0,50,200,1000,], // 分數配置
            },

            1 : {
                name : "太上老君", 
                score : [0,0,40,140,600,],
            },

            2 : {
                name : "哮天犬", 
                score : [0,0,40,110,400,],
            },

            3 : {
                name : "葫蘆", 
                score : [0,0,30,90,240,],
            },

            4 : {
                name : "A", 
                score : [0,0,20,70,130,],
            },

            5 : {
                name : "K", 
                score : [0,0,20,60,110,],
            },

            6 : {
                name : "Q", 
                score : [0,0,20,50,90,],
            },

            7 : {
                name : "J", 
                score : [0,0,20,40,70,],
            },

            8 : {
                name : "Wild", 
                score : null,
            },

            9 : {
                name: "FS",
                score : null,
                type: "free_game",
                round: [0,0,0,5,8,10], // free game 局數
            },

            10: {
                name : "蟠桃",
                score: null,
                type: "bonus_game",
            },

            11: {
                name : "全開",
                score: null,
            },

            12: {
                name : "全閉",
                score: null,
            },

            13: {
                name : "半開",
                score: null,
                score_plus : {
                    1: 50,
                    2: 30,
                    3: 20,
                },

                free_score_plus:{
                    2: 50,
                    3: 30,
                    4: 20,
                },
            },

        };

        this.ReelLine = [ // 20 線的配置
            [1,1,1,1,1], [0,0,0,0,0], [2,2,2,2,2], [0,1,2,1,0], [2,1,0,1,2], 
            [0,1,1,1,0], [2,1,1,1,2], [1,0,0,0,1], [1,2,2,2,1], [1,1,0,1,1], 
            [1,1,2,1,1], [0,0,1,0,0], [2,2,1,2,2], [0,1,0,1,0], [2,1,2,1,2],
            [1,0,1,0,1], [1,2,1,2,1], [0,2,2,2,0], [2,0,0,0,2], [2,2,0,2,2]
        ];
    },


    check_pink(round_data) {
        
        if ( round_data == null ) return 0;
        if ( round_data['symbol'] == null ) return 0;
        if ( round_data['symbol'][10] == null ) return 0;
        if ( round_data['symbol'][10]['count'] == null ) return 0;

        var count = 0;
        for(var x=0;x<round_data['reel_frame'].length;x++) {
            for(var y=0;y<round_data['reel_frame'][x].length;y++) {
                if ( round_data['reel_frame'][x][y] == 10 ) count++;
            }
        }
        return count;
    },

    free_game(sym, symbol_data, result) {
        
        var count = 0;
        for(var i=0;i<symbol_data['count'].length;i++) {
            count += symbol_data['count'][i];
        }

        var free_spin = this.symbol[sym]['round'][count];
        var origin_free_spin = free_spin;

        if ( free_spin == 0 ) return null;
        
        var total_score = result['result']['total_score'] * 1000;

        this.freeGame = true;
        for(var i=1;i<free_spin+1;i++) {
            result['result']['all_round_data'][i] = this.make_spin('ReelCombinationFreeGame');
            var reel_frame = result['result']['all_round_data'][i]['reel_frame'];
            
            var score = result['result']['all_round_data'][i]['total_score'];
            total_score += (score * 1000);

            var pink_count = this.check_pink(result['result']['all_round_data'][i]);
            if ( pink_count > 0 ) {
                result['result']['all_round_data'][i]['pink_count'] = pink_count;
                free_spin += pink_count;
            }
        }
        this.freeGame = false;

        total_score = total_score / 1000;

        result['result']['all_round_data'][0]['bonus_count'] = free_spin;
        result['result']['all_round_data'][0]['bonus_symbol'] = sym;
        result['result']['all_round_data'][0]['type'] = 'enter_bonus';
        result['result']['type'] = 'bonus';
        result['result']['round_count'] = origin_free_spin;
        result['result']['total_score'] = total_score;

        return result;
    },

    /**
     * 變換圖標
     * @param {*} reel_frame 
     * @param {*} from_sym 
     * @param {*} to_sym 
     */
    change_symbol(spin_result) {
       var reel_frame = spin_result['reel_frame'];
       var new_frame = new Array(reel_frame.length);
       var reel_wild = [ false,false,false,false,false ];
        for(let x=0;x<reel_frame.length;x++) {
            new_frame[x] = new Array(reel_frame[x].length);
            for(let y=0;y<reel_frame[x].length;y++) {
                var sym = reel_frame[x][y];
                if ( sym == 0 || sym == 1 ) {
                    if ( this.freeGame == true ) {
                        new_frame[x] = [8,8,8];
                        reel_wild[x] = true;
                        break;
                    } else {
                        new_frame[x][y] = 8;
                    }
                } else {
                    new_frame[x][y] = sym;
                }
            }
        }

        spin_result['new_frame'] = new_frame;
        var symbol_data = this.reckon_symbol_data(new_frame);
        var payline_data = this.reckon_payline_data(symbol_data);
        var total_score = this.reckon_total_score(payline_data);
        var new_spin_result = {
            "reel_wild" : reel_wild,
            "payline" : payline_data,
            "reel_frame" : reel_frame,
            "symbol": symbol_data,
            "total_score" : total_score,
            "new_frame" : new_frame,
            "eye" : 11,
        };

        return new_spin_result;
    },

    free_game_change_symbol(spin_result) {
        var reel_frame = spin_result['reel_frame'];
        var new_frame = new Array(reel_frame.length);
        var have_change = false;
        var wildReel = [false, false, false, false, false];
        for(let x=0;x<reel_frame.length;x++) {
            new_frame[x] = new Array(reel_frame[x].length);
            for(let y=0;y<reel_frame[x].length;y++) {
                var sym = reel_frame[x][y];
                if ( sym == 0 || sym == 1 ) {
                    have_change = true;
                    new_frame[x] = [8,8,8];
                    wildReel[x] = true;
                    break;
                } else {
                    new_frame[x][y] = sym;
                }
            }
        }

        if ( have_change == false ) return null;

        spin_result['new_frame'] = new_frame;
        var symbol_data = this.reckon_symbol_data(new_frame);
        var payline_data = this.reckon_payline_data(symbol_data);
        var total_score = this.reckon_total_score(payline_data);
        var new_spin_result = {
            "payline" : payline_data,
            "reel_frame" : reel_frame,
            "symbol": symbol_data,
            "total_score" : total_score,
            "new_frame" : new_frame,
            "eye" : 11,
            "reel_wild" : wildReel,
        };

        return new_spin_result;
    },


    /**
     * 乘倍
     * @param {*} spin_result 
     */
    double_score(spin_result) {
        var ran = Math.floor(Math.random()*100);
        var double_rate = this.symbol[13]['score_plus'];
        if ( this.freeGame == true ) double_rate = this.symbol[13]['free_score_plus'];
        
        var double = Object.keys(double_rate);
        var value = 0;
        for(var idx in double) {
            value = parseInt(double[idx]);
            ran -= double_rate[value];
            if ( ran < 0) break;
        }

        spin_result['double'] = value;
        spin_result['total_score'] = spin_result['total_score'] * value;
        return spin_result;
    },

    event_make_spin(spin_result) { 
        spin_result['eye'] = spin_result['reel_frame'][5][2];
        // spin_result['eye'] = 11;
        switch(spin_result['eye']) {
            case 11:
                return this.change_symbol(spin_result);
            case 12: // 眼睛全閉
                return null;
            case 13: // 眼睛半開
                return this.double_score(spin_result);
        }
        return spin_result; 
    },

});
