cc.Class({
    extends: cc.Component,

    properties: {
        BeginFreeGameSound : {
            type: cc.AudioClip,
            default: null,
        },

        FreeGameBGSound : {
            type:cc.AudioClip,
            default:null,
        },

        FreeGameEndUISound: {
            type:cc.AudioClip,
            default:null,
        },

        BeginFreeGameUI : {
            type: cc.Node,
            default: null,
        },

        BeginFreeGameLabel : {
            type: cc.Label,
            default: null,
        },

        EndFreeGameUI : {
            type:cc.Node,
            default:null,
        },

        EndFreeGameTotalScore : {
            type:cc.Label,
            default:null,
        },

        EyeSound: {
            type: cc.AudioClip,
            default:null,
        },

        EyeMessage : {
            type: cc.Label,
            default:null,
        },

        FreeGameBackground : {
            type: cc.Node,
            default : null,
        },

        ChangeSymbolReelEffect : {
            type: [cc.Node],
            default: [],
        },

        EyeEffect11 : {
            type: cc.Node,
            default: null,
        },

        EyeEffectChangeEye : {
            type: cc.Node,
            default: null,
        },

        EyeEffect13 : {
            type: cc.Node,
            default: null,
        },

        EyeEffect13Label : {
            type: cc.Label,
            default: null,
        },

        SpinButtonIcon: {
            type: cc.Node,
            default: null,
        },

        DragonLightEffect: {
            type: cc.Node,
            default: null,
        },
    },

    onLoad() {
        this.BeginFreeGameUI.active = false;
    },

    start(){
        cc.lair.register_node("EhangeSymbolEffect", this.EyeEffectChangeEye);
        this.scheduleOnce(function(){
            cc.machine.slot_control_bar.SpinLabel.string = "";
            cc.machine.slot_control_bar.SpinLabel.active = true;
        },1);
        
    },


    begin_free_game_show_payline() {
        var round_data = cc.machine.status.round_data.result.all_round_data[0];
        var total_score = round_data['total_score'];
        if ( total_score == 0 ) return 0;
        cc.machine.payline.show_all_payline(round_data);
        cc.machine.payline.call_stop();
        var waiting = 3;
        this.scheduleOnce(function(){
            cc.machine.payline.show_end_payline(true); 
            cc.machine.payline.show_payline_done();
        },waiting);

        return waiting + 0.1;
    },

    begin_free_game_ui(symbol, param) {
        var waiting = this.begin_free_game_show_payline();
        this.scheduleOnce(function(){ 
            this.reset_reel();
            this.begin_free_game_show_symbol(); 
        },waiting); 
    },

    begin_free_game_show_symbol() {
        cc.machine.reel_frame.symbol_id_show_payline(9, false);
        this.scheduleOnce(function(){
            this.reset_reel();
            cc.machine.status.bonus_gaming = true;
            // this.BeginFreeGameLabel.string = param['bonus_count'];
            this.BeginFreeGameLabel.string = cc.machine.status.round_data.result.round_count;
            this.BeginFreeGameUI.active = true;
            cc.Sound.PlaySound(this.BeginFreeGameSound, false);
            this.scheduleOnce(function(){
                this.start_free_game();
            },3);
        },3);  
    },

    start_free_game() {
        cc.Sound.playMusic(this.FreeGameBGSound);
        this.FreeGameBackground.active = true;
        this.free_game_score = cc.machine.status.round_data.result.all_round_data[0].total_score;
        this.BeginFreeGameUI.active = false;
        this.FreeGameTimes = 5;
        this.SpinButtonIcon.active = false;
        this.DragonLightEffect.active = true;
        cc.machine.slot_control_bar.SpinStopNode.active = false;
        cc.machine.slot_control_bar.SpinLabel.string = "5";
        cc.machine.slot_control_bar.AutoStopNode.active = false;
        cc.machine.slot_control_bar.set_win_score_label(this.free_game_score);
        this.scheduleOnce(function(){
            cc.machine.slot_control_bar.SpinLabel.string = "5";
            this.spin_round(1);
        },2);
    },

    begin_end_free_game_ui() {
        if ( this.FreeGameEndUISound != null ) cc.Sound.PlaySound(this.FreeGameEndUISound, false);
        cc.machine.slot_control_bar.SpinLabel.string = "";
        this.EndFreeGameUI.active = true;
        this.EndFreeGameTotalScore.string = this.free_game_score;
        this.scheduleOnce(function(){this.close_end_free_game_ui()},3);
    },

    close_end_free_game_ui() {
        cc.Sound.stopMusic();
        cc.machine.slot_control_bar.SpinLabel.string = "";
        this.DragonLightEffect.active = false;
        this.SpinButtonIcon.active = true;
        this.BeginFreeGameUI.active = false;
        this.EndFreeGameUI.active = false;
        this.FreeGameBackground.active = false;
        cc.machine.status.bonus_gaming = false;
        cc.machine.status.bonus_done = false;
        //if ( cc.machine.check_big_win() == true ) cc.machine.run_big_win();
        cc.machine.back_to_normal();
    },

    check_free_game_end(idx) {
        this.reset_reel();
        console.log(this.free_game_score);
        cc.machine.slot_control_bar.set_win_score_label(this.free_game_score);
        this.reset_reel_wild();
        idx ++;
        this.EyeMessage.string = "";
        var result = cc.machine.status.round_data.result;
        if ( result['all_round_data'][idx] == null ) return this.begin_end_free_game_ui();
        return this.scheduleOnce(function(){ this.spin_round(idx) },1);
    },

    reset_reel() {
        cc.machine.mState.Payline 
        cc.machine.payline.call_stop();
        cc.machine.payline.show_end_payline(true);
        cc.machine.payline.show_payline_done();
        
        for(var idx in cc.machine.payline.lines) {
            cc.machine.payline.lines[idx].active = false;
        }
    },

    reset_reel_wild() {
        var index = this.SpinRound;
        var round_data = cc.machine.status.round_data.result.all_round_data[index];
        
        var reel_wild = round_data['reel_wild'];
        if ( reel_wild == null) return;
        for(var idx in reel_wild) {
            if ( reel_wild[idx] == false ) continue;
            this.reelActive(idx, true);
        }
        this.ChangeSymbolReelEffect[0].active = false;
        this.ChangeSymbolReelEffect[1].active = false;
        this.ChangeSymbolReelEffect[2].active = false;
        this.ChangeSymbolReelEffect[3].active = false;
        this.ChangeSymbolReelEffect[4].active = false;
        return; 
    },

    free_game_show_pink(round_data) {
        if ( round_data['pink_count'] == null ) return 0;
        this.reset_reel();
        this.FreeGameTimes += round_data['pink_count'];
        console.log("addpink",[this.FreeGameTimes, round_data['pink_count']]);
        cc.machine.slot_control_bar.SpinLabel.string = this.FreeGameTimes;
        cc.machine.reel_frame.symbol_id_show_payline(10, false);
        //this.scheduleOnce(function(){ this.reset_reel(); },wait);

        return 2;
    },

    // 執行Spin
    free_game_spin(idx) {
        this.reset_reel();
        var result = cc.machine.status.round_data.result;
        cc.machine.reel_frame.spin_response(result['all_round_data'][idx]['reel_frame']);
        
        this.scheduleOnce(function() {
            var round_data = result['all_round_data'][idx];
            var total_score = round_data['total_score'];
            var eye = round_data['eye'];
            var eye_wait = 0;

            this.free_game_score += total_score;
            if ( eye != 12) eye_wait = 2;
            this.scheduleOnce(function(){ // 等眼睛特效
                var wait = this.free_game_show_pink(round_data);

                if ( total_score == null || total_score == 0 ) {
                    this.scheduleOnce(function(){ 
                        this.free_game_end_round(idx);
                    }, 0.5+wait );
                    return;
                }
                
                this.scheduleOnce(function(){ // 等桃子
                    this.reset_reel();
                    cc.machine.payline.show_all_payline(round_data); 
                    cc.machine.payline.call_stop();
                    this.scheduleOnce(function() { 
                        this.reset_reel();
                        this.scheduleOnce(function() { 
                            this.free_game_end_round(idx);
                        },0.5);
                    },2);
                },wait+0.5);
            },eye_wait); // 眼睛特效
        },4); // 停輪等4秒
    },

    /**
     * 結束一局free game 
     * @param {*} idx 
     */
    free_game_end_round(idx) {
        var result = cc.machine.status.round_data.result;
        var round_data = result['all_round_data'][idx];
        var total_score = round_data['total_score'];
        var bet = cc.machine.total_bet_value();

        if ( cc.big_win_controller.check_big_win_score(bet, total_score) == false ) {
            return this.check_free_game_end(idx);
        }
        
        this.scheduleOnce(function() { this.free_game_waiting_bigwin(idx); },3);
        return cc.big_win_controller.run_big_win(bet, total_score);
    },

    /**
     * 等 BigWin 結束
     * @param {*} idx 
     */
    free_game_waiting_bigwin(idx) {
        if ( cc.big_win_controller.node.active == true ) {
            this.scheduleOnce(function(){ this.free_game_waiting_bigwin(idx); },1);
            return;
        }

        return this.check_free_game_end(idx);
    },

    spin_round(idx) {
        cc.machine.slot_control_bar.AutoStopNode.active = false;
        cc.machine.payline.show_end_payline(true);
        cc.machine.payline.show_payline_done();
        this.FreeGameTimes --;
        cc.machine.slot_control_bar.SpinLabel.string = this.FreeGameTimes;
        cc.machine.reel_frame.start_spin();
        this.SpinRound = idx;
        this.scheduleOnce(function(){
           this.free_game_spin(idx);
        },1);
    },

    put_symbol_id(data) {
        var sym_id = data[1];
        var reel_id = data[0];

        if ( reel_id == 5 ) {
            if ( sym_id == 11 ) return null;
            if ( sym_id == 12 ) return null;
            if ( sym_id == 13 ) return null;
            var ran_sym = [11,12,13];
            var ran = Math.floor(Math.random() * ran_sym.length );
            return ran_sym[ran];
        } else {
            if ( sym_id == 11 ) return 5;
            if ( sym_id == 12 ) return 6;
            if ( sym_id == 13 ) return 7;
        }

        return null;
    },

    event_all_reel_done(idx=0) {
        if ( cc.machine.status.bonus_gaming == true ) {
            idx = this.SpinRound;
            this.reset_reel();
        }
        var result = cc.machine.status.round_data.result;
        var data = result['all_round_data'][idx];
        var eye = data['eye'];

        switch(eye) {
            case 11:
                this.EyeEffect11.active = true;
                cc.Sound.PlaySound(this.EyeSound, false);
                this.scheduleOnce(function(){
                    this.EyeEffect11.active = false;
                    this.change_symbol(data);
                    this.scheduleOnce(function(){
                        if ( cc.machine.status.bonus_gaming == true ) return;
                        cc.machine.payline.all_reel_done(data);
                        cc.machine.event_all_reel_done();
                    },1.5);
                },1.5);
                return true;
            case 12:
                return false;
            case 13:
                this.EyeEffect13.active = true;
                cc.Sound.PlaySound(this.EyeSound, false);
                this.EyeEffect13Label.string = 'WIN X'+data['double'];
                this.scheduleOnce(function(){
                    this.EyeEffect13Label.string = '';
                    this.EyeEffect13.active = false;
                    if ( cc.machine.status.bonus_gaming == true ) return;
                    cc.machine.payline.all_reel_done(data);
                    cc.machine.event_all_reel_done();
                },1.5);
                return true;
        }

        return false;
    },

    reelActive(index, active, link=null) {
        var symbols = cc.machine.reel_frame.reel[index].query_symbol();
        console.log("reelActive",[index, active, symbols]);
        for(var idx in symbols) {
            var sym = symbols[idx];
            sym.active = active;
            sym.link = link;
        }
    },

    change_symbol(data) {
        var reel_frame = data['reel_frame'];
        var new_frame = data['new_frame'];
        var reel_wild = data['reel_wild'];
        
        var allChangeEffect = [];
        var showEffectTarget = cc.machine.payline.ShowPaylineNode;

        for(let x=0;x<reel_frame.length-1;x++) {
            if ( reel_frame[x] == null ) continue;

            if ( reel_wild[x] == true ) {
                this.ChangeSymbolReelEffect[x].active = true;
                this.reelActive(x, false, this.ChangeSymbolReelEffect[x]);
                continue;
            }

            for(let y=0;y<reel_frame[x].length;y++) {
                if ( reel_frame[x][y] == null ) continue;
                if ( reel_frame[x][y] == new_frame[x][y]) continue;
                cc.machine.reel_frame.reel[x].set_symbol(y, new_frame[x][y]);

                var effect = cc.lair.take_node("EhangeSymbolEffect");
                var sym = cc.machine.reel_frame.reel[x].query_symbol()[y];
                var world_pos = showEffectTarget.parent.convertToWorldSpaceAR(showEffectTarget.getPosition());
                var to_pos = sym.convertToNodeSpaceAR(world_pos);
                to_pos.y = cc.machine.reel_frame.reel[x].array_y[y];
                to_pos.x *= -1;

                effect.setParent(showEffectTarget);
                effect.setPosition(to_pos);
                effect.active = true;
                allChangeEffect.push(effect);
            }
        }

        if ( cc.machine.status.bonus_gaming == true ) return;
        this.scheduleOnce(function(){
            for(var index in allChangeEffect) {
                console.log("change symbol back");
                var eff = allChangeEffect[index];
                eff.active = false;
                this.reelActive(index, true);
                cc.lair.put_node(eff, "EhangeSymbolEffect");
            }
        },1);
    },

    get_reel_symbol(reelID) {
        var ranSymTable;
        if ( reelID == 5 ) {
            ranSymTable = [11,12,13];
        } else {
            ranSymTable = [0,1,2,3,4,5,6,7,8,9];
        }

        var ran = Math.floor(Math.random() * ranSymTable.length);  
        return ranSymTable[ran];
    },

    spin() {
        cc.machine.payline.show_end_payline(true); 
        cc.machine.payline.show_payline_done();
    },

    back_to_normal() {
        cc.machine.payline.show_end_payline(true); 
        cc.machine.payline.show_payline_done();

    },
});
