var simulation = require("../../../../public/module/slot/machine/scripts/simulation_server");
cc.Class({
    extends: simulation,

    init() {
        // 滾輪區大小 3x5
        this.ReelSize = [3,3,3,3,3];

        // 排列帶
        this.ReelCombination = {
            0 : [0,1,2,3,4,5,6,7,9],
            1 : [0,1,2,9,3,4,5,9,6,7,8],
            2 : [0,1,2,9,3,4,5,9,6,7,8],
            3 : [0,1,2,9,3,4,5,9,6,7,8],
            4 : [0,1,2,9,3,4,5,9,6,7,8],
        };

        this.ReelCombinationFreeGame = {
            0 : [0,1,2,3,4,5,6,7],
            1 : [0,1,2,3,4,5,6,7,8],
            2 : [0,1,2,3,4,5,6,7,8],
            3 : [0,1,2,3,4,5,6,7,8],
            4 : [0,1,2,3,4,5,6,7,8],
        };


        this.symbol = { 
            wild : 8, // 百搭編號

            0 : {
                name : "二郎神",            // 名稱, 只參考用 
                score : [0,0,50,200,1000,], // 分數配置
            },

            1 : {
                name : "太上老君", 
                score : [0,0,40,140,600,],
            },

            2 : {
                name : "哮天犬", 
                score : [0,0,40,110,400,],
            },

            3 : {
                name : "葫蘆", 
                score : [0,0,30,90,240,],
            },

            4 : {
                name : "A", 
                score : [0,0,20,70,130,],
            },

            5 : {
                name : "K", 
                score : [0,0,20,60,110,],
            },

            6 : {
                name : "Q", 
                score : [0,0,20,50,90,],
            },

            7 : {
                name : "J", 
                score : [0,0,20,40,70,],
            },

            8 : {
                name : "Wild", 
                score : null,
            },

            9 : {
                name: "FS",
                score : null,
                type: "free_game",
                round: [0,0,0,5,8,10], // free game 局數
            },

            10: {
                name : "蟠桃",
                score: null,
                type: "bonus_game",
            },

        };

        this.ReelLine = [ // 20 線的配置
            [1,1,1,1,1], [0,0,0,0,0], [2,2,2,2,2], [0,1,2,1,0], [2,1,0,1,2], 
            [0,1,1,1,0], [2,1,1,1,2], [1,0,0,0,1], [1,2,2,2,1], [1,1,0,1,1], 
            [1,1,2,1,1], [0,0,1,0,0], [2,2,1,2,2], [0,1,0,1,0], [2,1,2,1,2],
            [1,0,1,0,1], [1,2,1,2,1], [0,2,2,2,0], [2,0,0,0,2], [2,2,0,2,2]
        ];
    },

    free_game(sym, symbol_data, result) {
        
        var count = 0;
        for(var i=0;i<symbol_data['count'].length;i++) {
            count += symbol_data['count'][i];
        }

        var free_spin = this.symbol[sym]['round'][count];
        if ( free_spin == 0 ) return null;
        
        var total_score = result['result']['total_score'] * 1000;
        for(var i=1;i<free_spin+1;i++) {
            result['result']['all_round_data'][i] = this.make_spin('ReelCombinationFreeGame');
            var score = result['result']['all_round_data'][i]['total_score'];
            total_score += (score * 1000);
        }

        total_score = total_score / 1000;

        result['result']['all_round_data'][0]['bonus_count'] = free_spin;
        result['result']['all_round_data'][0]['bonus_symbol'] = sym;
        result['result']['all_round_data'][0]['type'] = 'enter_bonus';
        result['result']['type'] = 'bonus';
        result['result']['round_count'] = free_spin+1;
        result['result']['total_score'] = total_score;

        return result;
    },

});
