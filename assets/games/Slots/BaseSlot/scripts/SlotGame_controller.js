cc.Class({
    extends: cc.Component,

    properties: {
        BeginFreeGameUI : {
            type: cc.Node,
            default: null,
        },

        BeginFreeGameLabel : {
            type: cc.Label,
            default: null,
        },

        EndFreeGameUI : {
            type:cc.Node,
            default:null,
        },

        EndFreeGameTotalScore : {
            type:cc.Label,
            default:null,
        },
    },

    onLoad() {
        this.BeginFreeGameUI.active = false;
    },

    begin_free_game_ui(symbol, param) {
        cc.machine.status.bonus_gaming = true;
        this.BeginFreeGameLabel.string = param['bonus_count'];
        this.BeginFreeGameUI.active = true;
    },

    start_free_game() {
        this.free_game_score = 0;
        this.BeginFreeGameUI.active = false;
        this.scheduleOnce(function(){
            this.spin_round(1);
        },2);
    },

    begin_end_free_game_ui() {
        this.EndFreeGameUI.active = true;
        this.EndFreeGameTotalScore.string = this.free_game_score;
    },

    close_end_free_game_ui() {
        this.BeginFreeGameUI.active = false;
        this.EndFreeGameUI.active = false;
        cc.machine.status.bonus_gaming = false;
        cc.machine.status.bonus_done = true;
        cc.machine.back_to_normal();
    },

    check_free_game_end(idx) {
        idx ++;
        console.log('score',this.free_game_score);
        var result = cc.machine.status.round_data.result;
        if ( result['all_round_data'][idx] == null ) return this.begin_end_free_game_ui();
        return this.scheduleOnce(function(){ this.spin_round(idx) },1);
    },

    spin_round(idx) {
        
        cc.machine.reel_frame.start_spin();
        var result = cc.machine.status.round_data.result;
        this.scheduleOnce(function(){
            // 執行Spin
            cc.machine.reel_frame.spin_response(result['all_round_data'][idx]['reel_frame']);

            this.scheduleOnce(function(){

                var total_score = result['all_round_data'][idx]['total_score'];
                if ( total_score > 0 ) {
                    this.free_game_score += total_score;
                    
                    this.scheduleOnce(function(){ 
                        cc.machine.payline.show_all_payline(result['all_round_data'][idx]); 
                        this.scheduleOnce(function(){ 
                            cc.machine.payline.show_end_payline(true); 
                            this.scheduleOnce(function(){ this.check_free_game_end(idx); },1);
                        },3);
                    },2);
                    
                } else {
                    this.scheduleOnce(function(){ this.check_free_game_end(idx); },1);
                }
            },2);
        },1);
    },

});
